/*
 * Copyright 2020 Lisandro Damián Nicanor Pérez Meyer <perezmeyer@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 */
#ifndef DEVICESINFOPARSER_H
#define DEVICESINFOPARSER_H

#include <QString>

#include <stdint.h>

#include <mosquitto.h>

#include <mosimpaqt/definitions.h>

class QJsonObject;

using namespace MoSimPa;

class DevicesInfoParser
{
public:

    /**
     * @brief The values struct
     * Note that they types are actually bigger and signed versions of the
     * original data, this is in order to be able to send a negative value to
     * demark that a value has not been found.
     */
    struct values {
        QString mac;
        batt_mv_t batterymV;
        time_t time;
        msg_id_t msgId;
    };

    static QString topic() { return QStringLiteral("devices/info"); }
    [[nodiscard]] static bool parseMessage(struct mosquitto_message * msg, struct values & values);
};

#endif // DEVICESINFOPARSER_H
