/*
 * Copyright 2020 Lisandro Damián Nicanor Pérez Meyer <perezmeyer@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 */
#include <QDebug>
#include <QTimer>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>
#include <QString>
#include <QByteArray>

#include <mosimpaqt/mosquittoclient.h>
#include "devicesinfoparser.h"

/**
 * @brief DevicesInfoParser::parseMessage
 * @param msg Pointer to the message to be parsed.
 * @param values Struct that will return the read values.
 * @returns false if the JSON can't be parsed, true otherwise.
 */
bool DevicesInfoParser::parseMessage(struct mosquitto_message * msg, DevicesInfoParser::values &values)
{
    QByteArray payload(static_cast<char*>(msg->payload), msg->payloadlen);
    QJsonDocument jsonDoc = QJsonDocument::fromJson(payload);
    QRegExp macRx("^([0-9a-f]{2}){6}$");
    Q_ASSERT_X(macRx.isValid(), __FUNCTION__, "The MAC regular expression must be valid.");
    int64_t tmp;

    values.mac = QString();
    values.time = -1;
    values.msgId = 0;
    values.batterymV = 0;

    if(!jsonDoc.isObject())
    {
        qDebug() << "No JSON object received, discarding message with payload " << QString(payload);
        return false;
    }

    QJsonObject mainObject = jsonDoc.object();

    // MAC.
    if(!mainObject.contains("WiFiMAC") || !mainObject["WiFiMAC"].isString())
        return false;

    values.mac = mainObject["WiFiMAC"].toString("").toLower();
    if(!macRx.exactMatch(values.mac))
        return false;

    // battery mV.
    if(!mainObject.contains("battmV") || !mainObject["battmV"].isDouble())
        return false;

    tmp = static_cast<int64_t>(mainObject["battmV"].toDouble(-1.0));
    if((tmp < 0) || (tmp > std::numeric_limits<batt_mv_t>::max()))
        return false;

    values.batterymV = static_cast<batt_mv_t>(tmp);

    // Time.
    if(!mainObject.contains("time") || !mainObject["time"].isDouble())
        return false;

    tmp = static_cast<int64_t>(mainObject["time"].toDouble(-1.0));
    if(tmp < 0)
        return false;

    values.time = static_cast<time_t>(tmp);

    // Message ID.
    if(!mainObject.contains("msgId") || !mainObject["msgId"].isDouble())
        return false;

    tmp = static_cast<int64_t>(mainObject["msgId"].toDouble(-1.0));
    if((tmp < 0) || (tmp > std::numeric_limits<msg_id_t>::max()))
        return false;

    values.msgId = static_cast<msg_id_t>(tmp);

    return true;
}
