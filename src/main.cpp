/*
 * Copyright 2020 Lisandro Damián Nicanor Pérez Meyer <perezmeyer@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 */
#include <QCoreApplication>
#include <QTranslator>
#include <QLibraryInfo>
#include <QLocale>
#include <QTextStream>
#include <QTimer>
#include <QString>


#include "engine.h"

int main(int argc, char *argv[])
{
   QCoreApplication app(argc, argv);
   QMap<QString,QString> logMap;

   // Basic app info.
   app.setOrganizationName("MoSimPa");
   app.setOrganizationDomain("mosimpa.gitlab.io");
   app.setApplicationName("datakeeper");
   app.setApplicationVersion(DATAKEEPER_VERSION);

   QTextStream stream(stdout);
   stream << QString("Initializing %1 version %2 on PID %3")
                    .arg(app.applicationName())
                    .arg(DATAKEEPER_VERSION_STRING)
                    .arg(app.applicationPid()) << endl;

   QTranslator qtTranslator;
   qtTranslator.load("qt_" + QLocale::system().name(),
                      QLibraryInfo::location(QLibraryInfo::TranslationsPath));
   app.installTranslator(&qtTranslator);

   QTranslator qtBaseTranslator;
   qtBaseTranslator.load("qtbase_" + QLocale::system().name(),
                         QLibraryInfo::location(QLibraryInfo::TranslationsPath));
   app.installTranslator(&qtBaseTranslator);

   QTranslator appTranslator;
   /*
    *  Try loading the translations from the system's normal path first, else
    *  at the same path as the binary itself.
    */
   if(!appTranslator.load("/usr/share/mosimpa-datakeeper/qm/datakeeper_" + QLocale::system().name()))
       appTranslator.load("datakeeper_" + QLocale::system().name());
   app.installTranslator(&appTranslator);

   auto engine = new Engine();
   QTimer::singleShot(200, engine, SLOT(connect()));

   return app.exec();
}
