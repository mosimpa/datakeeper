#include <QObject>
#include <QSettings>
#include <QTextStream>
#include <QCoreApplication>
#include <QString>

#include "settings.h"

Settings::Settings(QObject *parent) : QObject(parent)
{
    if(mSettings == nullptr)
    {
#ifdef DATAKEEPER_TEST
        // Basic app info.
        qApp->setOrganizationName("MoSimPa");
        qApp->setOrganizationDomain("mosimpa.gitlab.io");
        // Do not set the application name, let the test identify itself.
        qApp->setApplicationVersion(DATAKEEPER_VERSION);
        mSettings = new QSettings();
#else
        mSettings = new QSettings(QSettings::NativeFormat, QSettings::SystemScope,
                                  qApp->organizationName(), qApp->applicationName(), this);
#endif
    }

    QTextStream stdoutStream(stdout);
    stdoutStream << tr("%1: settings are being stored in %2").arg(qApp->applicationName())
                                                             .arg(mSettings->fileName())
                 << endl;

    load();
}

Settings::~Settings()
{
    mSettings->sync();
}

/**
 * @brief Settings::instance Singleton instance.
 * @return Reference to the instance.
 */
Settings &Settings::instance()
{
    static Settings instance;
    return instance;
}

void Settings::load()
{
    mSettings->beginGroup("database");
    mDBHostname = readOrSet(QStringLiteral("hostname"), QStringLiteral("localhost"));
    mDatabase = readOrSet(QStringLiteral("database"), QStringLiteral("mosimpa-datakeeper"));
    mDBUserName = readOrSet(QStringLiteral("username"), QStringLiteral("datakeeper"));
    mDBPassword = readOrSet(QStringLiteral("password"), QStringLiteral("youshouldreallychangeme"));
    mOldDataThresholdS = readOrSet(QStringLiteral("oldDataThresholdS"), 24*60*60);

    mSettings->endGroup();

    mSettings->beginGroup("mqtt");
    mMqttBroker = readOrSet(QStringLiteral("broker"), QStringLiteral("localhost"));
    mSettings->endGroup();

    mSettings->beginGroup("reports");
    mReportsHostname = readOrSet(QStringLiteral("reportsUrl"), QStringLiteral("localhost"));
    mSettings->endGroup();
}

QString Settings::readOrSet(const QString &key, const QString & defaultValue)
{
    if(mSettings->contains(key))
        return mSettings->value(key).toString();

    mSettings->setValue(key, defaultValue);
    return defaultValue;
}

int32_t Settings::readOrSet(const QString &key, const int32_t &defaultValue)
{
    if(mSettings->contains(key))
        return mSettings->value(key).toInt();

    mSettings->setValue(key, defaultValue);
    return defaultValue;
}
