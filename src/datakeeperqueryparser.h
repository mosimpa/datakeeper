/*
 * Copyright 2020 Lisandro Damián Nicanor Pérez Meyer <perezmeyer@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 */
#ifndef DATAKEEPERQUERYPARSER_H
#define DATAKEEPERQUERYPARSER_H

#include <QString>
#include <QJsonObject>

#include <limits>
#include <stdint.h>

#include <mosquitto.h>
#include <mosimpaqt/definitions.h>

using namespace MoSimPa;

class DatakeeperQueryParser
{
public:

    /**
     * @brief The values struct
     * The strings in it will be empty if the object is not found.
     * QJsonObject is only valid if command is not empty and is one
     * of the commands that require it according to
     * https://mosimpa.gitlab.io/datos_a_transmitir/#interaccion-con-datakeeper
     */
    struct values {
        QString mac;
        QString command;
        QString id;
        db_id_t internment_id;
        QString lastAlarmUpdate;
        time_t fromTime;
        time_t toTime;
        int numOfSamples;

        spo2_t spo2LT;
        heart_rate_t hRLT;
        heart_rate_t hRGT;
        body_temp_t bTLT;
        body_temp_t bTGT;
        blood_pressure_t bPSysLT;
        blood_pressure_t bPSysGT;

        uint8_t spo2AlarmDelayS;
        uint8_t hRAlarmDelayS;
        uint8_t bTAlarmDelayS;
        uint8_t bPAlarmDelayS;
    };

    enum Commands {
        CommandUnknown = 0,
        CommandInternments,
        CommandSpO2,
        CommandHeartRate,
        CommandBodyTemperature,
        CommandBloodPressure,
        CommandAlarmsThresholds,
        CommandSetAlarmsThresholds,
        CommandAlarmRanges,
        CommandGenerateReport
    };

    static QString topic() { return QStringLiteral("datakeeper/query"); }
    static Commands parseMessage(struct mosquitto_message * msg, struct values & values);
};

#endif // DATAKEEPERQUERYPARSER_H
