#include <QDebug>
#include <QObject>
#include <QTimer>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>
#include <QString>
#include <QByteArray>
#include <QSql>
#include <QSqlQuery>
#include <QSqlDatabase>
#include <QSqlError>
#include <QVector>
#include <QTimer>
#include <QDateTime>
#include <QSqlDriver>
#include <QTimeZone>
#include <QMap>

#include <mosimpaqt/mosquittoclient.h>
#include <mosimpaqt/readsparser.h>
#include <mosimpaqt/definitions.h>

#include "devicesinfoparser.h"
#include "datakeeperqueryparser.h"
#include "settings.h"
#include "reportgeneratorcontroller.h"
#include "reportgenerator.h"
#include "engine.h"

using namespace MoSimPa;

Engine::Engine(QObject *parent) : QObject(parent)
{
    mMosqClient = new MosquittoClient(false, this);
    mMosqClient->setEnableLogs(false);

    QObject::connect(mMosqClient, &MosquittoClient::connectionTried, this, &Engine::onConnectionTried);
    QObject::connect(mMosqClient, &MosquittoClient::errorWhileLooping, this, &Engine::onErrorWhileLooping);
    QObject::connect(mMosqClient, &MosquittoClient::messageReceived, this, &Engine::onMessageReceived);

    mConnectionWasLost = false;

    mDeleteDataCounter = 0;
    mGeneralTimer = new QTimer(this);
    QObject::connect(mGeneralTimer, &QTimer::timeout, this, &Engine::onTimerEvent);
    mGeneralTimer->start(TIMEOUT_MS);
}

void Engine::connect()
{
    auto db = QSqlDatabase::addDatabase("QPSQL");
    db.setHostName(Settings::instance().dBHostname());
    db.setUserName(Settings::instance().dBUserName());
    db.setPassword(Settings::instance().dBPassword());
    db.setDatabaseName(Settings::instance().database());

    if(!db.isOpen() && !db.open())
    {
        qWarning() << "Could not open database connection!";
        return;
    }

    mConnectionWasLost = false;
    qDebug() << "Connected to DB.";

    if(!db.driver()->subscribeToNotification(internmentsChangedNotificationString()))
        qDebug() << "Could not subscribe to notification" << internmentsChangedNotificationString();
    else
        qDebug() << "Subscribed to notification" << db.driver()->subscribedToNotifications();

    QObject::connect(db.driver(), SIGNAL(notification(const QString &)), this, SLOT(internmentsChanged(const QString &)));

    /// \todo Check return value!!!
    mMosqClient->connect(Settings::instance().mqttBroker());

    refreshActiveInternments();
}

void Engine::onConnectionTried(int result)
{
    if(result != MOSQ_ERR_SUCCESS)
    {
        qWarning() << "Connection failed! Error: " << result;
        QTimer::singleShot(1000, this, &Engine::connect);
        return;
    }

    mMosqClient->subscribe(ReadsParser::topic(), 1);
    mMosqClient->subscribe(DevicesInfoParser::topic(), 1);
    mMosqClient->subscribe(DatakeeperQueryParser::topic(), 1);
}

void Engine::onErrorWhileLooping(int error)
{
    qWarning() << "Error while looping, error: " << error;
    QTimer::singleShot(1000, this, &Engine::connect);
}

void Engine::onMessageReceived()
{
    QVector<struct mosquitto_message*> msgs = mMosqClient->messages();

    for(int i = 0; i < msgs.size(); i++)
        parseMessage(msgs.at(i));

    for(int i = 0; i < msgs.size(); i++)
        mosquitto_message_free(&(msgs[i]));
}

void Engine::parseMessage(mosquitto_message *msg)
{
    struct DevicesInfoParser::values devicesValues;
    struct DatakeeperQueryParser::values datakeeperValues;
    QString topic(msg->topic);

    if(topic == DevicesInfoParser::topic())
    {
        if(DevicesInfoParser::parseMessage(msg, devicesValues))
            addToDB(devicesValues);
        return;
    }

    if(topic.startsWith(QStringLiteral("reads/")))
    {
        ReadsParser parser;
        if(parser.parseMessage(msg))
            addToDB(parser);
        return;
    }

    if(topic == DatakeeperQueryParser::topic())
    {
        const auto command = DatakeeperQueryParser::parseMessage(msg, datakeeperValues);
        if(command == DatakeeperQueryParser::CommandUnknown)
            sendCommandResult(datakeeperValues, ResultError, ReasonCommandUnknown);
        execDatakeeperQuery(command, datakeeperValues);
        return;
    }
}

void Engine::deleteOldData()
{
    auto oldDataThresholdS = Settings::instance().oldDataThresholdS();

    if(oldDataThresholdS == 0)
    {
        qDebug() << "Deletion of old data is disabled.";
        return;
    }

    QDateTime dt = QDateTime::currentDateTime().addSecs(-1*oldDataThresholdS);
    qDebug() << "Deleting data older than " << dt.toString(Qt::ISODate);

    QString queryString = QString("SELECT delete_old_data(%1)")
                                 .arg(dt.toSecsSinceEpoch());

    QSqlQuery query;
    if(!query.exec(queryString))
    {
        qWarning() << QString("Failed to delete old data, error was: %1")
                      .arg(query.lastError().text());
    }

    qDebug() << "Performing VACUUM in *_values tables.";
    queryString = QString("VACUUM ANALYZE spo2_values, blood_pressure_values, heart_rate_values, body_temperature_values");
    if(!query.exec(queryString))
    {
        qWarning() << QString("Failed to run VACUUM, error was: %1")
                      .arg(query.lastError().text());
    }
}

void Engine::internmentsChanged(const QString & /*str*/)
{
    refreshActiveInternments();

    QString payload = QString("{\"signal\":\"internments_changed\"}");
    mMosqClient->publish(QStringLiteral("datakeeper/signals"), payload.toUtf8(), 1, false);
}

bool Engine::checkDBConnection()
{
    if(mConnectionWasLost)
        connect();

    if(!QSqlDatabase::database().isOpen())
    {
        qDebug() << "Database connection lost.";
        mConnectionWasLost = true;
        return false;
    }

    return true;
}

void Engine::handleReport(bool success, QString reportUrl, const DatakeeperQueryParser::values &values, ReportGeneratorController *rgc)
{
    if(!success || values.mac.isEmpty() || values.id.isEmpty() || values.command.isEmpty())
    {
        sendCommandResult(values, ResultError, ReasonReportGenerationFailed);
        return;
    }

    QString msg;
    msg = QString("{\"id\":\"%1\",\"command\":\"gen_report\",\"url\":\"%2\"}")
                 .arg(values.id).arg(reportUrl);

    mMosqClient->publish(QString("monitor/%1").arg(values.mac), msg.toUtf8(), 1, false);

    rgc->deleteLater();
}

void Engine::addToDB(const DevicesInfoParser::values &values)
{
    /// \todo Add firmware version field.
    QString queryString = QString("SELECT add_or_update_device('%1', %2, %3, %4)")
                                 .arg(values.mac)
                                 .arg(values.batterymV)
                                 .arg(values.time)
                                 .arg(values.msgId);

    QSqlQuery query;
    if(!query.exec(queryString))
    {
        qWarning() << QString("Failed to add device info, error was: %1")
                      .arg(query.lastError().text());
    }
}

void Engine::addToDB(const ReadsParser &readsParser)
{
    updateDeviceLastSeenTimestamp(readsParser.mac());

    if(readsParser.spO2ValuesSize() > 0)
        addSpo2ToDB(readsParser);

    if(readsParser.bodyTempValuesSize() > 0)
        addBodyTemperatureToDB(readsParser);

    if(readsParser.bloodPressureValuesSize() > 0)
        addBloodPressureToDB(readsParser);

    if(readsParser.heartRateValuesSize() > 0)
        addHeartRateToDB(readsParser);
}

void Engine::execDatakeeperQuery(const DatakeeperQueryParser::Commands command, const DatakeeperQueryParser::values &values)
{
    switch(command) {
    case DatakeeperQueryParser::CommandInternments:
        sendActiveInternments(values.mac, values.id);
        break;

    case DatakeeperQueryParser::CommandSpO2:
        sendSpO2Data(values);
        break;

    case DatakeeperQueryParser::CommandHeartRate:
        sendHeartRate2Data(values);
        break;

    case DatakeeperQueryParser::CommandBloodPressure:
        sendBloodPressureData(values);
        break;

    case DatakeeperQueryParser::CommandBodyTemperature:
        sendBodyTemperatureData(values);
        break;

    case DatakeeperQueryParser::CommandAlarmsThresholds:
        sendAlarmsThresholds(values);
        break;

    case DatakeeperQueryParser::CommandSetAlarmsThresholds:
        updateAlarmsThresholds(values);
        break;

    case DatakeeperQueryParser::CommandAlarmRanges:
        sendAlarmsRanges(values);
        break;

    case DatakeeperQueryParser::CommandGenerateReport:
        generateReport(values);
        break;

    case DatakeeperQueryParser::CommandUnknown:
        break;
    }
}

void Engine::sendActiveInternments(const QString mac, const QString id)
{
    QString queryString = QString("SELECT i.internment_id, i.date_time_begins, d.mac, "
                                  "l.type, l.description, p.name, p.surname, "
                                  "p.age, p.gender FROM internments i, "
                                  "devices d, locations l, patients p "
                                  "WHERE i.mac = d.mac AND "
                                  "i.location_id = l.location_id AND "
                                  "i.patient_id = p.patient_id");

    QSqlQuery query;
    if(!query.exec(queryString))
    {
        qWarning() << "Active internments query failed with error:" << query.lastError().text();
        return;
    }

    QVector<QString> internments;

    while(query.next())
    {
        QString internment = QString("{\"internment_id\":%1,").arg(static_cast<db_id_t>(query.value(0).toLongLong()));
        internment.append(QString("\"from_time\":%1,").arg(query.value(1).toDateTime().toSecsSinceEpoch()));
        internment.append(QString("\"device\":\"%1\",").arg(query.value(2).toString().remove(':')));

        internment.append(QStringLiteral("\"location\":{"));
        internment.append(QString("\"type\":\"%1\",").arg(query.value(3).toString()));
        internment.append(QString("\"desc\":\"%1\"").arg(query.value(4).toString()));
        internment.append(QStringLiteral("},"));

        internment.append(QStringLiteral("\"patient\":{"));
        internment.append(QString("\"name\":\"%1\",").arg(query.value(5).toString()));
        internment.append(QString("\"surname\":\"%1\",").arg(query.value(6).toString()));
        internment.append(QString("\"age\":%1,").arg(query.value(7).toString()));
        internment.append(QString("\"gender\":\"%1\"").arg(query.value(8).toString()));
        internment.append(QStringLiteral("}}"));

        internments.append(internment);
    }

    QString result(QString("{\"id\":\"%1\",").arg(id));
    if(internments.isEmpty())
    {
        result.append(QStringLiteral("\"internments\":[]}"));
        mMosqClient->publish(QString("monitor/%1").arg(mac), result.toUtf8() , 1, false);
        return;
    }

    result.append(QStringLiteral("\"internments\":["));
    for(int i = 0; i < internments.size() - 1; i++)
        result.append(QString("%1,").arg(internments.at(i)));
    result.append(QString("%1]}").arg(internments.last()));

    mMosqClient->publish(QString("monitor/%1").arg(mac), result.toUtf8(), 1, false);
}

void Engine::sendSpO2Data(const DatakeeperQueryParser::values &values)
{
    QString queryString;
    if(values.numOfSamples == 0)
    {
        queryString = QString("SELECT time, spo2 FROM spo2_values "
                              "WHERE internment_id = %1 AND time >= %2 "
                              "ORDER BY time desc")
                              .arg(values.internment_id)
                              .arg(values.fromTime);
    }
    else
    {
        queryString = QString("SELECT time, spo2, R FROM spo2_values "
                              "WHERE internment_id = %1 AND time >= %2 "
                              "ORDER BY time desc "
                              "LIMIT %3")
                              .arg(values.internment_id)
                              .arg(values.fromTime)
                              .arg(values.numOfSamples);
    }

    QSqlQuery query;
    if(!query.exec(queryString))
    {
        qWarning() << "SpO2 values query failed with error:" << query.lastError().text();
        return;
    }

    QVector<QString> samples;

    while(query.next())
    {
        QString sample = QString("{\"time\":%1,").arg(static_cast<db_time_t>(query.value(0).toLongLong()));
        sample.append(QString("\"SpO2\":%1").arg(static_cast<spo2_t>(query.value(1).toInt())));
        sample.append(QStringLiteral("}"));

        samples.append(sample);
    }

    QString result(QString("{\"id\":\"%1\",\"internment_id\":%2,").arg(values.id).arg(values.internment_id));
    if(samples.isEmpty())
    {
        result.append(QStringLiteral("\"spo2\":[]}"));
        mMosqClient->publish(QString("monitor/%1").arg(values.mac), result.toUtf8() , 1, false);
        return;
    }

    result.append(QStringLiteral("\"spo2\":["));
    for(int i = samples.size() -1 ; i > 0; i--)
        result.append(QString("%1,").arg(samples.at(i)));
    result.append(QString("%1]}").arg(samples.first()));

    mMosqClient->publish(QString("monitor/%1").arg(values.mac), result.toUtf8(), 1, false);
}

void Engine::sendHeartRate2Data(const DatakeeperQueryParser::values &values)
{
    QString queryString;
    if(values.numOfSamples == 0)
    {
        queryString = QString("SELECT time, heart_rate FROM heart_rate_values "
                              "WHERE internment_id = %1 AND time >= %2 "
                              "ORDER BY time desc")
                              .arg(values.internment_id)
                              .arg(values.fromTime);
    }
    else
    {
        queryString = QString("SELECT time, heart_rate FROM heart_rate_values "
                              "WHERE internment_id = %1 AND time >= %2 "
                              "ORDER BY time desc "
                              "LIMIT %3")
                              .arg(values.internment_id)
                              .arg(values.fromTime)
                              .arg(values.numOfSamples);
    }

    QSqlQuery query;
    if(!query.exec(queryString))
    {
        qWarning() << "Heart rate values query failed with error:" << query.lastError().text();
        return;
    }

    QVector<QString> samples;

    while(query.next())
    {
        QString sample = QString("{\"time\":%1,").arg(static_cast<db_time_t>(query.value(0).toLongLong()));
        sample.append(QString("\"heartR\":%1").arg(static_cast<heart_rate_t>(query.value(1).toInt())));
        sample.append(QStringLiteral("}"));

        samples.append(sample);
    }

    QString result(QString("{\"id\":\"%1\",\"internment_id\":%2,").arg(values.id).arg(values.internment_id));
    if(samples.isEmpty())
    {
        result.append(QStringLiteral("\"heartR\":[]}"));
        mMosqClient->publish(QString("monitor/%1").arg(values.mac), result.toUtf8() , 1, false);
        return;
    }

    result.append(QStringLiteral("\"heartR\":["));
    for(int i = samples.size() -1 ; i > 0; i--)
        result.append(QString("%1,").arg(samples.at(i)));
    result.append(QString("%1]}").arg(samples.first()));

    mMosqClient->publish(QString("monitor/%1").arg(values.mac), result.toUtf8(), 1, false);
}

void Engine::sendBloodPressureData(const DatakeeperQueryParser::values &values)
{
    QString queryString;
    if(values.numOfSamples == 0)
    {
        queryString = QString("SELECT time, systolic, diastolic FROM blood_pressure_values "
                              "WHERE internment_id = %1 AND time >= %2 "
                              "ORDER BY time desc")
                              .arg(values.internment_id)
                              .arg(values.fromTime);
    }
    else
    {
        queryString = QString("SELECT time, systolic, diastolic FROM blood_pressure_values "
                              "WHERE internment_id = %1 AND time >= %2 "
                              "ORDER BY time desc "
                              "LIMIT %3")
                              .arg(values.internment_id)
                              .arg(values.fromTime)
                              .arg(values.numOfSamples);
    }

    QSqlQuery query;
    if(!query.exec(queryString))
    {
        qWarning() << "Blood pressure values query failed with error:" << query.lastError().text();
        return;
    }

    QVector<QString> samples;

    while(query.next())
    {
        QString sample = QString("{\"time\":%1,").arg(static_cast<db_time_t>(query.value(0).toLongLong()));
        sample.append(QString("\"sys\":%1,").arg(static_cast<blood_pressure_t>(query.value(1).toInt())));
        sample.append(QString("\"dia\":%1").arg(static_cast<blood_pressure_t>(query.value(2).toInt())));
        sample.append(QStringLiteral("}"));

        samples.append(sample);
    }

    QString result(QString("{\"id\":\"%1\",\"internment_id\":%2,").arg(values.id).arg(values.internment_id));
    if(samples.isEmpty())
    {
        result.append(QStringLiteral("\"bloodP\":[]}"));
        mMosqClient->publish(QString("monitor/%1").arg(values.mac), result.toUtf8() , 1, false);
        return;
    }

    result.append(QStringLiteral("\"bloodP\":["));
    for(int i = samples.size() -1 ; i > 0; i--)
        result.append(QString("%1,").arg(samples.at(i)));
    result.append(QString("%1]}").arg(samples.first()));

    mMosqClient->publish(QString("monitor/%1").arg(values.mac), result.toUtf8(), 1, false);
}

void Engine::sendBodyTemperatureData(const DatakeeperQueryParser::values &values)
{
    QString queryString;

    if(values.numOfSamples == 0)
    {
        queryString = QString("SELECT time, temperature FROM body_temperature_values "
                              "WHERE internment_id = %1 AND time >= %2 "
                              "ORDER BY time desc")
                              .arg(values.internment_id)
                              .arg(values.fromTime);
    }
    else
    {
        queryString = QString("SELECT time, temperature FROM body_temperature_values "
                              "WHERE internment_id = %1 AND time >= %2 "
                              "ORDER BY time desc "
                              "LIMIT %3")
                              .arg(values.internment_id)
                              .arg(values.fromTime)
                              .arg(values.numOfSamples);
    }

    QSqlQuery query;
    if(!query.exec(queryString))
    {
        qWarning() << "Body temperature values query failed with error:" << query.lastError().text();
        return;
    }

    QVector<QString> samples;

    while(query.next())
    {
        QString sample = QString("{\"time\":%1,").arg(static_cast<db_time_t>(query.value(0).toLongLong()));
        sample.append(QString("\"temp\":%1").arg(static_cast<body_temp_t>(query.value(1).toInt())));
        sample.append(QStringLiteral("}"));

        samples.append(sample);
    }

    QString result(QString("{\"id\":\"%1\",\"internment_id\":%2,").arg(values.id).arg(values.internment_id));
    if(samples.isEmpty())
    {
        result.append(QStringLiteral("\"bodyT\":[]}"));
        mMosqClient->publish(QString("monitor/%1").arg(values.mac), result.toUtf8() , 1, false);
        return;
    }

    result.append(QStringLiteral("\"bodyT\":["));
    for(int i = samples.size() -1 ; i > 0; i--)
        result.append(QString("%1,").arg(samples.at(i)));
    result.append(QString("%1]}").arg(samples.first()));

    mMosqClient->publish(QString("monitor/%1").arg(values.mac), result.toUtf8(), 1, false);
}

void Engine::sendAlarmsThresholds(const DatakeeperQueryParser::values &values)
{
    QString queryString;
    queryString = QString("SELECT modify_ts, spo2_alarm_lt, spo2_alarm_delay_s, "
                          "hr_alarm_lt, hr_alarm_gt, hr_alarm_delay_s, "
                          "bt_alarm_lt, bt_alarm_gt, bt_alarm_delay_s, "
                          "bp_sys_alarm_lt, bp_sys_alarm_gt, bp_alarm_delay_s "
                          "FROM alarms WHERE internment_id = %1 "
                          "ORDER BY modify_ts DESC LIMIT 1").arg(values.internment_id);

    QSqlQuery query;
    if(!query.exec(queryString))
    {
        qWarning() << QString("Failed to get alarms, error was: %1")
                      .arg(query.lastError().text());
        sendCommandResult(values, ResultError, ReasonFailedToExecuteQuery);
        return;
    }

    if(query.size() == 0)
    {
        // There is no entry for this alarms yet. Create it.
        queryString = QString("INSERT INTO alarms(modify_ts, modifying_mac, internment_id) "
                              "VALUES(cast('%1' as timestamptz), '000000000000', %2)")
                .arg(QDateTime::currentDateTime().toString(Qt::ISODate)).arg(values.internment_id);

        if(!query.exec(queryString))
        {
            qWarning() << QString("Failed to create default alarms entry, error was: %1")
                          .arg(query.lastError().text());
            sendCommandResult(values, ResultError, ReasonFailedToExecuteQuery);
            return;
        }

        // Get the values.
        queryString = QString("SELECT modify_ts, spo2_alarm_lt, spo2_alarm_delay_s, "
                              "hr_alarm_lt, hr_alarm_gt, hr_alarm_delay_s, "
                              "bt_alarm_lt, bt_alarm_gt, bt_alarm_delay_s, "
                              "bp_sys_alarm_lt, bp_sys_alarm_gt, bp_alarm_delay_s "
                              "FROM alarms WHERE internment_id = %1 "
                              "ORDER BY modify_ts DESC LIMIT 1").arg(values.internment_id);

        if(!query.exec(queryString))
        {
            qWarning() << QString("Failed to get alarms, error was: %1")
                          .arg(query.lastError().text());
            sendCommandResult(values, ResultError, ReasonFailedToExecuteQuery);
            return;
        }
    }

    if(!query.next())
        sendCommandResult(values, ResultError, ReasonFailedToExecuteQuery);

    // Be sure to specify the timezone...
    QDateTime time = query.value(0).toDateTime();
    time.setTimeZone(QTimeZone::systemTimeZone());

    /*
     * Send the message. Note that we ask Date/Time with milliseconds, they
     * have been rounded to 3 digits in the stored procedure so that it matches
     * what we can handle with Qt. Ideally I would just send the string but I
     * don't seem able to bypass Qt in this :-/
     */
    auto msg = QString("{\"id\":\"%1\",\"internment_id\":%2,"
                       "\"alarms_thresholds\":{\"last_update\":\"%3\","
                       "\"spo2_lt\":%4,\"spo2_delay_s\":%5,"
                       "\"hr_lt\":%6,\"hr_gt\":%7,\"hr_delay_s\":%8,"
                       "\"bt_lt\":%9,\"bt_gt\":%10,\"bt_delay_s\":%11,"
                       "\"bp_sys_lt\":%12,\"bp_sys_gt\":%13,\"bp_delay_s\":%14}}")
            .arg(values.id).arg(values.internment_id)
            .arg(time.toString(Qt::ISODateWithMs))
            .arg(query.value(1).toInt()).arg(query.value(2).toInt())
            .arg(query.value(3).toInt()).arg(query.value(4).toInt()).arg(query.value(5).toInt())
            .arg(query.value(6).toInt()).arg(query.value(7).toInt()).arg(query.value(8).toInt())
            .arg(query.value(9).toInt()).arg(query.value(10).toInt()).arg(query.value(11).toInt());

    mMosqClient->publish(QString("monitor/%1").arg(values.mac), msg.toUtf8(), 1, false);
}

bool Engine::updateAlarmsThresholds(const DatakeeperQueryParser::values &values)
{
    QString queryString = QString("SELECT update_alarms('%1', cast('%2' as timestamptz), %3, "
                                                        "%4, %5::SMALLINT, "
                                                        "%6, %7, %8::SMALLINT, "
                                                        "%9, %10, %11::SMALLINT, "
                                                        "%12, %13, %14::SMALLINT)")
                                 .arg(values.mac).arg(values.lastAlarmUpdate).arg(values.internment_id)
                                 .arg(values.spo2LT).arg(values.spo2AlarmDelayS)
                                 .arg(values.hRLT).arg(values.hRGT).arg(values.hRAlarmDelayS)
                                 .arg(values.bTLT).arg(values.bTGT).arg(values.bTAlarmDelayS)
                                 .arg(values.bPSysLT).arg(values.bPSysGT).arg(values.bPAlarmDelayS);

    QSqlQuery query;
    if(!query.exec(queryString))
    {
        qWarning() << QString("Failed to update alarms, error was: %1")
                      .arg(query.lastError().text());
        sendCommandResult(values, ResultError, ReasonFailedToExecuteQuery);
        return false;
    }

    if(query.size() != 1)
    {
        qWarning() << "update_alarms should have returned a value!";
        sendCommandResult(values, ResultError, ReasonFailedToExecuteQuery);

        return false;
    }

    query.next();
    bool ok;
    int result = query.value(0).toInt(&ok);

    if(ok && (result == 0))
    {
        sendCommandResult(values, ResultOk);

        // Notify there is an update.
        QString msg = QString("{\"signal\":\"alarms_thresholds_updated\", \"internment_id\":%1}").arg(values.internment_id);
        mMosqClient->publish(QStringLiteral("datakeeper/signals"), msg.toUtf8(), 1, false);
        return true;
    }

    if(!ok)
        qWarning() << "update_alarms: return value is not an integer";
    else if(result == 1)
        qWarning() << "update_alarms: could not find internment " << values.internment_id;
    else if(result == 2)
        qWarning() << "update_alarms: the timestamp has been changed while setting the alarms' thresholds";
    else
        qWarning() << "update_alarms: unknown exit code";

    sendCommandResult(values, ResultError, ReasonStoredProcedureError);
    return false;

}

void Engine::sendAlarmsRanges(const DatakeeperQueryParser::values &values)
{
    QString msg;

    int32_t bottom;
    int32_t top;

    msg = QString("{\"id\":\"%1\",").arg(values.id);
    msg.append(QString("\"command\":\"alarms_ranges\","));

    // SpO2.
    bottom = Settings::spO2EqOrLessPercBot();
    top = Settings::spO2EqOrLessPercTop();
    msg.append(QString("\"spo2_eq_or_less_perc\":\"%1,%2\",").arg(bottom).arg(top));

    bottom = Settings::spO2DelaySMin();
    top = Settings::spO2DelaySMax();
    msg.append(QString("\"spo2_delay_s\":\"%1,%2\",").arg(bottom).arg(top));

    // Heart rate.
    bottom = Settings::heartRateEqOrLessBPMBot();
    top = Settings::heartRateEqOrLessBPMTop();
    msg.append(QString("\"heart_rate_eq_or_less_bpm\":\"%1,%2\",").arg(bottom).arg(top));

    bottom = Settings::heartRateEqOrMoreBPMBot();
    top = Settings::heartRateEqOrMoreBPMTop();
    msg.append(QString("\"heart_rate_eq_or_more_bpm\":\"%1,%2\",").arg(bottom).arg(top));

    bottom = Settings::heartRateDelaySMin();
    top = Settings::heartRateDelaySMax();
    msg.append(QString("\"heart_rate_delay_s\":\"%1,%2\",").arg(bottom).arg(top));

    // Body temperature.
    bottom = Settings::bodyTempEqOrLessCelsBot();
    top = Settings::bodyTempEqOrLessCelsTop();
    msg.append(QString("\"body_temperature_eq_or_less_cels\":\"%1,%2\",").arg(bottom).arg(top));

    bottom = Settings::bodyTempEqOrMoreCelsBot();
    top = Settings::bodyTempEqOrMoreCelsTop();
    msg.append(QString("\"body_temperature_eq_or_more_cels\":\"%1,%2\",").arg(bottom).arg(top));

    bottom = Settings::bodyTempDelaySMin();
    top = Settings::bodyTempDelaySMax();
    msg.append(QString("\"body_temperature_delay_s\":\"%1,%2\",").arg(bottom).arg(top));

    // Blood pressure.
    bottom = Settings::bloodPressureSysEqOrLessBot();
    top = Settings::bloodPressureSysEqOrLessTop();
    msg.append(QString("\"blood_pressure_sys_eq_or_less\":\"%1,%2\",").arg(bottom).arg(top));

    bottom = Settings::bloodPressureSysEqOrMoreBot();
    top = Settings::bloodPressureSysEqOrMoreTop();
    msg.append(QString("\"blood_pressure_sys_eq_or_more\":\"%1,%2\",").arg(bottom).arg(top));

    bottom = Settings::bloodPressureDelaySMin();
    top = Settings::bloodPressureDelaySMax();
    msg.append(QString("\"blood_pressure_delay_s\":\"%1,%2\"}").arg(bottom).arg(top));

    mMosqClient->publish(QString("monitor/%1").arg(values.mac), msg.toUtf8(), 1, false);
}

/**
 * @brief Engine::sendCommandResult Send the result of a command.
 * @param values Set of data from a query.
 * @param result The end result of a command.
 * @param message Aditional message
 */
void Engine::sendCommandResult(const DatakeeperQueryParser::values &values, const CommandResults result, const ErrorReasons reason)
{
    if(values.mac.isEmpty() || values.id.isEmpty() || values.command.isEmpty())
        return;

    QString resultString;

    switch(result) {
    case ResultOk:
        resultString = QStringLiteral("OK");
    break;

    case ResultError:
        resultString = QStringLiteral("ERROR");
    break;

    case ResultUpdate:
        resultString = QStringLiteral("UPDATE");
    }


    QString msg;

    if(result != ResultError)
    {
        msg = QString("{\"id\":\"%1\",\"internment_id\":%2,\"command\":\"%3\",\"result\":\"%4\"}")
                     .arg(values.id).arg(values.internment_id).arg(values.command).arg(resultString);
    }
    else
    {
        msg = QString("{\"id\":\"%1\",\"internment_id\":%2,\"command\":\"%3\",\"result\":\"%4\",\"reason\":%5}")
                     .arg(values.id).arg(values.internment_id).arg(values.command).arg(resultString).arg(reason);
    }

    mMosqClient->publish(QString("monitor/%1").arg(values.mac), msg.toUtf8(), 1, false);
}

void Engine::updateDeviceLastSeenTimestamp(const QString &mac)
{
    QString queryString;
    queryString = QString("SELECT update_device_last_seen_timestamp('%1')").arg(mac);

    QSqlQuery query;
    if(!query.exec(queryString))
        qWarning() << "Update device last seen timestamp query failed with error:" << query.lastError().text();
}

void Engine::onTimerEvent()
{
    mCheckDeviceStatusCounter++;
    mDeleteDataCounter++;

    if(!checkDBConnection())
        return;

    generateAlarms();

    if(mCheckDeviceStatusCounter > CHECK_DEVICES_STATUS_TIMEOUT_S)
    {
        mCheckDeviceStatusCounter = 0;
        checkDeviceStatus();
    }

    if(mDeleteDataCounter > DELETE_OLD_DATA_TIMEOUT_S)
    {
        mDeleteDataCounter = 0;
        deleteOldData();
    }

    // Heartbeat.
    auto msg = QString("{\"time\":%1}").arg(QDateTime::currentDateTime().toSecsSinceEpoch());
    mMosqClient->publish(QString("datakeeper/heartbeat"), msg.toUtf8(), 1, false);
}

void Engine::checkDeviceStatus()
{
    auto curDT = QDateTime::currentDateTime();
    QString alarmType;
    QString payload;

    QString queryString("SELECT mac, date_time_last_seen, battmV from devices "
                        "WHERE mac IN "
                        "(SELECT mac FROM internments "
                        "WHERE date_time_ends IS NULL AND mac IS NOT NULL) "
                        "ORDER BY mac ASC");

    QSqlQuery query;
    if(!query.exec(queryString))
        qWarning() << "Failed to execute query device status query.";

    while(query.next())
    {
        auto lastSeen = query.value(1).toDateTime();
        lastSeen.setTimeZone(QTimeZone::systemTimeZone());
        auto battmV = query.value(2).toInt();

        QString devData = QString("{\"mac\":\"%1\",\"last_seen\":\"%2\",\"battmV\":%3")
                .arg(query.value(0).toString().remove(":"))
                .arg(lastSeen.toString(Qt::ISODate))
                .arg(battmV);

        int64_t deltaS = lastSeen.secsTo(curDT);

        // Classify the time.
        if(deltaS < 20)
            devData.append(",\"device_missing\":\"green\"");
        else if((deltaS >= 20) && (deltaS < 40))
            devData.append(",\"device_missing\":\"yellow\"");
        else if((deltaS < 60))
            devData.append(",\"device_missing\":\"orange\"");
        else
            devData.append(",\"device_missing\":\"red\"");

        // Now check the battery.
        /// \todo This is **JUST AN EXAMPLE**, see https://gitlab.com/mosimpa/documentation/-/issues/103
        if(battmV >= 8000)
            devData.append(",\"battery_low\":\"green\"},");
        else if(battmV >= 7600)
            devData.append(",\"battery_low\":\"orange\"},");
        else
            devData.append(",\"battery_low\":\"red\"},");

        payload.append(devData);
    }

    payload.remove(payload.lastIndexOf(','), 1);

    QString msg = QString("{\"devices\":[%1]}").arg(payload);
    mMosqClient->publish("datakeeper/alarms", msg.toUtf8(), 1, false);
}

void Engine::generateReport(const DatakeeperQueryParser::values &values)
{
    auto rgc = new ReportGeneratorController(values, this);
    QObject::connect(rgc, &ReportGeneratorController::generatorFinished, this, &Engine::handleReport);
    rgc->generateReport();
}

void Engine::refreshActiveInternments()
{
    QString queryString;
    queryString = QStringLiteral("SELECT internment_id FROM internments "
                                 "WHERE date_time_begins IS NOT NULL AND "
                                 "date_time_ends IS NULL");
    QSqlQuery query;
    if(!query.exec(queryString))
    {
        qWarning() << QString("Failed to update the list of active internments, error was: %1")
                      .arg(query.lastError().text());
        return;
    }

    // Query has suceed, clear the list now.
    mActiveInternments.clear();

    while(query.next())
        mActiveInternments.append(query.value(0).toInt());
}

void Engine::generateAlarms()
{
    struct Thresholds {
        db_id_t internmentId;
        spo2_t spO2LT;
        heart_rate_t hRLT;
        heart_rate_t hRGT;
        body_temp_t bTLT;
        body_temp_t bTGT;
        blood_pressure_t bPLT;
        blood_pressure_t bPGT;
        uint8_t spO2DelayS;
        uint8_t hRDelayS;
        uint8_t bTDelayS;
        uint8_t bPDelayS;
        bool spO2Alarm;
        bool hRAlarm;
        bool bTAlarm;
        bool bPAlarm;
    };
    QMap<db_id_t, Thresholds> thresholds;

    QStringList internmentAlarm;
    QSqlQuery query;
    QString queryString;
    const auto timestamp = QDateTime::currentDateTime().toSecsSinceEpoch();

    if(mActiveInternments.isEmpty())
        return;

    queryString = QStringLiteral("SELECT internment_id, max(modify_ts) "
                                 "FROM alarms WHERE internment_id IN (");

    for(int i = 0; i < mActiveInternments.size(); i++)
    {
        queryString.append(QString("%1,").arg(mActiveInternments.at(i)));
    }

    // Remove last comma.
    queryString = queryString.remove(queryString.lastIndexOf(','), 1);
    queryString.append(") GROUP BY internment_id");

    if(!runQuery(queryString, query, "retrieving alarms' thresholds"))
        return;

    // We have the latests entries for each internment, now retrieve the values.
    queryString = QStringLiteral("SELECT internment_id, spo2_alarm_lt, "
                                 "hr_alarm_lt, hr_alarm_gt, "
                                 "bt_alarm_lt, bt_alarm_gt, "
                                 "bp_sys_alarm_lt, bp_sys_alarm_gt, "
                                 "spo2_alarm_delay_s, hr_alarm_delay_s, "
                                 "bt_alarm_delay_s, bp_alarm_delay_s "
                                 "FROM alarms WHERE modify_ts IN "
                                 "(SELECT modify_ts FROM alarms WHERE ");
    while(query.next())
    {
        QDateTime modifyTs = query.value(1).toDateTime();
        modifyTs.setTimeZone(QTimeZone::systemTimeZone());
        queryString.append(QString("(internment_id = %1) AND "
                                   "modify_ts = cast('%2' as timestamptz) OR ")
                           .arg(query.value(0).toInt()).arg(modifyTs.toString(Qt::ISODateWithMs)));
    }

    queryString = queryString.remove(queryString.lastIndexOf("OR "), 3);
    queryString.append(")");

    if(!runQuery(queryString, query, "retrieving alarms' thresholds"))
        return;

    // Now load the thresholds into the vector.
    while(query.next())
    {
        Thresholds t;
        t.internmentId = query.value(0).toInt();
        t.spO2LT = static_cast<spo2_t>(query.value(1).toInt());
        t.hRLT = static_cast<heart_rate_t>(query.value(2).toInt());
        t.hRGT = static_cast<heart_rate_t>(query.value(3).toInt());
        t.bTLT = static_cast<body_temp_t>(query.value(4).toInt());
        t.bTGT = static_cast<body_temp_t>(query.value(5).toInt());
        t.bPLT = static_cast<blood_pressure_t>(query.value(6).toInt());
        t.bPGT = static_cast<blood_pressure_t>(query.value(7).toInt());
        t.spO2DelayS = static_cast<uint8_t>(query.value(8).toInt());
        t.hRDelayS = static_cast<uint8_t>(query.value(9).toInt());
        t.bTDelayS = static_cast<uint8_t>(query.value(10).toInt());
        t.bPDelayS = static_cast<uint8_t>(query.value(11).toInt());

        t.spO2Alarm = true;
        t.hRAlarm = true;
        t.bTAlarm = true;
        t.bPAlarm = true;

        thresholds[t.internmentId] = t;
    }

    // Check SpO2 alarms.
    queryString = QStringLiteral("SELECT internment_id, count(*) "
                          "FROM spo2_values WHERE (");
    QMap<db_id_t, Thresholds>::const_iterator thresholdIterator = thresholds.constBegin();
    while(thresholdIterator != thresholds.constEnd())
    {
        queryString.append(QString("(time > %1 AND "
                                   "internment_id = %2 AND "
                                   "spo2 > %3) OR ")
                           .arg(timestamp - thresholdIterator.value().spO2DelayS)
                           .arg(thresholdIterator.value().internmentId)
                           .arg(thresholdIterator.value().spO2LT));

        ++thresholdIterator;
    }
    queryString = queryString.remove(queryString.lastIndexOf("OR "), 3);
    queryString.append(") GROUP BY internment_id");

    if(!runQuery(queryString, query, "checking SpO2 alarms"))
        return;

    while(query.next())
    {
        auto t = thresholds.value(query.value(0).toInt());
        t.spO2Alarm = false;
        thresholds[t.internmentId] = t;
    }

    // Check heart rate alarms.
    queryString = QStringLiteral("SELECT internment_id, count(*) "
                          "FROM heart_rate_values WHERE (");
    thresholdIterator = thresholds.constBegin();
    while(thresholdIterator != thresholds.constEnd())
    {
        queryString.append(QString("(time > %1 AND "
                                   "internment_id = %2 AND "
                                   "heart_rate > %3 AND "
                                   "heart_rate < %4) OR ")
                           .arg(timestamp - thresholdIterator.value().hRDelayS)
                           .arg(thresholdIterator.value().internmentId)
                           .arg(thresholdIterator.value().hRLT)
                           .arg(thresholdIterator.value().hRGT));

        ++thresholdIterator;
    }
    queryString = queryString.remove(queryString.lastIndexOf("OR "), 3);
    queryString.append(") GROUP BY internment_id");

    if(!runQuery(queryString, query, "checking heart rate alarms"))
        return;

    while(query.next())
    {
        auto t = thresholds.value(query.value(0).toInt());
        t.hRAlarm = false;
        thresholds[t.internmentId] = t;
    }

    // Check body temperature alarms.
    queryString = QStringLiteral("SELECT internment_id, count(*) "
                          "FROM body_temperature_values WHERE (");
    thresholdIterator = thresholds.constBegin();
    while(thresholdIterator != thresholds.constEnd())
    {
        queryString.append(QString("(time > %1 AND "
                                   "internment_id = %2 AND "
                                   "temperature > %3 AND "
                                   "temperature < %4) OR ")
                           .arg(timestamp - thresholdIterator.value().bTDelayS)
                           .arg(thresholdIterator.value().internmentId)
                           .arg(thresholdIterator.value().bTLT)
                           .arg(thresholdIterator.value().bTGT));

        ++thresholdIterator;
    }
    queryString = queryString.remove(queryString.lastIndexOf("OR "), 3);
    queryString.append(") GROUP BY internment_id");

    if(!runQuery(queryString, query, "checking body temperature alarms"))
        return;

    while(query.next())
    {
        auto t = thresholds.value(query.value(0).toInt());
        t.bTAlarm = false;
        thresholds[t.internmentId] = t;
    }

    // Check blood pressure alarms.
    queryString = QStringLiteral("SELECT internment_id, count(*) "
                          "FROM blood_pressure_values WHERE (");
    thresholdIterator = thresholds.constBegin();
    while(thresholdIterator != thresholds.constEnd())
    {
        queryString.append(QString("(time > %1 AND "
                                   "internment_id = %2 AND "
                                   "systolic > %3 AND "
                                   "systolic < %4) OR ")
                           .arg(timestamp - thresholdIterator.value().bPDelayS)
                           .arg(thresholdIterator.value().internmentId)
                           .arg(thresholdIterator.value().bPLT)
                           .arg(thresholdIterator.value().bPGT));

        ++thresholdIterator;
    }
    queryString = queryString.remove(queryString.lastIndexOf("OR "), 3);
    queryString.append(") GROUP BY internment_id");

    if(!runQuery(queryString, query, "checking blood pressure alarms"))
        return;

    while(query.next())
    {
        auto t = thresholds.value(query.value(0).toInt());
        t.bPAlarm = false;
        thresholds[t.internmentId] = t;
    }

    // Now publish all the alarms that are set off.
    QString msg = QStringLiteral("{\"internments\":[");
    thresholdIterator = thresholds.constBegin();
    while(thresholdIterator != thresholds.constEnd())
    {
        if(!thresholdIterator->spO2Alarm &&
           !thresholdIterator->hRAlarm &&
           !thresholdIterator->bTAlarm &&
           !thresholdIterator->bPAlarm)
        {
            ++thresholdIterator;
            continue;
        }

        QString internment = QString("{\"id\":%1,").arg(thresholdIterator->internmentId);

        if(thresholdIterator->spO2Alarm)
            internment.append(QStringLiteral("\"spo2\":\"critic\","));

        if(thresholdIterator->hRAlarm)
            internment.append(QStringLiteral("\"heart_rate\":\"critic\","));

        if(thresholdIterator->bTAlarm)
            internment.append(QStringLiteral("\"body_temp\":\"critic\","));

        if(thresholdIterator->bPAlarm)
            internment.append(QStringLiteral("\"blood_pressure\":\"critic\","));

        // Remove last comma.
        internment = internment.remove(internment.lastIndexOf(','), 1);

        internment.append(QStringLiteral("},"));

        msg.append(internment);

        ++thresholdIterator;
    }

    // Remove last comma.
    auto index = msg.lastIndexOf(',');
    if(index > 0)
        msg = msg.remove(index, 1);
    msg.append(QStringLiteral("]}"));

    mMosqClient->publish("datakeeper/alarms", msg.toUtf8(), 1, false);
}

/// \todo Replace all possible queries to use this method.
bool Engine::runQuery(QString queryString, QSqlQuery &query, QString queryPurpose)
{
    if(!query.exec(queryString))
    {
        qWarning() << QString("Error while executing SQL query for %1. "
                              "Error was: %2")
                      .arg(queryPurpose).arg(query.lastError().text());
        return false;
    }

    return true;
}

void Engine::printValueAddDBDebug(QString type, const QString mac, const int retval)
{
    type.append(": ");

    switch(retval) {
    case 0:
        break;

    case 1:
        qWarning() << type << "device MAC address" << mac << "not known.";
        break;

    case 2:
        qWarning() << type << "MAC address not associated with an internment.";
        break;

    case 3:
        qWarning() << type << "more than one internments with the device" << mac << "associated to them and date_time_ends = NULL.";
        break;

    default:
        qWarning() << type << "return value "<< retval <<" is not known.";
        break;
    }
}

void Engine::addSpo2ToDB(const ReadsParser & parser)
{
    const QVector<ReadsParser::SpO2Values> values = parser.spO2Values();
    for(int i = 0; i < values.size(); i++)
    {
        QString queryString = QString("SELECT add_spo2('%1', %2, %3)")
                                     .arg(parser.mac())
                                     .arg(values.at(i).time)
                                     .arg(values.at(i).spO2);

        QSqlQuery query;
        if(!query.exec(queryString))
        {
            qWarning() << QString("Failed to add SpO2 info, error was: %1")
                          .arg(query.lastError().text());
            return;
        }

        if(query.size() == 1)
        {
            query.next();
            bool ok;
            int result = query.value(0).toInt(&ok);

            if(!ok)
                qWarning() << "add_spo2 return value is not an integer!";
            else
                printValueAddDBDebug("Adding SpO2", parser.mac(), result);
        }
        else
            qWarning() << "add_spo2 should have returned a value!";
    }

}

void Engine::addHeartRateToDB(const ReadsParser &parser)
{
    const QVector<ReadsParser::HeartRateValues> values = parser.heartRateValues();
    for(int i = 0; i < values.size(); i++)
    {
        QString queryString = QString("SELECT add_heart_rate('%1', %2, %3)")
                                     .arg(parser.mac())
                                     .arg(values.at(i).time)
                                     .arg(values.at(i).heartR);

        QSqlQuery query;
        if(!query.exec(queryString))
        {
            qWarning() << QString("Failed to add heart rate info, error was: %1")
                          .arg(query.lastError().text());
            return;
        }

        if(query.size() == 1)
        {
            query.next();
            bool ok;
            int result = query.value(0).toInt(&ok);

            if(!ok)
                qWarning() << "add_heart_rate return value is not an integer!";
            else
                printValueAddDBDebug("Adding heart rate", parser.mac(), result);
        }
        else
            qWarning() << "add_heart_rate should have returned a value!";
    }
}

void Engine::addBloodPressureToDB(const ReadsParser & parser)
{
    const QVector<ReadsParser::BloodPressureValues> values = parser.bloodPressureValues();
    for(int i = 0; i < values.size(); i++)
    {
        QString queryString = QString("SELECT add_blood_pressure('%1', %2, %3::SMALLINT, %4::SMALLINT)")
                                     .arg(parser.mac())
                                     .arg(values.at(i).time)
                                     .arg(values.at(i).sys)
                                     .arg(values.at(i).dias);

        QSqlQuery query;
        if(!query.exec(queryString))
        {
            qWarning() << QString("Failed to add blood pressure info, error was: %1")
                          .arg(query.lastError().text());
            return;
        }

        if(query.size() == 1)
        {
            query.next();
            bool ok;
            int result = query.value(0).toInt(&ok);

            if(!ok)
                qWarning() << "add_blood_pressure return value is not an integer!";
            else
                printValueAddDBDebug("Adding blood pressure", parser.mac(), result);
        }
        else
            qWarning() << "add_blood_pressure should have returned a value!";
    }
}

void Engine::addBodyTemperatureToDB(const ReadsParser &parser)
{
    const QVector<ReadsParser::BodyTempValues> values = parser.bodyTempValues();

    for(int i = 0; i < values.size(); i++)
    {
        QString queryString = QString("SELECT add_body_temperature('%1', %2, %3)")
                                     .arg(parser.mac())
                                     .arg(values.at(i).time)
                                     .arg(values.at(i).bodyTemp);

        QSqlQuery query;
        if(!query.exec(queryString))
        {
            qWarning() << QString("Failed to add body temperature info, error was: %1")
                          .arg(query.lastError().text());
            return;
        }

        if(query.size() == 1)
        {
            query.next();
            bool ok;
            int result = query.value(0).toInt(&ok);

            if(!ok)
                qWarning() << "add_body_temperature return value is not an integer!";
            else
                printValueAddDBDebug("Adding body temperature", parser.mac(), result);
        }
        else
            qWarning() << "add_body_temperature should have returned a value!";
    }
}
