/*
 * Copyright 2020 Lisandro Damián Nicanor Pérez Meyer <perezmeyer@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 */
#include <QDebug>
#include <QTimer>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>
#include <QString>
#include <QByteArray>
#include <QRegExp>
#include <QDateTime>

#include <mosimpaqt/mosquittoclient.h>
#include <mosimpaqt/definitions.h>

#include "settings.h"

#include "datakeeperqueryparser.h"

/**
 * @brief DatakeeperQueryParser::parseMessage
 * @param msg message to be parsed.
 * @param values struct containing the values retrieved.
 * -1 if they are not found.
 * mac will return the topic after "reads/".
 * @return true if parsed successfully, false otherwise.
 */
DatakeeperQueryParser::Commands DatakeeperQueryParser::parseMessage(mosquitto_message * msg, DatakeeperQueryParser::values &values)
{
    QByteArray payload(static_cast<char*>(msg->payload), msg->payloadlen);
    QJsonDocument jsonDoc = QJsonDocument::fromJson(payload);
    QRegExp macRx("^([0-9a-f]{2}){6}$");
    Q_ASSERT_X(macRx.isValid(), __FUNCTION__, "The MAC regular expression must be valid.");

    values.mac.clear();
    values.command.clear();
    values.id.clear();
    values.internment_id = -1;
    values.fromTime = -1;
    values.numOfSamples = -1;
    values.lastAlarmUpdate = "";

    struct values parsed = values;

    if(!jsonDoc.isObject())
    {
        qDebug() << "No JSON object received, discarding message with payload " << QString(payload);
        return CommandUnknown;
    }

    QJsonObject mainObject = jsonDoc.object();

    if(!mainObject.contains("mac") || !mainObject["mac"].isString())
        return CommandUnknown;

    if(!mainObject.contains("command") || !mainObject["command"].isString())
        return CommandUnknown;

    if(!mainObject.contains("id") || !mainObject["id"].isString())
        return CommandUnknown;

    parsed.mac = mainObject["mac"].toString("").toLower();
    if(!macRx.exactMatch(parsed.mac))
        return CommandUnknown;

    parsed.command = mainObject["command"].toString("");
    parsed.id = mainObject["id"].toString("");
    if(parsed.id.size() > 32)
        return CommandUnknown;

    if((parsed.command == "spo2") || (parsed.command == "heartR") ||
       (parsed.command == "bloodP") || (parsed.command == "bodyT"))
    {
        int64_t tmp;

        // Internment ID.
        if(!mainObject.contains("internment_id") || !mainObject["internment_id"].isDouble())
            return CommandUnknown;

        tmp = static_cast<int64_t>(mainObject["internment_id"].toDouble(static_cast<double>(std::numeric_limits<int32_t>::min()) - 3.0));
        if((tmp < DB_ID_MIN) || (tmp > DB_ID_MAX))
            return CommandUnknown;

        parsed.internment_id = static_cast<db_id_t>(tmp);

        // From time.
        if(!mainObject.contains("from_time") || !mainObject["from_time"].isDouble())
            return CommandUnknown;

        tmp = static_cast<int64_t>(mainObject["from_time"].toDouble(-1));

        if((tmp < 0) || (tmp > std::numeric_limits<int32_t>::max()))
            return CommandUnknown;

        parsed.fromTime = static_cast<time_t>(tmp);

        // Number of samples.
        if(!mainObject.contains("num_of_sam") || !mainObject["num_of_sam"].isDouble())
            return CommandUnknown;

        tmp = static_cast<int64_t>(mainObject["num_of_sam"].toDouble(-1));

        if((tmp < 0) || (tmp > std::numeric_limits<int32_t>::max()))
            return CommandUnknown;

        parsed.numOfSamples =  static_cast<int32_t>(tmp);
    }
    else if(parsed.command == "alarms_thresholds")
    {
        if(!mainObject.contains("internment_id") || !mainObject["internment_id"].isDouble())
            return CommandUnknown;

        parsed.internment_id = mainObject["internment_id"].toInt();
    }
    else if(parsed.command == "set_alarms")
    {
        int64_t tmp;

        if(!mainObject.contains("internment_id") || !mainObject["internment_id"].isDouble())
            return CommandUnknown;

        parsed.internment_id = mainObject["internment_id"].toInt();

        if(!mainObject.contains("alarms") || !mainObject["alarms"].isObject())
            return CommandUnknown;

        auto alarms = mainObject["alarms"].toObject();

        // Last alarm ID.
        if(!alarms.contains("last_update") || !alarms["last_update"].isString())
            return CommandUnknown;

        parsed.lastAlarmUpdate = alarms["last_update"].toString("");
        auto dt = QDateTime::fromString(parsed.lastAlarmUpdate, Qt::ISODate);
        if(!dt.isValid())
            return CommandUnknown;

        // SpO2 equal or less than.
        if(!alarms.contains("spo2_lt") || !alarms["spo2_lt"].isDouble())
            return CommandUnknown;

        tmp = static_cast<int64_t>(alarms["spo2_lt"].toDouble(Settings::instance().wireSpO2EqOrLessPercBot() - 1.0));

        if((tmp < Settings::instance().wireSpO2EqOrLessPercBot()) ||
           (tmp > Settings::instance().wireSpO2EqOrLessPercTop()))
            return CommandUnknown;

        parsed.spo2LT =  static_cast<spo2_t>(tmp);

        // SpO2 alarm delay.
        if(!alarms.contains("spo2_delay_s") || !alarms["spo2_delay_s"].isDouble())
            return CommandUnknown;

        tmp = static_cast<int64_t>(alarms["spo2_delay_s"].toDouble(Settings::instance().spO2DelaySMin() - 1.0));

        if((tmp < Settings::instance().spO2DelaySMin()) ||
           (tmp > Settings::instance().spO2DelaySMax()))
            return CommandUnknown;

        parsed.spo2AlarmDelayS =  static_cast<uint8_t>(tmp);

        // Heart rate equal or less.
        if(!alarms.contains("hr_lt") || !alarms["hr_lt"].isDouble())
            return CommandUnknown;

        tmp = static_cast<int64_t>(alarms["hr_lt"].toDouble(Settings::instance().heartRateEqOrLessWireBot() - 1.0));

        if((tmp < Settings::instance().heartRateEqOrLessWireBot()) ||
           (tmp > Settings::instance().heartRateEqOrLessWireTop()))
            return CommandUnknown;

        parsed.hRLT =  static_cast<heart_rate_t>(tmp);

        // Heart rate equal or more
        if(!alarms.contains("hr_gt") || !alarms["hr_gt"].isDouble())
            return CommandUnknown;

        tmp = static_cast<int64_t>(alarms["hr_gt"].toDouble(Settings::instance().heartRateEqOrMoreWireBot() - 1.0));

        if((tmp < Settings::instance().heartRateEqOrMoreWireBot()) ||
           (tmp > Settings::instance().heartRateEqOrMoreWireTop()))
            return CommandUnknown;

        parsed.hRGT =  static_cast<heart_rate_t>(tmp);

        // hRLT should be < hRGT.
        if(parsed.hRLT >= parsed.hRGT)
            return CommandUnknown;

        // Heart rate alarm delay.
        if(!alarms.contains("hr_delay_s") || !alarms["hr_delay_s"].isDouble())
            return CommandUnknown;

        tmp = static_cast<int64_t>(alarms["hr_delay_s"].toDouble(Settings::instance().heartRateDelaySMin() - 1.0));

        if((tmp < Settings::instance().heartRateDelaySMin()) ||
           (tmp > Settings::instance().heartRateDelaySMax()))
            return CommandUnknown;

        parsed.hRAlarmDelayS =  static_cast<uint8_t>(tmp);

        // Body temperature equal or less
        if(!alarms.contains("bt_lt") || !alarms["bt_lt"].isDouble())
            return CommandUnknown;

        tmp = static_cast<int64_t>(alarms["bt_lt"].toDouble(Settings::instance().bodyTempEqOrLessWireBot() - 1.0));

        if((tmp < Settings::instance().bodyTempEqOrLessWireBot()) ||
           (tmp > Settings::instance().bodyTempEqOrLessWireTop()))
            return CommandUnknown;

        parsed.bTLT =  static_cast<body_temp_t>(tmp);

        // Body temperature equal or greater.
        if(!alarms.contains("bt_gt") || !alarms["bt_gt"].isDouble())
            return CommandUnknown;

        tmp = static_cast<int64_t>(alarms["bt_gt"].toDouble(Settings::instance().bodyTempEqOrMoreWireBot() - 1.0));

        if((tmp < Settings::instance().bodyTempEqOrMoreWireBot()) ||
           (tmp > Settings::instance().bodyTempEqOrMoreWireTop()))
            return CommandUnknown;

        parsed.bTGT =  static_cast<body_temp_t>(tmp);

        // bTLT should be < bTGT.
        if(parsed.bTLT >= parsed.bTGT)
            return CommandUnknown;

        // Body temperature alarm delay.
        if(!alarms.contains("bt_delay_s") || !alarms["bt_delay_s"].isDouble())
            return CommandUnknown;

        tmp = static_cast<int64_t>(alarms["bt_delay_s"].toDouble(Settings::instance().bodyTempDelaySMin() - 1.0));

        if((tmp < Settings::instance().bodyTempDelaySMin()) ||
           (tmp > Settings::instance().bodyTempDelaySMax()))
            return CommandUnknown;

        parsed.bTAlarmDelayS =  static_cast<uint8_t>(tmp);

        // Blood pressure equal or less.
        if(!alarms.contains("bp_sys_lt") || !alarms["bp_sys_lt"].isDouble())
            return CommandUnknown;

        tmp = static_cast<int64_t>(alarms["bp_sys_lt"].toDouble(Settings::instance().bloodPressureSysEqOrLessBot() - 1.0));

        if((tmp < Settings::instance().bloodPressureSysEqOrLessBot()) ||
           (tmp > Settings::instance().bloodPressureSysEqOrLessTop()))
            return CommandUnknown;

        parsed.bPSysLT =  static_cast<blood_pressure_t>(tmp);

        // Blood pressure equal or greater.
        if(!alarms.contains("bp_sys_gt") || !alarms["bp_sys_gt"].isDouble())
            return CommandUnknown;

        tmp = static_cast<int64_t>(alarms["bp_sys_gt"].toDouble(Settings::instance().bloodPressureSysEqOrMoreBot() - 1.0));

        if((tmp < Settings::instance().bloodPressureSysEqOrMoreBot()) ||
           (tmp > Settings::instance().bloodPressureSysEqOrMoreTop()))
            return CommandUnknown;

        parsed.bPSysGT =  static_cast<blood_pressure_t>(tmp);

        if(parsed.bPSysLT >= parsed.bPSysGT)
            return CommandUnknown;

        // Blood pressure alarm delay.
        if(!alarms.contains("bp_delay_s") || !alarms["bp_delay_s"].isDouble())
            return CommandUnknown;

        tmp = static_cast<int64_t>(alarms["bp_delay_s"].toDouble(Settings::instance().bloodPressureDelaySMin() - 1.0));

        if((tmp < Settings::instance().bloodPressureDelaySMin()) ||
           (tmp > Settings::instance().bloodPressureDelaySMax()))
            return CommandUnknown;

        parsed.bPAlarmDelayS =  static_cast<uint8_t>(tmp);
    }
    else if(parsed.command == "gen_report")
    {
        int64_t tmp;

        if(!mainObject.contains("internment_id") || !mainObject["internment_id"].isDouble())
            return CommandUnknown;

        parsed.internment_id = mainObject["internment_id"].toInt();

        // From time.
        if(!mainObject.contains("from_time") || !mainObject["from_time"].isDouble())
            return CommandUnknown;

        tmp = static_cast<int64_t>(mainObject["from_time"].toDouble(-1));

        if((tmp < 0) || (tmp > std::numeric_limits<int32_t>::max()))
            return CommandUnknown;

        parsed.fromTime = static_cast<time_t>(tmp);

        // To time.
        if(!mainObject.contains("to_time") || !mainObject["to_time"].isDouble())
            return CommandUnknown;

        tmp = static_cast<int64_t>(mainObject["to_time"].toDouble(-1));

        if((tmp < parsed.fromTime + 3600) || (tmp > std::numeric_limits<int32_t>::max()))
            return CommandUnknown;

        parsed.toTime = static_cast<time_t>(tmp);
    }

    values = parsed;

    if(values.command == "spo2")
        return CommandSpO2;

    if(values.command == "heartR")
        return CommandHeartRate;

    if(values.command == "bloodP")
        return CommandBloodPressure;

    if(values.command == "bodyT")
        return CommandBodyTemperature;

    if(values.command == "alarms_thresholds")
        return CommandAlarmsThresholds;

    if(values.command == "set_alarms")
        return CommandSetAlarmsThresholds;

    if(values.command == "alarms_ranges")
        return CommandAlarmRanges;

    if(values.command == "internments")
        return CommandInternments;

    if(values.command == "gen_report")
        return CommandGenerateReport;

    return CommandUnknown;
}

