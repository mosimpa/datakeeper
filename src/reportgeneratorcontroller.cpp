#include <QDebug>

#include "reportgeneratorcontroller.h"

ReportGeneratorController::ReportGeneratorController(const DatakeeperQueryParser::values &values, QObject *parent) : QObject(parent)
{
    _thread = new QThread(this);
    _rG = new ReportGenerator();
    _rG->moveToThread(_thread);
    QObject::connect(_rG, &ReportGenerator::generatorFinished, this, &ReportGeneratorController::handleReport);

    _values = values;
}

void ReportGeneratorController::generateReport()
{
    _rG->generateReport(_values.internment_id, _values.fromTime, _values.toTime);
}

void ReportGeneratorController::handleReport(const bool success, const QString reportPath)
{
    emit generatorFinished(success, reportPath, _values, this);
}
