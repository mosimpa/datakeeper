#ifndef SETTINGS_H
#define SETTINGS_H

#include <QObject>
#include <QSettings>
#include <QString>

#include <mosimpaqt/definitions.h>
#include <mosimpaqt/scaling.h>

/**
 * @brief The Settings class
 *
 * Check https://stackoverflow.com/questions/1008019/c-singleton-design-pattern
 * on how the singleton pattern has been used.
 */

class Settings : public QObject
{
    Q_OBJECT
public:
    Settings(Settings const &) = delete;
    void operator=(Settings const &) = delete;
    static Settings & instance();

    QString dBHostname() { return mDBHostname; }
    QString database() { return mDatabase; }
    QString dBUserName() { return mDBUserName; }
    QString dBPassword() { return mDBPassword; }

    QString mqttBroker() { return mMqttBroker; }

    int32_t oldDataThresholdS() { return mOldDataThresholdS; }

    QString reportsHostname() { return mReportsHostname; }

    /// See https://gitlab.com/mosimpa/datakeeper/-/issues/41
    static int32_t spO2EqOrLessPercBot() { return 85; }
    static int32_t spO2EqOrLessPercTop() { return 95; }

    static uint8_t spO2DelaySMin() { return 15; }
    static uint8_t spO2DelaySMax() { return 60; }

    static int32_t heartRateEqOrLessBPMBot() { return 40; }
    static int32_t heartRateEqOrLessBPMTop() { return 60; }
    static int32_t heartRateEqOrMoreBPMBot () { return 100; }
    static int32_t heartRateEqOrMoreBPMTop () { return 140; }

    static uint8_t heartRateDelaySMin() { return 15; }
    static uint8_t heartRateDelaySMax() { return 60; }

    static int32_t bodyTempEqOrLessCelsBot() { return 35; }
    static int32_t bodyTempEqOrLessCelsTop() { return 37; }
    static int32_t bodyTempEqOrMoreCelsBot() { return 38; }
    static int32_t bodyTempEqOrMoreCelsTop() { return 40; }

    static uint8_t bodyTempDelaySMin() { return 15; }
    static uint8_t bodyTempDelaySMax() { return 60; }

    static blood_pressure_t bloodPressureSysEqOrLessBot() { return 100; }
    static blood_pressure_t bloodPressureSysEqOrLessTop() { return 120; }
    static blood_pressure_t bloodPressureSysEqOrMoreBot() { return 150; }
    static blood_pressure_t bloodPressureSysEqOrMoreTop() { return 170; }

    static uint8_t bloodPressureDelaySMin() { return 15; }
    static uint8_t bloodPressureDelaySMax() { return 60; }

    /// \todo This should be renamed to spO2EqOrLessWire[Bot Top].
    static spo2_t wireSpO2EqOrLessPercBot() { return Scaling::spO2PercentageToWire(spO2EqOrLessPercBot()); }
    static spo2_t wireSpO2EqOrLessPercTop() { return Scaling::spO2PercentageToWire(spO2EqOrLessPercTop()); }

    static heart_rate_t heartRateEqOrLessWireBot() { return Scaling::heartRateBPMToWire(heartRateEqOrLessBPMBot()); }
    static heart_rate_t heartRateEqOrLessWireTop() { return Scaling::heartRateBPMToWire(heartRateEqOrLessBPMTop()); }

    static heart_rate_t heartRateEqOrMoreWireBot() { return Scaling::heartRateBPMToWire(heartRateEqOrMoreBPMBot()); }
    static heart_rate_t heartRateEqOrMoreWireTop() { return Scaling::heartRateBPMToWire(heartRateEqOrMoreBPMTop()); }

    static body_temp_t bodyTempEqOrLessWireBot() { return Scaling::bodyTemperatureDegCelsiusToWire(bodyTempEqOrLessCelsBot()); }
    static body_temp_t bodyTempEqOrLessWireTop() { return Scaling::bodyTemperatureDegCelsiusToWire(bodyTempEqOrLessCelsTop()); }

    static body_temp_t bodyTempEqOrMoreWireBot() { return Scaling::bodyTemperatureDegCelsiusToWire(bodyTempEqOrMoreCelsBot()); }
    static body_temp_t bodyTempEqOrMoreWireTop() { return Scaling::bodyTemperatureDegCelsiusToWire(bodyTempEqOrMoreCelsTop()); }

private:
    Settings(QObject *parent = 0);
    ~Settings();

    void load();
    QString readOrSet(const QString & key, const QString &defaultValue);
    int32_t readOrSet(const QString & key, const int32_t &defaultValue);

    QString mDBHostname;
    QString mDatabase;
    QString mDBUserName;
    QString mDBPassword;

    QString mMqttBroker;

    /// Threshold in seconds that decides when data becomes old.
    int32_t mOldDataThresholdS;

    QString mReportsHostname;

    QSettings * mSettings;
};

#endif // SETTINGS_H
