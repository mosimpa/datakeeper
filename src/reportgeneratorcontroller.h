#ifndef REPORTGENERATORCONTROLLER_H
#define REPORTGENERATORCONTROLLER_H

#include <QObject>
#include <QThread>

#include <mosimpaqt/definitions.h>

#include "datakeeperqueryparser.h"
#include "reportgenerator.h"

using namespace MoSimPa;

class ReportGeneratorController : public QObject
{
    Q_OBJECT
public:
    explicit ReportGeneratorController(const DatakeeperQueryParser::values &values, QObject *parent = nullptr);

    void generateReport();

signals:
    void generatorFinished(const bool success, const QString reportUrl, const DatakeeperQueryParser::values &values, ReportGeneratorController * rgc);

private slots:
    void handleReport(const bool success, const QString reportPath);

private:
    QThread * _thread;
    ReportGenerator * _rG;
    DatakeeperQueryParser::values _values;
};

#endif // REPORTGENERATORCONTROLLER_H
