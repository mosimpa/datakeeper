#ifndef REPORTGENERATOR_H
#define REPORTGENERATOR_H

#include <QObject>
#include <QString>
#include <QTemporaryDir>
#include <QTemporaryFile>
#include <QVector>

#include <mosimpaqt/definitions.h>

using namespace MoSimPa;

class ReportGenerator : public QObject
{
    Q_OBJECT

public:

    ReportGenerator(QObject * parent = nullptr);

    void generateReport(const db_id_t internmentId, const time_t fromTime, const time_t toTime);

signals:
    void generatorFinished(const bool success, const QString reportUrl);

private:

    struct DBValues {
        time_t time;
        double average;
        double stdDev;
        double min;
        double max;
        int32_t numOfSamples;
    };

    QString spO2QueryString(const db_id_t internmentId, const time_t fromTime, const time_t toTime);
    QString heartRateQueryString(const db_id_t internmentId, const time_t fromTime, const time_t toTime);
    QString bodyTempQueryString(const db_id_t internmentId, const time_t fromTime, const time_t toTime);
    bool generatePlot(const QString & queryString,
                      const QString & title, const QString &yTitle,
                      const QTemporaryDir &tmpDir, const QString & imgFile,
                      QVector<DBValues> & values);
    bool gnuplotCommandsFile(QTemporaryFile &file, const QString &yTitle, const QString & imagePath, const QString &csvFile);
    bool generatePlotsPage(const db_id_t internmentId, const time_t fromTime, const time_t toTime, const QTemporaryDir &tempDir);
    bool generateTablePage(const QVector<DBValues> & spo2, const QVector<DBValues> & hr, const QVector<DBValues> & bt, const QTemporaryDir &tempDir);
    bool getTotalAverage(const QString & column, const QString & table, const time_t fromTime, const time_t toTime, double &average);

    /**
     * Reports will have plots periods averaged by this time span in seconds.
     * In this case 900 s = 15'.
     */
    const int REPORT_PLOT_PERIOD_S = 900;
};

#endif // REPORTGENERATOR_H
