<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es_ES">
<context>
    <name>QObject</name>
    <message>
        <location filename="../reportgenerator.cpp" line="54"/>
        <source>SpO2₂</source>
        <translation>SpO2₂</translation>
    </message>
    <message>
        <location filename="../reportgenerator.cpp" line="54"/>
        <source>Oxygen saturation [%]</source>
        <translation>Saturación de oxígeno [%]</translation>
    </message>
    <message>
        <location filename="../reportgenerator.cpp" line="58"/>
        <source>Heart rate</source>
        <translation>Ritmo cardíaco</translation>
    </message>
    <message>
        <location filename="../reportgenerator.cpp" line="58"/>
        <location filename="../reportgenerator.cpp" line="518"/>
        <source>Heart rate [BPM]</source>
        <translation>Ritmo cardíaco [BPM]</translation>
    </message>
    <message>
        <location filename="../reportgenerator.cpp" line="62"/>
        <location filename="../reportgenerator.cpp" line="357"/>
        <source>Body temperature</source>
        <translation>Temperatura corporal</translation>
    </message>
    <message>
        <location filename="../reportgenerator.cpp" line="62"/>
        <source>Temperature [ºC]</source>
        <translation>Temperatura [ºC]</translation>
    </message>
    <message>
        <location filename="../reportgenerator.cpp" line="183"/>
        <source>dateTime</source>
        <translation>fechaHora</translation>
    </message>
    <message>
        <location filename="../reportgenerator.cpp" line="184"/>
        <source>average</source>
        <translation>media</translation>
    </message>
    <message>
        <location filename="../reportgenerator.cpp" line="186"/>
        <source>minimum</source>
        <translation>mínimo</translation>
    </message>
    <message>
        <location filename="../reportgenerator.cpp" line="187"/>
        <source>maximum</source>
        <translation>máximo</translation>
    </message>
    <message>
        <location filename="../reportgenerator.cpp" line="259"/>
        <source>Date and time</source>
        <translation>Fecha y hora</translation>
    </message>
    <message>
        <location filename="../reportgenerator.cpp" line="185"/>
        <location filename="../reportgenerator.cpp" line="278"/>
        <source>std dev</source>
        <translation>desv est</translation>
    </message>
    <message>
        <location filename="../reportgenerator.cpp" line="188"/>
        <source>count</source>
        <translation>total</translation>
    </message>
    <message>
        <location filename="../reportgenerator.cpp" line="297"/>
        <source>Location not defined</source>
        <translation>Ubicación sin definir</translation>
    </message>
    <message>
        <location filename="../reportgenerator.cpp" line="326"/>
        <source>Local</source>
        <translation>Local</translation>
    </message>
    <message>
        <location filename="../reportgenerator.cpp" line="328"/>
        <source>External</source>
        <translation>Externa</translation>
    </message>
    <message>
        <location filename="../reportgenerator.cpp" line="340"/>
        <source>Pacient report - MoSimPa</source>
        <translation>Reporte de paciente - MoSimPa</translation>
    </message>
    <message>
        <location filename="../reportgenerator.cpp" line="342"/>
        <source>Internment ID</source>
        <translation>ID de internación</translation>
    </message>
    <message>
        <location filename="../reportgenerator.cpp" line="343"/>
        <source>Location</source>
        <translation>Ubicación</translation>
    </message>
    <message>
        <location filename="../reportgenerator.cpp" line="344"/>
        <source>Timespan from</source>
        <translation>Rango temporal desde</translation>
    </message>
    <message>
        <location filename="../reportgenerator.cpp" line="345"/>
        <source>To</source>
        <translation>hasta</translation>
    </message>
    <message>
        <location filename="../reportgenerator.cpp" line="346"/>
        <source>Generated on</source>
        <translation>Generado el</translation>
    </message>
    <message>
        <location filename="../reportgenerator.cpp" line="347"/>
        <source>Date and time expressed in %1.</source>
        <translation>Fecha y hora expresadas en %1.</translation>
    </message>
    <message>
        <location filename="../reportgenerator.cpp" line="349"/>
        <source>Oxigenometry (SpO₂)</source>
        <translation>Oxigenometría (SpO₂)</translation>
    </message>
    <message>
        <location filename="../reportgenerator.cpp" line="351"/>
        <location filename="../reportgenerator.cpp" line="355"/>
        <location filename="../reportgenerator.cpp" line="359"/>
        <source>Total average: %1</source>
        <translation>Promedio total: %1</translation>
    </message>
    <message>
        <location filename="../reportgenerator.cpp" line="353"/>
        <source>Heart Rate</source>
        <translation>Ritmo cardíaco</translation>
    </message>
    <message>
        <location filename="../reportgenerator.cpp" line="511"/>
        <source>Data table</source>
        <translation>Tabla de datos</translation>
    </message>
    <message>
        <location filename="../reportgenerator.cpp" line="516"/>
        <source>Date</source>
        <translation>Fecha</translation>
    </message>
    <message>
        <location filename="../reportgenerator.cpp" line="517"/>
        <source>SpO₂</source>
        <translation>SpO₂</translation>
    </message>
    <message>
        <location filename="../reportgenerator.cpp" line="519"/>
        <source>Body temperature [ºC]</source>
        <translation>Temperatura corporal [ºC]</translation>
    </message>
    <message>
        <location filename="../reportgenerator.cpp" line="522"/>
        <location filename="../reportgenerator.cpp" line="527"/>
        <location filename="../reportgenerator.cpp" line="532"/>
        <source>Average</source>
        <translation>Media</translation>
    </message>
    <message>
        <location filename="../reportgenerator.cpp" line="523"/>
        <location filename="../reportgenerator.cpp" line="528"/>
        <location filename="../reportgenerator.cpp" line="533"/>
        <source>Std dev</source>
        <translation>Desv est</translation>
    </message>
    <message>
        <location filename="../reportgenerator.cpp" line="524"/>
        <location filename="../reportgenerator.cpp" line="529"/>
        <location filename="../reportgenerator.cpp" line="534"/>
        <source>Min</source>
        <translation>Min</translation>
    </message>
    <message>
        <location filename="../reportgenerator.cpp" line="525"/>
        <location filename="../reportgenerator.cpp" line="530"/>
        <location filename="../reportgenerator.cpp" line="535"/>
        <source>Max</source>
        <translation>Máx</translation>
    </message>
    <message>
        <location filename="../reportgenerator.cpp" line="526"/>
        <location filename="../reportgenerator.cpp" line="531"/>
        <location filename="../reportgenerator.cpp" line="536"/>
        <source>Nº samples</source>
        <translation>Nº muestras</translation>
    </message>
    <message>
        <location filename="../reportgenerator.cpp" line="551"/>
        <location filename="../reportgenerator.cpp" line="552"/>
        <location filename="../reportgenerator.cpp" line="553"/>
        <location filename="../reportgenerator.cpp" line="554"/>
        <location filename="../reportgenerator.cpp" line="555"/>
        <location filename="../reportgenerator.cpp" line="569"/>
        <location filename="../reportgenerator.cpp" line="570"/>
        <location filename="../reportgenerator.cpp" line="571"/>
        <location filename="../reportgenerator.cpp" line="572"/>
        <location filename="../reportgenerator.cpp" line="573"/>
        <location filename="../reportgenerator.cpp" line="587"/>
        <location filename="../reportgenerator.cpp" line="588"/>
        <location filename="../reportgenerator.cpp" line="589"/>
        <location filename="../reportgenerator.cpp" line="590"/>
        <location filename="../reportgenerator.cpp" line="591"/>
        <source>N/A</source>
        <translation>N/A</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../settings.cpp" line="27"/>
        <source>%1: settings are being stored in %2</source>
        <translation>%1: la configuración se guarda en %2</translation>
    </message>
</context>
</TS>
