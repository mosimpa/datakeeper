#ifndef ENGINE_H
#define ENGINE_H

#include <QObject>
#include <QString>
#include <QVector>
#include <QSqlQuery>

#include <mosquitto.h>

#include <mosimpaqt/readsparser.h>

#include "devicesinfoparser.h"
#include "datakeeperqueryparser.h"
#include "reportgeneratorcontroller.h"

class MosquittoClient;
class QTimer;

class Engine : public QObject
{
    Q_OBJECT
public:

    enum CommandResults {
        ResultOk = 0,
        ResultError,
        ResultUpdate
    };

    enum ErrorReasons {
        ReasonNoReason = 0,
        ReasonCommandUnknown,
        ReasonReportGenerationFailed,
        ReasonFailedToExecuteQuery,
        ReasonStoredProcedureError,
    };

    explicit Engine(QObject *parent = nullptr);

public slots:
    void connect();

signals:

private slots:
    void onConnectionTried(int result);
    void onErrorWhileLooping(int error);
    void onMessageReceived();
    void parseMessage(struct mosquitto_message * msg);
    void deleteOldData();
    void internmentsChanged(const QString &);
    bool checkDBConnection();
    void handleReport(bool success, QString reportUrl, const DatakeeperQueryParser::values &values, ReportGeneratorController * rgc);

private:
    void addToDB(const DevicesInfoParser::values & values);
    void addToDB(const ReadsParser & readsParser);
    void execDatakeeperQuery(const DatakeeperQueryParser::Commands command, const DatakeeperQueryParser::values & values);
    void sendActiveInternments(const QString mac, const QString id);
    void sendSpO2Data(const DatakeeperQueryParser::values &values);
    void sendHeartRate2Data(const DatakeeperQueryParser::values &values);
    void sendBloodPressureData(const DatakeeperQueryParser::values &values);
    void sendBodyTemperatureData(const DatakeeperQueryParser::values &values);
    void sendAlarmsThresholds(const DatakeeperQueryParser::values &values);
    bool updateAlarmsThresholds(const DatakeeperQueryParser::values &values);
    void sendAlarmsRanges(const DatakeeperQueryParser::values &values);
    void sendCommandResult(const DatakeeperQueryParser::values &values,
                           const CommandResults result,
                           const ErrorReasons reason = ReasonNoReason);
    void updateDeviceLastSeenTimestamp(const QString & mac);
    void onTimerEvent();
    void checkDeviceStatus();
    void generateReport(const DatakeeperQueryParser::values &values);
    void refreshActiveInternments();
    void generateAlarms();
    bool runQuery(QString queryString, QSqlQuery & query, QString queryPurpose);

    static void printValueAddDBDebug(QString type, const QString mac, const int retval);

    void addSpo2ToDB(const ReadsParser & parser);
    void addHeartRateToDB(const ReadsParser & parser);
    void addBloodPressureToDB(const ReadsParser &parser);
    void addBodyTemperatureToDB(const ReadsParser & parser);

    static QString internmentsChangedNotificationString() { return  QStringLiteral("internments_changed"); }

    MosquittoClient * mMosqClient;
    QTimer * mGeneralTimer;
    int32_t mDeleteDataCounter;
    int32_t mCheckDeviceStatusCounter;
    bool mConnectionWasLost;

    QVector<db_id_t> mActiveInternments;

    static const int TIMEOUT_MS = 1000;
    // Check for old data every hour.
    static const int DELETE_OLD_DATA_TIMEOUT_S = 60*60;
    static const int CHECK_DEVICES_STATUS_TIMEOUT_S = 5;

    friend class DBTests;
    friend class EngineTests;
};

#endif // ENGINE_H
