#include <QString>
#include <QSqlQuery>
#include <QSqlError>
#include <QDateTime>
#include <QDebug>
#include <QTemporaryDir>
#include <QTemporaryFile>
#include <QTextStream>
#include <QProcess>
#include <QStringList>
#include <QTimeZone>
#include <QDir>
#include <QHostInfo>

#include <mosimpaqt/scaling.h>

#include "settings.h"
#include "reportgenerator.h"

/**
 * @brief ReportGenerator::generateReport
 * @param internmentId
 * @param fromTime
 * @param toTime
 * @param reportPath
 * @return
 *
 * Implementation details: note that we use QTemporary[Dir File] so as to avoid
 * leaking patient's data into the filesystem.
 *
 * \todo Consider running this in it's own thread.
 */
ReportGenerator::ReportGenerator(QObject *parent) : QObject(parent)
{
}

void ReportGenerator::generateReport(db_id_t internmentId, time_t fromTime, time_t toTime)
{
    QString reportHostname = QStringLiteral("NOT GENERATED");
    QVector<DBValues> spO2Values;
    QVector<DBValues> heartRateValues;
    QVector<DBValues> bodyTempValues;

    QString queryString;

    QTemporaryDir dir;
    if(!dir.isValid())
    {
        reportHostname = dir.errorString();
        emit generatorFinished(false, reportHostname);
    }

    queryString = spO2QueryString(internmentId, fromTime, toTime);
    if(!generatePlot(queryString, QObject::tr("SpO2₂"), QObject::tr("Oxygen saturation [%]"), dir, "spo2.png", spO2Values))
        emit generatorFinished(false, reportHostname);

    queryString = heartRateQueryString(internmentId, fromTime, toTime);
    if(!generatePlot(queryString, QObject::tr("Heart rate"), QObject::tr("Heart rate [BPM]"), dir, "hr.png", heartRateValues))
        emit generatorFinished(false, reportHostname);

    queryString = bodyTempQueryString(internmentId, fromTime, toTime);
    if(!generatePlot(queryString, QObject::tr("Body temperature"), QObject::tr("Temperature [ºC]"), dir, "bt.png", bodyTempValues))
        emit generatorFinished(false, reportHostname);

    // Each page is generated in a different PDF in order to assure the page break.
    if(!generatePlotsPage(internmentId, fromTime, toTime, dir))
        emit generatorFinished(false, reportHostname);

    if(!generateTablePage(spO2Values, heartRateValues, bodyTempValues, dir))
        emit generatorFinished(false, reportHostname);

    // Check that the destination directory exists.
    QDir reportsDir(QDir::homePath() + "/reports/");
    if(!reportsDir.exists())
    {
        QDir homeDir(QDir::homePath());
        if(!homeDir.mkdir("reports"))
            emit generatorFinished(false, reportHostname);
    }

    QString reportFileName = QString("%1_%2_%3.pdf").arg(internmentId).arg(fromTime).arg(toTime);
    QString reportPath = reportsDir.absolutePath() + "/" + reportFileName;

    // Concatenate the PDFs.
    // Blocking process.
    QProcess pdfunite;
    pdfunite.setWorkingDirectory(dir.path());
    pdfunite.start("pdfunite", QStringList() << dir.filePath("page_1.pdf") << dir.filePath("page_2.pdf") << reportPath);

    if(!pdfunite.waitForStarted(5000))
    {
        qDebug() << "Not started";
        emit generatorFinished(false, reportHostname);
    }

    if(!pdfunite.waitForFinished(10000))
    {
        qDebug() << "Not finished";
        emit generatorFinished(false, reportHostname);
    }

    // Everything worked, create the URL.
    reportHostname = Settings::instance().reportsHostname();
    if(reportHostname == QStringLiteral("localhost"))
        reportHostname = QHostInfo::localHostName();
    emit generatorFinished(true, QString("https://%1/reports/%2").arg(reportHostname).arg(reportFileName));
}

QString ReportGenerator::spO2QueryString(const db_id_t internmentId, const time_t fromTime, const time_t toTime)
{
    // If this assertion fails then the query needs to get modififed with the proper scaling.
    Q_ASSERT(qFuzzyCompare(Scaling::spO2WireToPercentage(100), 10.0));
    return QString("SELECT * FROM ( "
                   "    SELECT internment_id, generate_series(min(time), max(time), %1) AS ts "
                   "    FROM spo2_values WHERE internment_id = %2 "
                   "    AND time >= %3 AND time < %4 GROUP by 1) grid "
                   "CROSS JOIN LATERAL ( "
                   "    SELECT round(avg(spo2)/10.0, 2) AS avg, "
                   "           round(stddev_samp(spo2)/10.0, 2) AS stddev, "
                   "           round(min(spo2)/10.0, 2) AS min, "
                   "           round(max(spo2)/10.0, 2) AS max, "
                   "           count(spo2) AS num "
                   "    FROM spo2_values "
                   "    WHERE internment_id = grid.internment_id AND"
                   "          time >= grid.ts AND time < grid.ts + %1) avg;")
            .arg(REPORT_PLOT_PERIOD_S).arg(internmentId).arg(fromTime).arg(toTime);
}

QString ReportGenerator::heartRateQueryString(const db_id_t internmentId, const time_t fromTime, const time_t toTime)
{
    // If this assertion fails then the query needs to get modififed with the proper scaling.
    Q_ASSERT(qFuzzyCompare(Scaling::heartRateWireToBPM(100), 10.0));
    return QString("SELECT * FROM ( "
                   "    SELECT internment_id, generate_series(min(time), max(time), %1) AS ts "
                   "    FROM heart_rate_values WHERE internment_id = %2 "
                   "    AND time >= %3 AND time < %4 GROUP by 1) grid "
                   "CROSS JOIN LATERAL ( "
                   "    SELECT round(avg(heart_rate)/10.0, 2) AS avg, "
                   "           round(stddev_samp(heart_rate)/10.0, 2) AS stddev, "
                   "           round(min(heart_rate)/10.0, 2) AS min, "
                   "           round(max(heart_rate)/10.0, 2) AS max, "
                   "           count(heart_rate) AS num "
                   "    FROM heart_rate_values "
                   "    WHERE internment_id = grid.internment_id AND"
                   "          time >= grid.ts AND time < grid.ts + %1) avg;")
            .arg(REPORT_PLOT_PERIOD_S).arg(internmentId).arg(fromTime).arg(toTime);
}

QString ReportGenerator::bodyTempQueryString(const db_id_t internmentId, const time_t fromTime, const time_t toTime)
{
    // If this assertion fails then the query needs to get modififed with the proper scaling.
    Q_ASSERT(qFuzzyCompare(Scaling::bodyTemperatureWireToDegCelsius(100), 10.0));
    return QString("SELECT * FROM ( "
                   "    SELECT internment_id, generate_series(min(time), max(time), %1) AS ts "
                   "    FROM body_temperature_values WHERE internment_id = %2 "
                   "    AND time >= %3 AND time < %4 GROUP by 1) grid "
                   "CROSS JOIN LATERAL ( "
                   "    SELECT round(avg(temperature)/10.0, 2) AS avg, "
                   "           round(stddev_samp(temperature)/10.0, 2) AS stddev, "
                   "           round(min(temperature)/10.0, 2) AS min, "
                   "           round(max(temperature)/10.0, 2) AS max, "
                   "           count(temperature) AS num "
                   "    FROM body_temperature_values "
                   "    WHERE internment_id = grid.internment_id AND"
                   "          time >= grid.ts AND time < grid.ts + %1) avg;")
            .arg(REPORT_PLOT_PERIOD_S).arg(internmentId).arg(fromTime).arg(toTime);
}

bool ReportGenerator::generatePlot(const QString &queryString,
                                   const QString &title, const QString &yTitle,
                                   const QTemporaryDir & tmpDir, const QString &imgFile,
                                   QVector<DBValues> &values)
{
    values.clear();

    QTemporaryFile cvsFile(tmpDir.filePath("XXXXXX.csv"));
    if(!cvsFile.open())
    {
        return false;
    }

    QTextStream cvsOut(&cvsFile);
    cvsOut << "\"" << QObject::tr("dateTime") << "\",";
    cvsOut << "\"" << QObject::tr("average") << "\",";
    cvsOut << "\"" << QObject::tr("std dev") << "\",";
    cvsOut << "\"" << QObject::tr("minimum") << "\",";
    cvsOut << "\"" << QObject::tr("maximum") << "\",";
    cvsOut << "\"" << QObject::tr("count") << "\"," << "\n";
    cvsOut << QString("# %1\n").arg(title);

    QSqlQuery query;
    if(!query.exec(queryString))
    {
        qWarning() << QString("Failed to generate report, error was: %1")
                      .arg(query.lastError().text());
        return false;
    }

    while(query.next())
    {
        auto dateTime = QDateTime::fromSecsSinceEpoch(query.value(1).toInt(), Qt::LocalTime);
        cvsOut << "\"" << dateTime.toString(Qt::ISODate) << "\",\""
               << query.value(2).toDouble() << "\",\""
               << query.value(3).toDouble() << "\",\""
               << query.value(4).toDouble() << "\",\""
               << query.value(5).toDouble() << "\"\n";

        DBValues value;
        value.time = query.value(1).toInt();
        value.average = query.value(2).toDouble();
        value.stdDev = query.value(3).toDouble();
        value.min = query.value(4).toDouble();
        value.max = query.value(5).toDouble();
        value.numOfSamples = query.value(6).toInt();

        values.append(value);
    }

    cvsFile.flush();
    cvsFile.close();

    QTemporaryFile gplotFile(tmpDir.filePath("XXXXXX.gnuplot"));
    QString imagePath = tmpDir.filePath(imgFile);

    if(!gnuplotCommandsFile(gplotFile, yTitle, imagePath, cvsFile.fileName()))
        return false;

    // Blocking process.
    QProcess gnuplot;
    gnuplot.start("gnuplot", QStringList() << gplotFile.fileName());

    if(!gnuplot.waitForStarted(5000))
    {
        qDebug() << "Not started";
        return false;
    }

    if(!gnuplot.waitForFinished(5000))
    {
        qDebug() << "Not finished";
        return false;
    }

    return true;
}

bool ReportGenerator::gnuplotCommandsFile(QTemporaryFile & file, const QString & yTitle, const QString &imagePath, const QString & csvFile)
{
    if(!file.open())
        return false;

    QTextStream out(&file);

    out << QStringLiteral("set datafile separator ','\n");
    out << QStringLiteral("set xdata time # tells gnuplot the x axis is time data\n");
    out << QStringLiteral("set timefmt \"%Y-%m-%dT%H:%M:%S\"\n");
    out << QStringLiteral("set key autotitle columnhead\n");
    out << QString("set ylabel \"%1\"\n").arg(yTitle);
    out << QStringLiteral("set xlabel '") << QObject::tr("Date and time") << QStringLiteral("'\n");

    out << QStringLiteral("set style line 100 lt 1 lc rgb \"grey\" lw 0.5 # linestyle for the grid\n");
    out << QStringLiteral("set grid ls 100 # enable grid with specific linestyle\n");
    out << QStringLiteral("set xtics 3600 # an hour\n");

    out << QStringLiteral("set style line 101 lw 4 lt rgb \"#f62aa0\" # style for average (1) (pink)\n");
    out << QStringLiteral("set style line 102 lw 3 lt rgb \"#26dfd0\" # style for std dev (2) (light blue)\n");
    out << QStringLiteral("set style line 103 lw 3 lt rgb \"#dfd325\"\n");
    out << QStringLiteral("set style line 104 lw 3 lt rgb \"#15df1b\"\n");
    out << QStringLiteral("\n");
    out << QStringLiteral("set xtics rotate # rotate labels on the x axis\n");
    out << QStringLiteral("set key below # legend placement\n");

    /// \todo Set font...
    out << QStringLiteral("set terminal pngcairo size 1024,400 enhanced font 'Noto Sans,10'\n");
    out << QString("set output '%1'\n").arg(imagePath);
    out << QStringLiteral("\n");
    out << QString("plot '%1' using 1:2 with lines ls 101, '' using 1:2:3 title '").arg(csvFile)
        << QObject::tr("std dev") << QStringLiteral("' with yerrorbars ls 102, ")
        << QString("'' using 1:4 ls 103, '' using 1:5 ls 104\n");

    file.flush();
    file.close();

    return true;
}

bool ReportGenerator::generatePlotsPage(const db_id_t internmentId, const time_t fromTime, const time_t toTime, const QTemporaryDir & tempDir)
{
    // If any of this fails check the total averages below.
    Q_ASSERT(qFuzzyCompare(Scaling::spO2WireToPercentage(100), 10.0));
    Q_ASSERT(qFuzzyCompare(Scaling::heartRateWireToBPM(100), 10.0));
    Q_ASSERT(qFuzzyCompare(Scaling::heartRateWireToBPM(100), 10.0));

    double spo2Avg;
    double hrAvg;
    double btAvg;
    QString locationType = QObject::tr("Location not defined");
    auto fromDateTime = QDateTime::fromSecsSinceEpoch(fromTime, Qt::LocalTime);
    auto toDateTime = QDateTime::fromSecsSinceEpoch(toTime, Qt::LocalTime);

    QTemporaryFile page(tempDir.filePath("XXXXXX.md"));
    if(!page.open())
    {
        return false;
    }

    QTextStream out(&page);

    // Get the location.
    QString queryString = QString("SELECT l.description, l.type "
                                  "FROM locations l, internments i "
                                  "WHERE i.internment_id = %1 AND "
                                  "      i.location_id = l.location_id").arg(internmentId);
    QSqlQuery query;
    if(!query.exec(queryString))
    {
        qWarning() << QString("Failed to obtain location for generating a report, error was: %1")
                      .arg(query.lastError().text());
        return false;
    }

    if(!query.next())
        return false;

    if(query.value(1) == QStringLiteral("local"))
        locationType = QObject::tr("Local");
    else if(query.value(1) == QStringLiteral("external"))
        locationType = QObject::tr("External");

    // Get the total average for each value.
    if(!getTotalAverage("spo2", "spo2_values", fromTime, toTime, spo2Avg))
        return false;

    if(!getTotalAverage("heart_rate", "heart_rate_values", fromTime, toTime, hrAvg))
        return false;

    if(!getTotalAverage("temperature", "body_temperature_values", fromTime, toTime, btAvg))
        return false;

    out << "# " << QObject::tr("Pacient report - MoSimPa") << endl;
    out << endl;
    out << "**" << QObject::tr("Internment ID") << ":** " << internmentId << "<br>" << endl;
    out << "**" << QObject::tr("Location") << ":** " << query.value(0).toString() << " - " << locationType << "<br>" << endl;
    out << "**" << QObject::tr("Timespan from") << ":** " << fromDateTime.toString(Qt::ISODate)
        << " **" << QObject::tr("To") << ":** " << toDateTime.toString(Qt::ISODate) << "<br>" << endl;
    out << " **" << QObject::tr("Generated on") << ":** " << QDateTime::currentDateTime().toString(Qt::ISODate) << "<br>";
    out << QObject::tr("Date and time expressed in %1.").arg(QTimeZone::systemTimeZone().displayName(QTimeZone::StandardTime)) << endl;
    out << endl;
    out << "## " << QObject::tr("Oxigenometry (SpO₂)") << endl;
    out << QString("![](%1/spo2.png)").arg(tempDir.path()) << endl;
    out << QString(QObject::tr("Total average: %1")).arg(spo2Avg/10.0) << endl;
    out << endl;
    out << "## " << QObject::tr("Heart Rate") << endl;
    out << QString("![](%1/hr.png)").arg(tempDir.path()) << endl;
    out << QString(QObject::tr("Total average: %1")).arg(hrAvg/10.0) << endl;
    out << endl;
    out << "## " << QObject::tr("Body temperature") << endl;
    out << QString("![](%1/bt.png)").arg(tempDir.path()) << endl;
    out << QString(QObject::tr("Total average: %1")).arg(btAvg/10.0) << endl;

    page.flush();
    page.close();

    // Blocking process.
    QProcess pandoc;
    pandoc.setWorkingDirectory(tempDir.path());
    pandoc.start("pandoc", QStringList() << page.fileName() << "--pdf-engine=wkhtmltopdf" << "-opage_1.pdf");

    if(!pandoc.waitForStarted(5000))
    {
        qDebug() << "Not started";
        return false;
    }

    if(!pandoc.waitForFinished(10000))
    {
        qDebug() << "Not finished";
        return false;
    }

    return true;
}

bool ReportGenerator::generateTablePage(const QVector<DBValues> & spo2, const QVector<DBValues> & hr, const QVector<DBValues> & bt, const QTemporaryDir &tempDir)
{
    // Load all data in a single structure.

    struct Row {
        double spO2Avg;
        double spO2StdDev;
        double spO2Min;
        double spO2Max;
        double hrAvg;
        double hrStdDev;
        double hrMin;
        double hrMax;
        double btAvg;
        double btStdDev;
        double btMin;
        double btMax;
        int spO2NumOfSamp;
        int hrNumOfSamp;
        int btNumOfSamp;
    };

    QMap<time_t, Row> rows;

    // All rows in SpO2 will be new.
    for(int i = 0; i < spo2.size(); i++)
    {
        Row row;
        row.spO2Avg = spo2.at(i).average;
        row.spO2StdDev = spo2.at(i).stdDev;
        row.spO2Min = spo2.at(i).min;
        row.spO2Max = spo2.at(i).max;
        row.hrAvg = -1.0;
        row.hrStdDev = -1.0;
        row.hrMin = -1.0;
        row.hrMax = -1.0;
        row.btAvg = -1.0;
        row.btStdDev = -1.0;
        row.btMin = -1.0;
        row.btMax = -1.0;
        row.spO2NumOfSamp = spo2.at(i).numOfSamples;
        row.hrNumOfSamp = -1;
        row.btNumOfSamp = -1;

        rows.insert(spo2.at(i).time, row);
    }

    // Heart rate data.
    for(int i= 0; i< hr.size(); i++)
    {
        time_t time = hr.at(i).time;

        if(rows.contains(time))
        {
            rows[time].hrAvg = hr.at(i).average;
            rows[time].hrStdDev = hr.at(i).stdDev;
            rows[time].hrMin = hr.at(i).min;
            rows[time].hrMax = hr.at(i).max;
            rows[time].hrNumOfSamp = hr.at(i).numOfSamples;
        }
        else
        {
            Row row;
            row.spO2Avg = -1.0;
            row.spO2StdDev = -1.0;
            row.spO2Min = 1.0;
            row.spO2Max = -1.0;
            row.hrAvg = hr.at(i).average;
            row.hrStdDev = hr.at(i).stdDev;
            row.hrMin = hr.at(i).min;
            row.hrMax = hr.at(i).max;
            row.btAvg = -1.0;
            row.btStdDev = -1.0;
            row.btMin = -1.0;
            row.btMax = -1.0;
            row.spO2NumOfSamp = -1;
            row.hrNumOfSamp = hr.at(i).numOfSamples;
            row.btNumOfSamp = -1;

            rows.insert(hr.at(i).time, row);
        }
    }

    // Body temperature data.
    for(int i= 0; i< bt.size(); i++)
    {
        time_t time = bt.at(i).time;

        if(rows.contains(time))
        {
            rows[time].btAvg = bt.at(i).average;
            rows[time].btStdDev = bt.at(i).stdDev;
            rows[time].btMin = bt.at(i).min;
            rows[time].btMax = bt.at(i).max;
            rows[time].btNumOfSamp = bt.at(i).numOfSamples;
        }
        else
        {
            Row row;
            row.spO2Avg = -1.0;
            row.spO2StdDev = -1.0;
            row.spO2Min = 1.0;
            row.spO2Max = -1.0;
            row.hrAvg = -1.0;
            row.hrStdDev = -1.0;
            row.hrMin = -1.0;
            row.hrMax = -1.0;
            row.btAvg = bt.at(i).average;
            row.btStdDev = bt.at(i).stdDev;
            row.btMin = bt.at(i).min;
            row.btMax = bt.at(i).max;
            row.spO2NumOfSamp = -1;
            row.hrNumOfSamp = -1;
            row.btNumOfSamp = bt.at(i).numOfSamples;

            rows.insert(bt.at(i).time, row);
        }
    }

    QTemporaryFile page(tempDir.filePath("XXXXXX.md"));
    if(!page.open())
    {
        return false;
    }

    QTextStream out(&page);

    out << "## "<< QObject::tr("Data table") << endl;
    out << "<table border=\"1\">" << endl;

    // Headers
    out << "<tr>" << endl;
    out << "  <td rowspan=\"2\">" << QObject::tr("Date") << "</td>" << endl;
    out << "  <td colspan=\"5\">" << QObject::tr("SpO₂") << " [%]</td>" << endl;
    out << "  <td colspan=\"5\">" << QObject::tr("Heart rate [BPM]") << "</td>" << endl;
    out << "  <td colspan=\"5\">" << QObject::tr("Body temperature [ºC]") << "</td>" << endl;
    out << "</tr>" << endl;
    out << "<tr>" << endl;
    out << "  <td>" << QObject::tr("Average") << "</td>" << endl;
    out << "  <td>" << QObject::tr("Std dev") << "</td>" << endl;
    out << "  <td>" << QObject::tr("Min") << "</td>" << endl;
    out << "  <td>" << QObject::tr("Max") << "</td>" << endl;
    out << "  <td>" << QObject::tr("Nº samples") << "</td>" << endl;
    out << "  <td>" << QObject::tr("Average") << "</td>" << endl;
    out << "  <td>" << QObject::tr("Std dev") << "</td>" << endl;
    out << "  <td>" << QObject::tr("Min") << "</td>" << endl;
    out << "  <td>" << QObject::tr("Max") << "</td>" << endl;
    out << "  <td>" << QObject::tr("Nº samples") << "</td>" << endl;
    out << "  <td>" << QObject::tr("Average") << "</td>" << endl;
    out << "  <td>" << QObject::tr("Std dev") << "</td>" << endl;
    out << "  <td>" << QObject::tr("Min") << "</td>" << endl;
    out << "  <td>" << QObject::tr("Max") << "</td>" << endl;
    out << "  <td>" << QObject::tr("Nº samples") << "</td>" << endl;
    out << "</tr>" << endl;

    QMap<time_t, Row>::const_iterator i = rows.constBegin();

    while (i != rows.constEnd())
    {
        auto dt = QDateTime::fromSecsSinceEpoch(i.key(), Qt::LocalTime);

        out << "<tr>" << endl;
        out << "  <td>" << dt.toString("yyyy-MM-dd hh:mm") << "</td>" << endl;

        // SpO2
        if(i.value().spO2NumOfSamp < 0)
        {
            out << "  <td>" << QObject::tr("N/A") << "</td>" << endl;
            out << "  <td>" << QObject::tr("N/A") << "</td>" << endl;
            out << "  <td>" << QObject::tr("N/A") << "</td>" << endl;
            out << "  <td>" << QObject::tr("N/A") << "</td>" << endl;
            out << "  <td>" << QObject::tr("N/A") << "</td>" << endl;
        }
        else
        {
            out << "  <td>" << i.value().spO2Avg << "</td>" << endl;
            out << "  <td>" << i.value().spO2StdDev << "</td>" << endl;
            out << "  <td>" << i.value().spO2Min << "</td>" << endl;
            out << "  <td>" << i.value().spO2Max << "</td>" << endl;
            out << "  <td>" << i.value().spO2NumOfSamp << "</td>" << endl;
        }

        // HR
        if(i.value().hrNumOfSamp < 0)
        {
            out << "  <td>" << QObject::tr("N/A") << "</td>" << endl;
            out << "  <td>" << QObject::tr("N/A") << "</td>" << endl;
            out << "  <td>" << QObject::tr("N/A") << "</td>" << endl;
            out << "  <td>" << QObject::tr("N/A") << "</td>" << endl;
            out << "  <td>" << QObject::tr("N/A") << "</td>" << endl;
        }
        else
        {
            out << "  <td>" << i.value().hrAvg << "</td>" << endl;
            out << "  <td>" << i.value().hrStdDev << "</td>" << endl;
            out << "  <td>" << i.value().hrMin << "</td>" << endl;
            out << "  <td>" << i.value().hrMax << "</td>" << endl;
            out << "  <td>" << i.value().hrNumOfSamp << "</td>" << endl;
        }

        // BT
        if(i.value().btNumOfSamp < 0)
        {
            out << "  <td>" << QObject::tr("N/A") << "</td>" << endl;
            out << "  <td>" << QObject::tr("N/A") << "</td>" << endl;
            out << "  <td>" << QObject::tr("N/A") << "</td>" << endl;
            out << "  <td>" << QObject::tr("N/A") << "</td>" << endl;
            out << "  <td>" << QObject::tr("N/A") << "</td>" << endl;
        }
        else
        {
            out << "  <td>" << i.value().btAvg << "</td>" << endl;
            out << "  <td>" << i.value().btStdDev << "</td>" << endl;
            out << "  <td>" << i.value().btMin << "</td>" << endl;
            out << "  <td>" << i.value().btMax << "</td>" << endl;
            out << "  <td>" << i.value().btNumOfSamp << "</td>" << endl;
        }

        out << "</tr>" << endl;

        ++i;
    }

    out << "</table>";

    page.flush();
    page.close();

    // Blocking process.
    QProcess pandoc;
    pandoc.setWorkingDirectory(tempDir.path());
    QStringList arguments;
    arguments << page.fileName() << "--pdf-engine=wkhtmltopdf" << "-opage_2.pdf";
    pandoc.start("pandoc", arguments);

    if(!pandoc.waitForStarted(5000))
    {
        qDebug() << "Not started";
        return false;
    }

    if(!pandoc.waitForFinished(10000))
    {
        qDebug() << "Not finished";
        return false;
    }

    return true;
}

bool ReportGenerator::getTotalAverage(const QString &column, const QString &table, const time_t fromTime, const time_t toTime, double & average)
{
    bool ok;
    QString queryString = QString("SELECT round(avg(%1), 2) FROM %2 WHERE time >= %3 and time < %4").arg(column).arg(table).arg(fromTime).arg(toTime);

    QSqlQuery query;
    if(!query.exec(queryString))
    {
        qWarning() << QString("Failed to obtain total average for column %1 table %2, error was: %3")
                      .arg(column)
                      .arg(table)
                      .arg(query.lastError().text());
        return false;
    }

    if(!query.next())
        return false;

    average = query.value(0).toDouble(&ok);

    return ok;
}
