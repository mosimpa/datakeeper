# MoSimPa data base application

The purposes of this application are listed in [here](https://mosimpa.gitlab.io/app_bd/).

It makes use of [GIT flow](https://nvie.com/posts/a-successful-git-branching-model/), so remember:

- master is the production branch.
- develop is the actual development branch from which you should start working
  from.

Happy hacking!

# Script to generate fake data

scripts/generate_fake_data.py can be used to generate fake data and load it into
the database.

You can create an SQL file by doing:

    ./scripts/generate_fake_data.py > fakedata.sql
    psql -d mosimpa-datakeeper < fakedata.sql

Or in a one liner:

    ./scripts/generate_fake_data.py | psql -d mosimpa-datakeeper

# Running the tests

Be sure to have all the necessary dependencies, check debian/control's Build-Depends field for a list of them.

The tests uses Debian's pg_virtualenv.

Build the project as usual for a CMake-based project and then run

    ctest

Or if you want to be verbose:

    ctest -VV
