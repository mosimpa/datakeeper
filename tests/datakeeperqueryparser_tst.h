#ifndef DATAKEEPERQUERYPARSERTESTS_H
#define DATAKEEPERQUERYPARSERTESTS_H

#include <QObject>
#include <QtTest/QTest>

#include <mosquitto.h>
#include "mosquittomessage.h"
#include "../src/datakeeperqueryparser.h"

Q_DECLARE_METATYPE(DatakeeperQueryParser::Commands);
Q_DECLARE_METATYPE(DatakeeperQueryParser::values);

class DatakeeperQueryParserTests : public QObject
{
    Q_OBJECT

private slots:
    void checkTopic();
    void checkParseMessage_data();
    void checkParseMessage();

    void checkSensorsDataRequest_data();
    void checkSensorsDataRequest();

    void checkSetAlarmsRanges_data();
    void checkSetAlarmsRanges();

private:
    void clearValues(DatakeeperQueryParser::values & values);
    MosquittoMessage * newMosquittoMessage(const QString & payload);
    const QString LAST_ALARM_UPDATE = QString("2020-07-29 15:39:31-03");
};

#endif // DATAKEEPERQUERYPARSERTESTS_H
