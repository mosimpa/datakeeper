#include "../src/settings.h"

#include <mosimpaqt/scaling.h>
#include "settingstests.h"

void SettingsTests::checkAlarmsRanges()
{
    QVERIFY(Settings::spO2EqOrLessPercBot() == 85);
    QVERIFY(Settings::spO2EqOrLessPercTop() == 95);
    QVERIFY2(Settings::spO2EqOrLessPercBot() < Settings::spO2EqOrLessPercTop(),
             "SpO2 bot < top");

    QVERIFY(Settings::spO2DelaySMin() == 15);
    QVERIFY(Settings::spO2DelaySMax() == 60);
    QVERIFY(Settings::spO2DelaySMin() < Settings::spO2DelaySMax());

    QVERIFY(Settings::heartRateEqOrLessBPMBot() == 40);
    QVERIFY(Settings::heartRateEqOrLessBPMTop() == 60);
    QVERIFY2(Settings::heartRateEqOrLessBPMBot() < Settings::heartRateEqOrLessBPMTop(),
             "HR equal or less bot < top");

    QVERIFY(Settings::heartRateEqOrMoreBPMBot() == 100);
    QVERIFY(Settings::heartRateEqOrMoreBPMTop() == 140);
    QVERIFY2(Settings::heartRateEqOrMoreBPMBot() < Settings::heartRateEqOrMoreBPMTop(),
             "HR equal or more bot < top");

    QVERIFY(Settings::heartRateDelaySMin() == 15);
    QVERIFY(Settings::heartRateDelaySMax() == 60);
    QVERIFY(Settings::heartRateDelaySMin() < Settings::heartRateDelaySMax());

    QVERIFY(Settings::bodyTempEqOrLessCelsBot() == 35);
    QVERIFY(Settings::bodyTempEqOrLessCelsTop() == 37);
    QVERIFY2(Settings::bodyTempEqOrLessCelsBot() < Settings::bodyTempEqOrLessCelsTop(),
             "BT equal or more bot < top");

    QVERIFY(Settings::bodyTempEqOrMoreCelsBot() == 38);
    QVERIFY(Settings::bodyTempEqOrMoreCelsTop() == 40);
    QVERIFY2(Settings::bodyTempEqOrMoreCelsBot() < Settings::bodyTempEqOrMoreCelsTop(),
             "BT equal or more bot < top");

    QVERIFY(Settings::bodyTempDelaySMin() == 15);
    QVERIFY(Settings::bodyTempDelaySMax() == 60);
    QVERIFY(Settings::bodyTempDelaySMin() < Settings::bodyTempDelaySMax());

    QVERIFY(Settings::bloodPressureSysEqOrLessBot() == 100);
    QVERIFY(Settings::bloodPressureSysEqOrLessTop() == 120);

    QVERIFY(Settings::bloodPressureSysEqOrMoreBot() == 150);
    QVERIFY(Settings::bloodPressureSysEqOrMoreTop() == 170);

    QVERIFY(Settings::bloodPressureDelaySMin() == 15);
    QVERIFY(Settings::bloodPressureDelaySMax() == 60);
    QVERIFY(Settings::bloodPressureDelaySMin() < Settings::bloodPressureDelaySMax());

    QVERIFY(Settings::wireSpO2EqOrLessPercBot() == Scaling::spO2PercentageToWire(85));
    QVERIFY(Settings::wireSpO2EqOrLessPercTop() == Scaling::spO2PercentageToWire(95));

    QVERIFY(Settings::heartRateEqOrLessWireBot() == Scaling::heartRateBPMToWire(40));
    QVERIFY(Settings::heartRateEqOrLessWireTop() == Scaling::heartRateBPMToWire(60));

    QVERIFY(Settings::heartRateEqOrMoreWireBot() == Scaling::heartRateBPMToWire(100));
    QVERIFY(Settings::heartRateEqOrMoreWireTop() == Scaling::heartRateBPMToWire(140));

    QVERIFY(Settings::bodyTempEqOrLessWireBot() == Scaling::bodyTemperatureDegCelsiusToWire(35));
    QVERIFY(Settings::bodyTempEqOrLessWireTop() == Scaling::bodyTemperatureDegCelsiusToWire(37));

    QVERIFY(Settings::bodyTempEqOrMoreWireBot() == Scaling::bodyTemperatureDegCelsiusToWire(38));
    QVERIFY(Settings::bodyTempEqOrMoreWireTop() == Scaling::bodyTemperatureDegCelsiusToWire(40));
}

void SettingsTests::checkAlarmsOverlaps()
{
    // Heart rate has two alarms options.
    QVERIFY2(Settings::heartRateEqOrLessBPMTop() < Settings::heartRateEqOrMoreBPMBot(),
             "Heart rate ranges should not overlap");

    // Body temperature has two alarms options.
    QVERIFY2(Settings::bodyTempEqOrLessCelsTop() < Settings::bodyTempEqOrMoreCelsBot(),
             "Body temperature ranges should not overlap");

    // Blood pressure has two alarms ranges.
    QVERIFY2(Settings::bloodPressureSysEqOrLessTop() < Settings::bloodPressureSysEqOrMoreBot(),
             "Blood pressure ranges should not overlap");
}

QTEST_MAIN(SettingsTests)
#include "settingstests.moc"
