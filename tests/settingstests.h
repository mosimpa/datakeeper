#ifndef SETTINGSTESTS_H
#define SETTINGSTESTS_H

#include <QObject>
#include <QtTest/QTest>

class SettingsTests : public QObject
{
    Q_OBJECT

private slots:
    void checkAlarmsRanges();
    void checkAlarmsOverlaps();
};

#endif // SETTINGSTESTS_H
