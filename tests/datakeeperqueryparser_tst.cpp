#include <QMetaType>
#include <QString>
#include <QTest>
#include <QVector>

#include <mosquitto.h>

#include <mosimpaqt/definitions.h>
#include <mosimpaqt/scaling.h>

#include "../src/datakeeperqueryparser.h"
#include "../src/settings.h"

#include "mosquittomessage.h"
#include "datakeeperqueryparser_tst.h"

void DatakeeperQueryParserTests::checkTopic()
{
    QVERIFY2(DatakeeperQueryParser::topic() == QStringLiteral("datakeeper/query"), "Topic must match.");
}

void DatakeeperQueryParserTests::checkParseMessage_data()
{
    const QString MAC("001122334455");
    const QString ID("an_ID");
    const int32_t internmentId = 5;
    QString payloadFirstPart;
    QString end = QStringLiteral("}");
    DatakeeperQueryParser::values values;
    clearValues(values);

    QTest::addColumn<MosquittoMessage *>("msg");
    QTest::addColumn<DatakeeperQueryParser::values>("expectedValues");
    QTest::addColumn<DatakeeperQueryParser::Commands>("expectedCommand");

    QTest::addRow("invalid_message") << newMosquittoMessage("foo")
                                     << values
                                     << DatakeeperQueryParser::CommandUnknown;

    QTest::addRow("valid_json_no_mac") << newMosquittoMessage("{\"foo\":\"bar\"}")
                                       << values
                                       << DatakeeperQueryParser::CommandUnknown;

    payloadFirstPart = QString("{\"mac\":\"%1\"").arg(MAC);
    values.mac = MAC;

    QTest::addRow("valid_json_no_command") << newMosquittoMessage(payloadFirstPart + end)
                                           << values
                                           << DatakeeperQueryParser::CommandUnknown;

    QTest::addRow("valid_json_no_id") << newMosquittoMessage(payloadFirstPart + ",\"command\":\"invalid_command\"" + end)
                                      << values
                                      << DatakeeperQueryParser::CommandUnknown;

    QTest::addRow("valid_json_invalid_command") << newMosquittoMessage(payloadFirstPart + ",\"id\":\"var\",\"command\":\"foo\"" + end)
                                                << values
                                                << DatakeeperQueryParser::CommandUnknown;

    // Commands without parameters.
    values.id = ID;
    values.command = "internments";
    QTest::addRow("command_internments") << newMosquittoMessage(payloadFirstPart + QString(",\"id\":\"%1\",\"command\":\"internments\"").arg(ID) + end)
                                         << values
                                         << DatakeeperQueryParser::CommandInternments;

    values.command = "alarms_ranges";
    QTest::addRow("command_alarms_ranges") << newMosquittoMessage(payloadFirstPart + QString(",\"id\":\"%1\",\"command\":\"alarms_ranges\"").arg(ID) + end)
                                           << values
                                           << DatakeeperQueryParser::CommandAlarmRanges;

    // Data requests.
    payloadFirstPart.append(",\"id\":\"@ID@\",\"command\":\"@CMD@\"");

    QStringList dataCmds;
    dataCmds << QStringLiteral("spo2") << QStringLiteral("heartR") << QStringLiteral("bloodP") << QStringLiteral("bodyT");
    for(int i = 0; i < dataCmds.size(); i++)
    {
        clearValues(values);
        values.mac = MAC;
        values.command = dataCmds.at(i);
        values.id = ID;

        QString cmdTitle = QString("%1_no_objects").arg(dataCmds.at(i));
        QString payload = payloadFirstPart;
        payload.replace("@ID@", ID);
        payload.replace("@CMD@", dataCmds.at(i));

        DatakeeperQueryParser::Commands expectedCommand;

        switch(i) {
        case 0:
            expectedCommand = DatakeeperQueryParser::CommandSpO2;
            break;

        case 1:
            expectedCommand = DatakeeperQueryParser::CommandHeartRate;
            break;

        case 2:
            expectedCommand = DatakeeperQueryParser::CommandBloodPressure;
            break;

        case 3:
            expectedCommand = DatakeeperQueryParser::CommandBodyTemperature;
            break;
        }

        QTest::addRow("%s", cmdTitle.toUtf8().constData()) << newMosquittoMessage(payload + "}")
                                                           << values
                                                           << DatakeeperQueryParser::CommandUnknown;

        cmdTitle = QString("%1_just_internment_id").arg(dataCmds.at(i));
        QTest::addRow("%s", cmdTitle.toUtf8().constData()) << newMosquittoMessage(payload + ",\"internment_id\":1}")
                                                           << values
                                                           << DatakeeperQueryParser::CommandUnknown;

        cmdTitle = QString("%1_internment_id_num_of_sam_from_time").arg(dataCmds.at(i));
        values.internment_id = 1;
        values.numOfSamples = 2;
        values.fromTime = 3;
        QTest::addRow("%s", cmdTitle.toUtf8().constData()) << newMosquittoMessage(payload + ",\"internment_id\":1,\"num_of_sam\":2,\"from_time\":3}")
                                                           << values
                                                           << expectedCommand;
    }

    payloadFirstPart = QString("{\"mac\":\"%1\"").arg(MAC);

    // Get an alarm.
    values.command = "alarms_thresholds";
    QTest::addRow("alarms_thresholds") << newMosquittoMessage(payloadFirstPart +
                                                              QString(",\"id\":\"%1\","
                                                              "\"command\":\"alarms_thresholds\","
                                                              "\"internment_id\":%2}")
                                                              .arg(ID).arg(values.internment_id))
                            << values << DatakeeperQueryParser::Commands::CommandAlarmsThresholds;

    // Report generation.
    values.command = "gen_report";
    values.fromTime = 1595017346;
    values.toTime = values.fromTime+3600;
    QTest::addRow("command_gen_report_ok") << newMosquittoMessage(payloadFirstPart +
                                                                 QString(",\"id\":\"%1\","
                                                                         "\"command\":\"gen_report\","
                                                                         "\"internment_id\":%2,"
                                                                         "\"from_time\":%3,"
                                                                         "\"to_time\":%4}")
                                                                         .arg(ID).arg(values.internment_id)
                                                                         .arg(values.fromTime).arg(values.toTime))
                                          << values
                                          << DatakeeperQueryParser::CommandGenerateReport;

    values.toTime = values.fromTime+3600-1;
    QTest::addRow("command_gen_report_short_span") << newMosquittoMessage(payloadFirstPart +
                                                                          QString(",\"id\":\"%1\","
                                                                          "\"command\":\"gen_report\","
                                                                          "\"internment_id\":%2,"
                                                                          "\"from_time\":%3,"
                                                                          "\"to_time\":%4}")
                                                                          .arg(ID).arg(values.internment_id)
                                                                          .arg(values.fromTime).arg(values.toTime))
                                          << values
                                          << DatakeeperQueryParser::CommandUnknown;

    // Alarms setting.
    values.command = "set_alarms";
    QTest::addRow("command_set_alarms_empty") << newMosquittoMessage(payloadFirstPart + QString(",\"id\":\"%1\",\"command\":\"set_alarms\"").arg(ID) + end)
                                              << values
                                              << DatakeeperQueryParser::CommandUnknown;

    QTest::addRow("command_set_alarms_internment_id") << newMosquittoMessage(payloadFirstPart +
                                                         QString(",\"id\":\"%1\",\"command\":\"set_alarms\","
                                                                 "\"internment_id\":%2").arg(ID).arg(internmentId) + end)
                                                      << values
                                                      << DatakeeperQueryParser::CommandUnknown;

    QTest::addRow("command_set_alarms_internment_id_empty_alarms") << newMosquittoMessage(payloadFirstPart +
                                                                      QString(",\"id\":\"%1\",\"command\":\"set_alarms\","
                                                                      "\"internment_id\":%2,\"alarms\":{}").arg(ID).arg(internmentId) + end)
                                                                   << values
                                                                   << DatakeeperQueryParser::CommandUnknown;

    // Test that the parsing fails untill all the required values are there.
    QStringList alarmsObjects;
    QVector<int32_t> alarmsValues;
    alarmsObjects << "last_update" << "spo2_lt" << "spo2_delay_s" << "hr_lt" << "hr_gt" << "hr_delay_s" << "bt_lt" << "bt_gt" << "bt_delay_s" << "bp_sys_lt" << "bp_sys_gt" << "bp_delay_s";
    alarmsValues  << 1              << 950      << 15             << 600     <<  1000   << 45           << 370     << 380     << 30           <<  120         << 150        << 45;

    QVERIFY2(alarmsObjects.size() == alarmsValues.size(), "The vectors sizez must match.");

    QString alarmsPayload = QString("{\"mac\":\"%1\",\"id\":\"@ID@\",\"command\":\"@CMD@\"").arg(MAC);
    alarmsPayload.replace("@ID@", ID);
    alarmsPayload.replace("@CMD@", "set_alarms");
    alarmsPayload.append(QString(",\"internment_id\":%1,\"alarms\":{").arg(internmentId));
    values.internment_id = internmentId;
    values.fromTime = -1;
    DatakeeperQueryParser::Commands command;
    for(int i = 0; i < alarmsObjects.size(); i++)
    {
        QString payload = alarmsPayload;
        switch(i) {
        case 11:
            values.bPAlarmDelayS = static_cast<uint8_t>(alarmsValues.at(11));
            payload.append(QString("\"bp_delay_s\":%1,").arg(alarmsValues.at(11)));
            [[fallthrough]];

        case 10:
            values.bPSysGT = static_cast<blood_pressure_t>(alarmsValues.at(10));
            payload.append(QString("\"bp_sys_gt\":%1,").arg(alarmsValues.at(10)));
            [[fallthrough]];

        case 9:
            values.bPSysLT = static_cast<blood_pressure_t>(alarmsValues.at(9));
            payload.append(QString("\"bp_sys_lt\":%1,").arg(alarmsValues.at(9)));
            [[fallthrough]];

        case 8:
            values.bTAlarmDelayS = static_cast<uint8_t>(alarmsValues.at(8));
            payload.append(QString("\"bt_delay_s\":%1,").arg(alarmsValues.at(8)));
            [[fallthrough]];

        case 7:
            values.bTGT = static_cast<body_temp_t>(alarmsValues.at(7));
            payload.append(QString("\"bt_gt\":%1,").arg(alarmsValues.at(7)));
            [[fallthrough]];

        case 6:
            values.bTLT = static_cast<body_temp_t>(alarmsValues.at(6));
            payload.append(QString("\"bt_lt\":%1,").arg(alarmsValues.at(6)));
            [[fallthrough]];

        case 5:
            values.hRAlarmDelayS = static_cast<heart_rate_t>(alarmsValues.at(5));
            payload.append(QString("\"hr_delay_s\":%1,").arg(alarmsValues.at(5)));
            [[fallthrough]];

        case 4:
            values.hRGT = static_cast<heart_rate_t>(alarmsValues.at(4));
            payload.append(QString("\"hr_gt\":%1,").arg(alarmsValues.at(4)));
            [[fallthrough]];

        case 3:
            values.hRLT = static_cast<heart_rate_t>(alarmsValues.at(3));
            payload.append(QString("\"hr_lt\":%1,").arg(alarmsValues.at(3)));
            [[fallthrough]];

        case 2:
            values.spo2AlarmDelayS = static_cast<uint8_t>(alarmsValues.at(2));
            payload.append(QString("\"spo2_delay_s\":%1,").arg(alarmsValues.at(2)));
            [[fallthrough]];

        case 1:
            values.spo2LT = static_cast<spo2_t>(alarmsValues.at(1));
            payload.append(QString("\"spo2_lt\":%1,").arg(alarmsValues.at(1)));
            [[fallthrough]];

        case 0:
            // Ignore the alarmsValue assigned before and use the provided string.
            values.lastAlarmUpdate = LAST_ALARM_UPDATE;
            payload.append(QString("\"last_update\":\"%1\"").arg(LAST_ALARM_UPDATE));
        }

        if(i == 11)
            command = DatakeeperQueryParser::CommandSetAlarmsThresholds;
        else
            command = DatakeeperQueryParser::CommandUnknown;

        QTest::addRow("command_set_alarms_with_%s", alarmsObjects.at(i).toUtf8().constData())
                << newMosquittoMessage(payload + "}}")
                << values
                << command;
    }
}

void DatakeeperQueryParserTests::checkParseMessage()
{
    DatakeeperQueryParser::values values;
    clearValues(values);

    QFETCH(MosquittoMessage *, msg);
    QFETCH(DatakeeperQueryParser::values, expectedValues);
    QFETCH(DatakeeperQueryParser::Commands, expectedCommand);

    QVERIFY2(msg != nullptr, "Message should not be a null pointer");

    auto retval = DatakeeperQueryParser::parseMessage(&(msg->msg), values);

    QVERIFY2(retval == expectedCommand, "retval should match expectedResult");

    if(retval == DatakeeperQueryParser::CommandUnknown)
        return;

    // Common data.
    QVERIFY2(values.mac == expectedValues.mac, "The MAC must match");
    QVERIFY2(values.command == expectedValues.command, "The command must match");
    QVERIFY2(values.id == expectedValues.id, "The ID must match");

    switch(retval) {
    case DatakeeperQueryParser::CommandUnknown:
        break;

    case DatakeeperQueryParser::CommandInternments:
    case DatakeeperQueryParser::CommandAlarmRanges:
        // No extra parameters.
        break;

    case DatakeeperQueryParser::CommandAlarmsThresholds:
        QVERIFY2(values.internment_id == expectedValues.internment_id, "Internment ID must match");
        break;

    case DatakeeperQueryParser::CommandSpO2:
    case DatakeeperQueryParser::CommandHeartRate:
    case DatakeeperQueryParser::CommandBloodPressure:
    case DatakeeperQueryParser::CommandBodyTemperature:
        QVERIFY2(values.internment_id == expectedValues.internment_id, "Internment ID must match");
        QVERIFY2(values.fromTime == expectedValues.fromTime, "From time must match");
        QVERIFY2(values.numOfSamples == expectedValues.numOfSamples, "Number of samples must match");
        break;

    case DatakeeperQueryParser::CommandSetAlarmsThresholds:
        QVERIFY2(values.internment_id == expectedValues.internment_id, "Internment ID must match");
        QVERIFY2(values.lastAlarmUpdate == expectedValues.lastAlarmUpdate, "Last alarm ID must match");
        QVERIFY2(values.spo2LT == expectedValues.spo2LT, "SpO2 LT must match");
        QVERIFY2(values.spo2AlarmDelayS == expectedValues.spo2AlarmDelayS, "SpO2 alarm delay must match");
        QVERIFY2(values.hRLT == expectedValues.hRLT, "Heart Rate LT must match");
        QVERIFY2(values.hRGT == expectedValues.hRGT, "Heart Rate GT must match");
        QVERIFY2(values.hRAlarmDelayS == expectedValues.hRAlarmDelayS, "Heart Rate alarm delay must match");
        QVERIFY2(values.bTLT == expectedValues.bTLT, "Body temperature LT must match");
        QVERIFY2(values.bTGT == expectedValues.bTGT, "Body temperature GT must match");
        QVERIFY2(values.bTAlarmDelayS == expectedValues.bTAlarmDelayS, "Body temperature alarm delay must match");
        QVERIFY2(values.bPSysLT == expectedValues.bPSysLT, "Blood presssure systolic LT must match");
        QVERIFY2(values.bPSysGT == expectedValues.bPSysGT, "Blood presssure systolic GT must match");
        QVERIFY2(values.bPAlarmDelayS == expectedValues.bPAlarmDelayS, "Blood presssure alarm delay must match");
        break;

    case DatakeeperQueryParser::CommandGenerateReport:
        QVERIFY2(values.internment_id == expectedValues.internment_id, "Internment ID must match");
        QVERIFY2(values.fromTime == expectedValues.fromTime, "From time must match");
        QVERIFY2(values.toTime == expectedValues.toTime, "To time must match");
        break;
    }
}

/**
 * @brief DatakeeperQueryParserTests::checkSensorsDataRequest_data
 *
 * These tests will check that out of bound values are correctly parsed.
 * spo2, heartT, bloodP and bodyT commands all have the same code path,
 * so it should be safe to avoid testing all the possible combinations.
 */
void DatakeeperQueryParserTests::checkSensorsDataRequest_data()
{
    DatakeeperQueryParser::values values;
    clearValues(values);

    QTest::addColumn<MosquittoMessage *>("msg");
    QTest::addColumn<DatakeeperQueryParser::Commands>("expectedCommand");

    // Start without from_time.
    QString payload;
    payload = QString("{\"mac\":\"aabbccddeeff\",\"command\":\"%1\","
                      "\"id\":\"%2\",\"internment_id\":%3,\"num_of_sam\":%4,\"from_time\":0}")
                      .arg("spo2")
                      .arg("a normal ID")
                      .arg(4)
                      .arg(4);

    QTest::addRow("spo2_all_normal") << newMosquittoMessage(payload)
                                     << DatakeeperQueryParser::CommandSpO2;

    // Non-conformant MAC.
    payload = QString("{\"mac\":\"aagbccddeeff\",\"command\":\"%1\","
                      "\"id\":\"%2\",\"internment_id\":%3,\"num_of_sam\":%4,\"from_time\":0}")
                      .arg("heartR")
                      .arg("a normal ID")
                      .arg(4)
                      .arg(4);
    QTest::addRow("heartR_wrong_mac") << newMosquittoMessage(payload)
                                      << DatakeeperQueryParser::CommandUnknown;

    payload = QString("{\"mac\":\"aa:bb:cc:dd:ee:ff\",\"command\":\"%1\","
                      "\"id\":\"%2\",\"internment_id\":%3,\"num_of_sam\":%4,\"from_time\":0}")
                      .arg("bloodP")
                      .arg("a normal ID")
                      .arg(4)
                      .arg(4);
    QTest::addRow("bloodP_wrong_mac") << newMosquittoMessage(payload)
                                      << DatakeeperQueryParser::CommandUnknown;

    // ID with 32 bytes.
    payload = QString("{\"mac\":\"aabbccddeeff\",\"command\":\"%1\","
                      "\"id\":\"%2\",\"internment_id\":%3,\"num_of_sam\":%4,\"from_time\":0}")
                      .arg("bodyT")
                      .arg("12345678901234567890123456789012")
                      .arg(4)
                      .arg(4);
    QTest::addRow("bodyT_full_id") << newMosquittoMessage(payload)
                                   << DatakeeperQueryParser::CommandBodyTemperature;

    // ID longer than 32 bytes.
    payload = QString("{\"mac\":\"aabbccddeeff\",\"command\":\"%1\","
                      "\"id\":\"%2\",\"internment_id\":%3,\"num_of_sam\":%4,\"from_time\":0}")
                      .arg("bodyT")
                      .arg("123456789012345678901234567890123")
                      .arg(4)
                      .arg(4);
    QTest::addRow("bodyT_long_id") << newMosquittoMessage(payload)
                                   << DatakeeperQueryParser::CommandUnknown;

    // Internment ID
    payload = QString("{\"mac\":\"aabbccddeeff\",\"command\":\"%1\","
                      "\"id\":\"%2\",\"internment_id\":%3,\"num_of_sam\":%4,\"from_time\":0}")
                      .arg("bodyT")
                      .arg("an_id")
                      .arg(static_cast<int64_t>(std::numeric_limits<int32_t>::min()) -1)
                      .arg(4);
    QTest::addRow("bodyT_internment_id_min-1") << newMosquittoMessage(payload)
                                               << DatakeeperQueryParser::CommandUnknown;

    payload = QString("{\"mac\":\"aabbccddeeff\",\"command\":\"%1\","
                      "\"id\":\"%2\",\"internment_id\":%3,\"num_of_sam\":%4,\"from_time\":0}")
                      .arg("bodyT")
                      .arg("an_id")
                      .arg(DB_ID_MIN)
                      .arg(4);
    QTest::addRow("bodyT_internment_id_min") << newMosquittoMessage(payload)
                                             << DatakeeperQueryParser::CommandBodyTemperature;

    payload = QString("{\"mac\":\"aabbccddeeff\",\"command\":\"%1\","
                      "\"id\":\"%2\",\"internment_id\":%3,\"num_of_sam\":%4,\"from_time\":0}")
                      .arg("bodyT")
                      .arg("an_id")
                      .arg(DB_ID_MAX)
                      .arg(4);
    QTest::addRow("bodyT_internment_id_max") << newMosquittoMessage(payload)
                                             << DatakeeperQueryParser::CommandBodyTemperature;

    payload = QString("{\"mac\":\"aabbccddeeff\",\"command\":\"%1\","
                      "\"id\":\"%2\",\"internment_id\":%3,\"num_of_sam\":%4,\"from_time\":0}")
                      .arg("bodyT")
                      .arg("an_id")
                      .arg(static_cast<int64_t>(DB_ID_MAX) + 1)
                      .arg(4);
    QTest::addRow("bodyT_internment_id_max+1") << newMosquittoMessage(payload)
                                               << DatakeeperQueryParser::CommandUnknown;

    // Number of samples.
    payload = QString("{\"mac\":\"aabbccddeeff\",\"command\":\"%1\","
                      "\"id\":\"%2\",\"internment_id\":%3,\"num_of_sam\":%4,\"from_time\":0}")
                      .arg("spo2")
                      .arg("an_id")
                      .arg(4)
                      .arg(static_cast<int64_t>(std::numeric_limits<int32_t>::max()) + 1);
    QTest::addRow("spo2_num_samples_max_+_1") << newMosquittoMessage(payload)
                                              << DatakeeperQueryParser::CommandUnknown;

    payload = QString("{\"mac\":\"aabbccddeeff\",\"command\":\"%1\","
                      "\"id\":\"%2\",\"internment_id\":%3,\"num_of_sam\":%4,\"from_time\":0}")
                      .arg("spo2")
                      .arg("an_id")
                      .arg(4)
                      .arg(std::numeric_limits<int32_t>::max());
    QTest::addRow("spo2_num_samples_max") << newMosquittoMessage(payload)
                                          << DatakeeperQueryParser::CommandSpO2;

    payload = QString("{\"mac\":\"aabbccddeeff\",\"command\":\"%1\","
                      "\"id\":\"%2\",\"internment_id\":%3,\"num_of_sam\":%4,\"from_time\":0}")
                      .arg("spo2")
                      .arg("an_id")
                      .arg(4)
                      .arg(0);
    QTest::addRow("spo2_num_samples_min") << newMosquittoMessage(payload)
                                          << DatakeeperQueryParser::CommandSpO2;
    payload = QString("{\"mac\":\"aabbccddeeff\",\"command\":\"%1\","
                      "\"id\":\"%2\",\"internment_id\":%3,\"num_of_sam\":%4,\"from_time\":0}")
                      .arg("spo2")
                      .arg("an_id")
                      .arg(4)
                      .arg(-1);
    QTest::addRow("spo2_num_samples_min-1") << newMosquittoMessage(payload)
                                          << DatakeeperQueryParser::CommandUnknown;

    // From time.
    payload = QString("{\"mac\":\"aabbccddeeff\",\"command\":\"%1\","
                      "\"id\":\"%2\",\"internment_id\":%3,\"num_of_sam\":%4,\"from_time\":%5}")
                      .arg("bodyT")
                      .arg("an_id")
                      .arg(4)
                      .arg(4)
                      .arg(-1);
    QTest::addRow("bodyT_from_time_min-1") << newMosquittoMessage(payload)
                                           << DatakeeperQueryParser::CommandUnknown;

    payload = QString("{\"mac\":\"aabbccddeeff\",\"command\":\"%1\","
                      "\"id\":\"%2\",\"internment_id\":%3,\"num_of_sam\":%4,\"from_time\":%5}")
                      .arg("bodyT")
                      .arg("an_id")
                      .arg(4)
                      .arg(4)
                      .arg(1);
    QTest::addRow("bodyT_from_time_min") << newMosquittoMessage(payload)
                                         << DatakeeperQueryParser::CommandBodyTemperature;

    payload = QString("{\"mac\":\"aabbccddeeff\",\"command\":\"%1\","
                      "\"id\":\"%2\",\"internment_id\":%3,\"num_of_sam\":%4,\"from_time\":%5}")
                      .arg("bodyT")
                      .arg("an_id")
                      .arg(4)
                      .arg(4)
                      .arg(std::numeric_limits<int32_t>::max());
    QTest::addRow("bodyT_from_time_max") << newMosquittoMessage(payload)
                                         << DatakeeperQueryParser::CommandBodyTemperature;

    payload = QString("{\"mac\":\"aabbccddeeff\",\"command\":\"%1\","
                      "\"id\":\"%2\",\"internment_id\":%3,\"num_of_sam\":%4,\"from_time\":%5}")
                      .arg("bodyT")
                      .arg("an_id")
                      .arg(4)
                      .arg(4)
                      .arg(static_cast<int64_t>(std::numeric_limits<int32_t>::max()) + 1);
    QTest::addRow("bodyT_from_time_max+1") << newMosquittoMessage(payload)
                                           << DatakeeperQueryParser::CommandUnknown;
}

void DatakeeperQueryParserTests::checkSensorsDataRequest()
{
    DatakeeperQueryParser::values values;
    clearValues(values);

    QFETCH(MosquittoMessage *, msg);
    QFETCH(DatakeeperQueryParser::Commands, expectedCommand);

    QVERIFY2(msg != nullptr, "Message should not be a null pointer");

    auto retval = DatakeeperQueryParser::parseMessage(&(msg->msg), values);

    QVERIFY2(retval == expectedCommand, "retval should match expectedResult");
}

void DatakeeperQueryParserTests::checkSetAlarmsRanges_data()
{
    const spo2_t SPO2_MED = (Settings::instance().wireSpO2EqOrLessPercBot() +
                             Settings::instance().wireSpO2EqOrLessPercTop()) / 2.0;

    const uint8_t SPO2_DELAY_MED = (Settings::spO2DelaySMin() + Settings::spO2DelaySMax()) / 2;

    const heart_rate_t HR_LT_MED = (Settings::instance().heartRateEqOrLessWireBot() +
                                    Settings::instance().heartRateEqOrLessWireTop()) / 2.0;

    const heart_rate_t HR_GT_MED = (Settings::instance().heartRateEqOrMoreWireBot() +
                                    Settings::instance().heartRateEqOrMoreWireTop()) / 2.0;

    const uint8_t HR_DELAY_MED = (Settings::heartRateDelaySMin() + Settings::heartRateDelaySMax()) / 2;

    const body_temp_t BT_LT_MED = (Settings::instance().bodyTempEqOrLessWireBot() +
                                   Settings::instance().bodyTempEqOrLessWireTop()) / 2.0;

    const body_temp_t BT_GT_MED = (Settings::instance().bodyTempEqOrMoreWireBot() +
                                   Settings::instance().bodyTempEqOrMoreWireTop()) / 2.0;

    const uint8_t BT_DELAY_MED = (Settings::bodyTempDelaySMin() + Settings::bodyTempDelaySMax()) / 2;

    const blood_pressure_t BP_SYS_LT_MED = (Settings::instance().bloodPressureSysEqOrLessBot() +
                                            Settings::instance().bloodPressureSysEqOrLessTop()) / 2.0;

    const blood_pressure_t BP_SYS_GT_MED = (Settings::instance().bloodPressureSysEqOrMoreBot() +
                                            Settings::instance().bloodPressureSysEqOrMoreTop()) / 2.0;

    const uint8_t BP_DELAY_MED = (Settings::bloodPressureDelaySMin() + Settings::bloodPressureDelaySMax()) / 2;

    QTest::addColumn<MosquittoMessage *>("msg");
    QTest::addColumn<DatakeeperQueryParser::Commands>("expectedCommand");

    QString payload;
    payload = QString("{\"mac\":\"aabbccddeeff\",\"command\":\"set_alarms\","
                      "\"id\":\"another_id\","
                      "\"internment_id\":5,"
                      "\"alarms\":{"
                      "\"last_update\":\"%1\","
                      "\"spo2_lt\":%2,"
                      "\"spo2_delay_s\":%3,"
                      "\"hr_lt\":%4,"
                      "\"hr_gt\":%5,"
                      "\"hr_delay_s\":%6,"
                      "\"bt_lt\":%7,"
                      "\"bt_gt\":%8,"
                      "\"bt_delay_s\":%9,"
                      "\"bp_sys_lt\":%10,"
                      "\"bp_sys_gt\":%11,"
                      "\"bp_delay_s\":%12}}")
                      .arg(LAST_ALARM_UPDATE)
                      .arg(Scaling::spO2PercentageToWire(Settings::instance().spO2EqOrLessPercBot()) - 1)
                      .arg(SPO2_DELAY_MED)
                      .arg(HR_LT_MED)
                      .arg(HR_GT_MED)
                      .arg(HR_DELAY_MED)
                      .arg(BT_LT_MED)
                      .arg(BT_GT_MED)
                      .arg(BT_DELAY_MED)
                      .arg(BP_SYS_LT_MED)
                      .arg(BP_SYS_GT_MED)
                      .arg(BP_DELAY_MED);

    QTest::addRow("all_normal") << newMosquittoMessage(payload)
                                << DatakeeperQueryParser::CommandUnknown;

    // SpO2
    payload = QString("{\"mac\":\"aabbccddeeff\",\"command\":\"set_alarms\","
                      "\"id\":\"another_id\","
                      "\"internment_id\":5,"
                      "\"alarms\":{"
                      "\"last_update\":\"%1\","
                      "\"spo2_lt\":%2,"
                      "\"spo2_delay_s\":%3,"
                      "\"hr_lt\":%4,"
                      "\"hr_gt\":%5,"
                      "\"hr_delay_s\":%6,"
                      "\"bt_lt\":%7,"
                      "\"bt_gt\":%8,"
                      "\"bt_delay_s\":%9,"
                      "\"bp_sys_lt\":%10,"
                      "\"bp_sys_gt\":%11,"
                      "\"bp_delay_s\":%12}}")
                      .arg(LAST_ALARM_UPDATE)
                      .arg(Settings::instance().wireSpO2EqOrLessPercBot() - 1)
                      .arg(SPO2_DELAY_MED)
                      .arg(HR_LT_MED)
                      .arg(HR_GT_MED)
                      .arg(HR_DELAY_MED)
                      .arg(BT_LT_MED)
                      .arg(BT_GT_MED)
                      .arg(BT_DELAY_MED)
                      .arg(BP_SYS_LT_MED)
                      .arg(BP_SYS_GT_MED)
                      .arg(BP_DELAY_MED);

    QTest::addRow("spo2_less_than_min") << newMosquittoMessage(payload)
                                        << DatakeeperQueryParser::CommandUnknown;

    payload = QString("{\"mac\":\"aabbccddeeff\",\"command\":\"set_alarms\","
                      "\"id\":\"another_id\","
                      "\"internment_id\":5,"
                      "\"alarms\":{"
                      "\"last_update\":\"%1\","
                      "\"spo2_lt\":%2,"
                      "\"spo2_delay_s\":%3,"
                      "\"hr_lt\":%4,"
                      "\"hr_gt\":%5,"
                      "\"hr_delay_s\":%6,"
                      "\"bt_lt\":%7,"
                      "\"bt_gt\":%8,"
                      "\"bt_delay_s\":%9,"
                      "\"bp_sys_lt\":%10,"
                      "\"bp_sys_gt\":%11,"
                      "\"bp_delay_s\":%12}}")
                      .arg(LAST_ALARM_UPDATE)
                      .arg(Settings::instance().wireSpO2EqOrLessPercBot())
                      .arg(SPO2_DELAY_MED)
                      .arg(HR_LT_MED)
                      .arg(HR_GT_MED)
                      .arg(HR_DELAY_MED)
                      .arg(BT_LT_MED)
                      .arg(BT_GT_MED)
                      .arg(BT_DELAY_MED)
                      .arg(BP_SYS_LT_MED)
                      .arg(BP_SYS_GT_MED)
                      .arg(BP_DELAY_MED);

    QTest::addRow("spo2_min") << newMosquittoMessage(payload)
                              << DatakeeperQueryParser::CommandSetAlarmsThresholds;

    payload = QString("{\"mac\":\"aabbccddeeff\",\"command\":\"set_alarms\","
                      "\"id\":\"another_id\","
                      "\"internment_id\":5,"
                      "\"alarms\":{"
                      "\"last_update\":\"%1\","
                      "\"spo2_lt\":%2,"
                      "\"spo2_delay_s\":%3,"
                      "\"hr_lt\":%4,"
                      "\"hr_gt\":%5,"
                      "\"hr_delay_s\":%6,"
                      "\"bt_lt\":%7,"
                      "\"bt_gt\":%8,"
                      "\"bt_delay_s\":%9,"
                      "\"bp_sys_lt\":%10,"
                      "\"bp_sys_gt\":%11,"
                      "\"bp_delay_s\":%12}}")
                      .arg(LAST_ALARM_UPDATE)
                      .arg(Settings::instance().wireSpO2EqOrLessPercTop())
                      .arg(SPO2_DELAY_MED)
                      .arg(HR_LT_MED)
                      .arg(HR_GT_MED)
                      .arg(HR_DELAY_MED)
                      .arg(BT_LT_MED)
                      .arg(BT_GT_MED)
                      .arg(BT_DELAY_MED)
                      .arg(BP_SYS_LT_MED)
                      .arg(BP_SYS_GT_MED)
                      .arg(BP_DELAY_MED);

    QTest::addRow("spo2_max") << newMosquittoMessage(payload)
                              << DatakeeperQueryParser::CommandSetAlarmsThresholds;

    payload = QString("{\"mac\":\"aabbccddeeff\",\"command\":\"set_alarms\","
                      "\"id\":\"another_id\","
                      "\"internment_id\":5,"
                      "\"alarms\":{"
                      "\"last_update\":\"%1\","
                      "\"spo2_lt\":%2,"
                      "\"spo2_delay_s\":%3,"
                      "\"hr_lt\":%4,"
                      "\"hr_gt\":%5,"
                      "\"hr_delay_s\":%6,"
                      "\"bt_lt\":%7,"
                      "\"bt_gt\":%8,"
                      "\"bt_delay_s\":%9,"
                      "\"bp_sys_lt\":%10,"
                      "\"bp_sys_gt\":%11,"
                      "\"bp_delay_s\":%12}}")
                      .arg(LAST_ALARM_UPDATE)
                      .arg(Settings::instance().wireSpO2EqOrLessPercTop() + 1)
                      .arg(SPO2_DELAY_MED)
                      .arg(HR_LT_MED)
                      .arg(HR_GT_MED)
                      .arg(HR_DELAY_MED)
                      .arg(BT_LT_MED)
                      .arg(BT_GT_MED)
                      .arg(BT_DELAY_MED)
                      .arg(BP_SYS_LT_MED)
                      .arg(BP_SYS_GT_MED)
                      .arg(BP_DELAY_MED);

    QTest::addRow("spo2_max+1") << newMosquittoMessage(payload)
                                << DatakeeperQueryParser::CommandUnknown;

    // SpO2 delay
    payload = QString("{\"mac\":\"aabbccddeeff\",\"command\":\"set_alarms\","
                      "\"id\":\"another_id\","
                      "\"internment_id\":5,"
                      "\"alarms\":{"
                      "\"last_update\":\"%1\","
                      "\"spo2_lt\":%2,"
                      "\"spo2_delay_s\":%3,"
                      "\"hr_lt\":%4,"
                      "\"hr_gt\":%5,"
                      "\"hr_delay_s\":%6,"
                      "\"bt_lt\":%7,"
                      "\"bt_gt\":%8,"
                      "\"bt_delay_s\":%9,"
                      "\"bp_sys_lt\":%10,"
                      "\"bp_sys_gt\":%11,"
                      "\"bp_delay_s\":%12}}")
                      .arg(LAST_ALARM_UPDATE)
                      .arg(SPO2_MED)
                      .arg(Settings::spO2DelaySMin() - 1)
                      .arg(HR_LT_MED)
                      .arg(HR_GT_MED)
                      .arg(HR_DELAY_MED)
                      .arg(BT_LT_MED)
                      .arg(BT_GT_MED)
                      .arg(BT_DELAY_MED)
                      .arg(BP_SYS_LT_MED)
                      .arg(BP_SYS_GT_MED)
                      .arg(BP_DELAY_MED);

    QTest::addRow("spo2_delay_less_than_min") << newMosquittoMessage(payload)
                                              << DatakeeperQueryParser::CommandUnknown;

    payload = QString("{\"mac\":\"aabbccddeeff\",\"command\":\"set_alarms\","
                      "\"id\":\"another_id\","
                      "\"internment_id\":5,"
                      "\"alarms\":{"
                      "\"last_update\":\"%1\","
                      "\"spo2_lt\":%2,"
                      "\"spo2_delay_s\":%3,"
                      "\"hr_lt\":%4,"
                      "\"hr_gt\":%5,"
                      "\"hr_delay_s\":%6,"
                      "\"bt_lt\":%7,"
                      "\"bt_gt\":%8,"
                      "\"bt_delay_s\":%9,"
                      "\"bp_sys_lt\":%10,"
                      "\"bp_sys_gt\":%11,"
                      "\"bp_delay_s\":%12}}")
                      .arg(LAST_ALARM_UPDATE)
                      .arg(SPO2_MED)
                      .arg(Settings::spO2DelaySMin())
                      .arg(HR_LT_MED)
                      .arg(HR_GT_MED)
                      .arg(HR_DELAY_MED)
                      .arg(BT_LT_MED)
                      .arg(BT_GT_MED)
                      .arg(BT_DELAY_MED)
                      .arg(BP_SYS_LT_MED)
                      .arg(BP_SYS_GT_MED)
                      .arg(BP_DELAY_MED);

    QTest::addRow("spo2_delay_min") << newMosquittoMessage(payload)
                                    << DatakeeperQueryParser::CommandSetAlarmsThresholds;

    payload = QString("{\"mac\":\"aabbccddeeff\",\"command\":\"set_alarms\","
                      "\"id\":\"another_id\","
                      "\"internment_id\":5,"
                      "\"alarms\":{"
                      "\"last_update\":\"%1\","
                      "\"spo2_lt\":%2,"
                      "\"spo2_delay_s\":%3,"
                      "\"hr_lt\":%4,"
                      "\"hr_gt\":%5,"
                      "\"hr_delay_s\":%6,"
                      "\"bt_lt\":%7,"
                      "\"bt_gt\":%8,"
                      "\"bt_delay_s\":%9,"
                      "\"bp_sys_lt\":%10,"
                      "\"bp_sys_gt\":%11,"
                      "\"bp_delay_s\":%12}}")
                      .arg(LAST_ALARM_UPDATE)
                      .arg(SPO2_MED)
                      .arg(Settings::spO2DelaySMax())
                      .arg(HR_LT_MED)
                      .arg(HR_GT_MED)
                      .arg(HR_DELAY_MED)
                      .arg(BT_LT_MED)
                      .arg(BT_GT_MED)
                      .arg(BT_DELAY_MED)
                      .arg(BP_SYS_LT_MED)
                      .arg(BP_SYS_GT_MED)
                      .arg(BP_DELAY_MED);

    QTest::addRow("spo2_delay_max") << newMosquittoMessage(payload)
                                    << DatakeeperQueryParser::CommandSetAlarmsThresholds;

    payload = QString("{\"mac\":\"aabbccddeeff\",\"command\":\"set_alarms\","
                      "\"id\":\"another_id\","
                      "\"internment_id\":5,"
                      "\"alarms\":{"
                      "\"last_update\":\"%1\","
                      "\"spo2_lt\":%2,"
                      "\"spo2_delay_s\":%3,"
                      "\"hr_lt\":%4,"
                      "\"hr_gt\":%5,"
                      "\"hr_delay_s\":%6,"
                      "\"bt_lt\":%7,"
                      "\"bt_gt\":%8,"
                      "\"bt_delay_s\":%9,"
                      "\"bp_sys_lt\":%10,"
                      "\"bp_sys_gt\":%11,"
                      "\"bp_delay_s\":%12}}")
                      .arg(LAST_ALARM_UPDATE)
                      .arg(SPO2_MED)
                      .arg(Settings::spO2DelaySMax() + 1)
                      .arg(HR_LT_MED)
                      .arg(HR_GT_MED)
                      .arg(HR_DELAY_MED)
                      .arg(BT_LT_MED)
                      .arg(BT_GT_MED)
                      .arg(BT_DELAY_MED)
                      .arg(BP_SYS_LT_MED)
                      .arg(BP_SYS_GT_MED)
                      .arg(BP_DELAY_MED);

    QTest::addRow("spo2_delay_max+1") << newMosquittoMessage(payload)
                                      << DatakeeperQueryParser::CommandUnknown;

    // Heart rate equal or less.
    payload = QString("{\"mac\":\"aabbccddeeff\",\"command\":\"set_alarms\","
                      "\"id\":\"another_id\","
                      "\"internment_id\":5,"
                      "\"alarms\":{"
                      "\"last_update\":\"%1\","
                      "\"spo2_lt\":%2,"
                      "\"spo2_delay_s\":%3,"
                      "\"hr_lt\":%4,"
                      "\"hr_gt\":%5,"
                      "\"hr_delay_s\":%6,"
                      "\"bt_lt\":%7,"
                      "\"bt_gt\":%8,"
                      "\"bt_delay_s\":%9,"
                      "\"bp_sys_lt\":%10,"
                      "\"bp_sys_gt\":%11,"
                      "\"bp_delay_s\":%12}}")
                      .arg(LAST_ALARM_UPDATE)
                      .arg(SPO2_MED)
                      .arg(SPO2_DELAY_MED)
                      .arg(Settings::instance().heartRateEqOrLessWireBot() - 1)
                      .arg(HR_GT_MED)
                      .arg(HR_DELAY_MED)
                      .arg(BT_LT_MED)
                      .arg(BT_GT_MED)
                      .arg(BT_DELAY_MED)
                      .arg(BP_SYS_LT_MED)
                      .arg(BP_SYS_GT_MED)
                      .arg(BP_DELAY_MED);

    QTest::addRow("hr_lt_less_than_min") << newMosquittoMessage(payload)
                                         << DatakeeperQueryParser::CommandUnknown;

    payload = QString("{\"mac\":\"aabbccddeeff\",\"command\":\"set_alarms\","
                      "\"id\":\"another_id\","
                      "\"internment_id\":5,"
                      "\"alarms\":{"
                      "\"last_update\":\"%1\","
                      "\"spo2_lt\":%2,"
                      "\"spo2_delay_s\":%3,"
                      "\"hr_lt\":%4,"
                      "\"hr_gt\":%5,"
                      "\"hr_delay_s\":%6,"
                      "\"bt_lt\":%7,"
                      "\"bt_gt\":%8,"
                      "\"bt_delay_s\":%9,"
                      "\"bp_sys_lt\":%10,"
                      "\"bp_sys_gt\":%11,"
                      "\"bp_delay_s\":%12}}")
                      .arg(LAST_ALARM_UPDATE)
                      .arg(SPO2_MED)
                      .arg(SPO2_DELAY_MED)
                      .arg(Settings::instance().heartRateEqOrLessWireBot())
                      .arg(HR_GT_MED)
                      .arg(HR_DELAY_MED)
                      .arg(BT_LT_MED)
                      .arg(BT_GT_MED)
                      .arg(BT_DELAY_MED)
                      .arg(BP_SYS_LT_MED)
                      .arg(BP_SYS_GT_MED)
                      .arg(BP_DELAY_MED);

    QTest::addRow("hr_lt_min") << newMosquittoMessage(payload)
                               << DatakeeperQueryParser::CommandSetAlarmsThresholds;

    payload = QString("{\"mac\":\"aabbccddeeff\",\"command\":\"set_alarms\","
                      "\"id\":\"another_id\","
                      "\"internment_id\":5,"
                      "\"alarms\":{"
                      "\"last_update\":\"%1\","
                      "\"spo2_lt\":%2,"
                      "\"spo2_delay_s\":%3,"
                      "\"hr_lt\":%4,"
                      "\"hr_gt\":%5,"
                      "\"hr_delay_s\":%6,"
                      "\"bt_lt\":%7,"
                      "\"bt_gt\":%8,"
                      "\"bt_delay_s\":%9,"
                      "\"bp_sys_lt\":%10,"
                      "\"bp_sys_gt\":%11,"
                      "\"bp_delay_s\":%12}}")
                      .arg(LAST_ALARM_UPDATE)
                      .arg(SPO2_MED)
                      .arg(SPO2_DELAY_MED)
                      .arg(Settings::instance().heartRateEqOrLessWireTop())
                      .arg(HR_GT_MED)
                      .arg(HR_DELAY_MED)
                      .arg(BT_LT_MED)
                      .arg(BT_GT_MED)
                      .arg(BT_DELAY_MED)
                      .arg(BP_SYS_LT_MED)
                      .arg(BP_SYS_GT_MED)
                      .arg(BP_DELAY_MED);

    QTest::addRow("hr_lt_max") << newMosquittoMessage(payload)
                               << DatakeeperQueryParser::CommandSetAlarmsThresholds;

    payload = QString("{\"mac\":\"aabbccddeeff\",\"command\":\"set_alarms\","
                      "\"id\":\"another_id\","
                      "\"internment_id\":5,"
                      "\"alarms\":{"
                      "\"last_update\":\"%1\","
                      "\"spo2_lt\":%2,"
                      "\"spo2_delay_s\":%3,"
                      "\"hr_lt\":%4,"
                      "\"hr_gt\":%5,"
                      "\"hr_delay_s\":%6,"
                      "\"bt_lt\":%7,"
                      "\"bt_gt\":%8,"
                      "\"bt_delay_s\":%9,"
                      "\"bp_sys_lt\":%10,"
                      "\"bp_sys_gt\":%11,"
                      "\"bp_delay_s\":%12}}")
                      .arg(LAST_ALARM_UPDATE)
                      .arg(SPO2_MED)
                      .arg(SPO2_DELAY_MED)
                      .arg(Settings::instance().heartRateEqOrLessWireTop() + 1)
                      .arg(HR_GT_MED)
                      .arg(HR_DELAY_MED)
                      .arg(BT_LT_MED)
                      .arg(BT_GT_MED)
                      .arg(BT_DELAY_MED)
                      .arg(BP_SYS_LT_MED)
                      .arg(BP_SYS_GT_MED)
                      .arg(BP_DELAY_MED);

    QTest::addRow("hr_lt_max+1") << newMosquittoMessage(payload)
                                 << DatakeeperQueryParser::CommandUnknown;

    // Heart rate equal or greater.
    payload = QString("{\"mac\":\"aabbccddeeff\",\"command\":\"set_alarms\","
                      "\"id\":\"another_id\","
                      "\"internment_id\":5,"
                      "\"alarms\":{"
                      "\"last_update\":\"%1\","
                      "\"spo2_lt\":%2,"
                      "\"spo2_delay_s\":%3,"
                      "\"hr_lt\":%4,"
                      "\"hr_gt\":%5,"
                      "\"hr_delay_s\":%6,"
                      "\"bt_lt\":%7,"
                      "\"bt_gt\":%8,"
                      "\"bt_delay_s\":%9,"
                      "\"bp_sys_lt\":%10,"
                      "\"bp_sys_gt\":%11,"
                      "\"bp_delay_s\":%12}}")
                      .arg(LAST_ALARM_UPDATE)
                      .arg(SPO2_MED)
                      .arg(SPO2_DELAY_MED)
                      .arg(HR_LT_MED)
                      .arg(Settings::instance().heartRateEqOrMoreWireBot() - 1)
                      .arg(HR_DELAY_MED)
                      .arg(BT_LT_MED)
                      .arg(BT_GT_MED)
                      .arg(BT_DELAY_MED)
                      .arg(BP_SYS_LT_MED)
                      .arg(BP_SYS_GT_MED)
                      .arg(BP_DELAY_MED);

    QTest::addRow("hr_gt_less_than_min") << newMosquittoMessage(payload)
                                         << DatakeeperQueryParser::CommandUnknown;

    payload = QString("{\"mac\":\"aabbccddeeff\",\"command\":\"set_alarms\","
                      "\"id\":\"another_id\","
                      "\"internment_id\":5,"
                      "\"alarms\":{"
                      "\"last_update\":\"%1\","
                      "\"spo2_lt\":%2,"
                      "\"spo2_delay_s\":%3,"
                      "\"hr_lt\":%4,"
                      "\"hr_gt\":%5,"
                      "\"hr_delay_s\":%6,"
                      "\"bt_lt\":%7,"
                      "\"bt_gt\":%8,"
                      "\"bt_delay_s\":%9,"
                      "\"bp_sys_lt\":%10,"
                      "\"bp_sys_gt\":%11,"
                      "\"bp_delay_s\":%12}}")
                      .arg(LAST_ALARM_UPDATE)
                      .arg(SPO2_MED)
                      .arg(SPO2_DELAY_MED)
                      .arg(HR_LT_MED)
                      .arg(Settings::instance().heartRateEqOrMoreWireBot())
                      .arg(HR_DELAY_MED)
                      .arg(BT_LT_MED)
                      .arg(BT_GT_MED)
                      .arg(BT_DELAY_MED)
                      .arg(BP_SYS_LT_MED)
                      .arg(BP_SYS_GT_MED)
                      .arg(BP_DELAY_MED);

    QTest::addRow("hr_gt_min") << newMosquittoMessage(payload)
                               << DatakeeperQueryParser::CommandSetAlarmsThresholds;

    payload = QString("{\"mac\":\"aabbccddeeff\",\"command\":\"set_alarms\","
                      "\"id\":\"another_id\","
                      "\"internment_id\":5,"
                      "\"alarms\":{"
                      "\"last_update\":\"%1\","
                      "\"spo2_lt\":%2,"
                      "\"spo2_delay_s\":%3,"
                      "\"hr_lt\":%4,"
                      "\"hr_gt\":%5,"
                      "\"hr_delay_s\":%6,"
                      "\"bt_lt\":%7,"
                      "\"bt_gt\":%8,"
                      "\"bt_delay_s\":%9,"
                      "\"bp_sys_lt\":%10,"
                      "\"bp_sys_gt\":%11,"
                      "\"bp_delay_s\":%12}}")
                      .arg(LAST_ALARM_UPDATE)
                      .arg(SPO2_MED)
                      .arg(SPO2_DELAY_MED)
                      .arg(HR_LT_MED)
                      .arg(Settings::instance().heartRateEqOrMoreWireTop())
                      .arg(HR_DELAY_MED)
                      .arg(BT_LT_MED)
                      .arg(BT_GT_MED)
                      .arg(BT_DELAY_MED)
                      .arg(BP_SYS_LT_MED)
                      .arg(BP_SYS_GT_MED)
                      .arg(BP_DELAY_MED);

    QTest::addRow("hr_gt_max") << newMosquittoMessage(payload)
                               << DatakeeperQueryParser::CommandSetAlarmsThresholds;

    payload = QString("{\"mac\":\"aabbccddeeff\",\"command\":\"set_alarms\","
                      "\"id\":\"another_id\","
                      "\"internment_id\":5,"
                      "\"alarms\":{"
                      "\"last_update\":\"%1\","
                      "\"spo2_lt\":%2,"
                      "\"spo2_delay_s\":%3,"
                      "\"hr_lt\":%4,"
                      "\"hr_gt\":%5,"
                      "\"hr_delay_s\":%6,"
                      "\"bt_lt\":%7,"
                      "\"bt_gt\":%8,"
                      "\"bt_delay_s\":%9,"
                      "\"bp_sys_lt\":%10,"
                      "\"bp_sys_gt\":%11,"
                      "\"bp_delay_s\":%12}}")
                      .arg(LAST_ALARM_UPDATE)
                      .arg(SPO2_MED)
                      .arg(SPO2_DELAY_MED)
                      .arg(HR_LT_MED)
                      .arg(Settings::instance().heartRateEqOrMoreWireTop() + 1)
                      .arg(HR_DELAY_MED)
                      .arg(BT_LT_MED)
                      .arg(BT_GT_MED)
                      .arg(BT_DELAY_MED)
                      .arg(BP_SYS_LT_MED)
                      .arg(BP_SYS_GT_MED)
                      .arg(BP_DELAY_MED);

    QTest::addRow("hr_gt_max+1") << newMosquittoMessage(payload)
                                 << DatakeeperQueryParser::CommandUnknown;

    /*
     * HR LT < HR GT is currently enforced by the ranges that do not overlap,
     * so we can't test this condition.
     */

    // Heart rate delay.
    payload = QString("{\"mac\":\"aabbccddeeff\",\"command\":\"set_alarms\","
                      "\"id\":\"another_id\","
                      "\"internment_id\":5,"
                      "\"alarms\":{"
                      "\"last_update\":\"%1\","
                      "\"spo2_lt\":%2,"
                      "\"spo2_delay_s\":%3,"
                      "\"hr_lt\":%4,"
                      "\"hr_gt\":%5,"
                      "\"hr_delay_s\":%6,"
                      "\"bt_lt\":%7,"
                      "\"bt_gt\":%8,"
                      "\"bt_delay_s\":%9,"
                      "\"bp_sys_lt\":%10,"
                      "\"bp_sys_gt\":%11,"
                      "\"bp_delay_s\":%12}}")
                      .arg(LAST_ALARM_UPDATE)
                      .arg(SPO2_MED)
                      .arg(SPO2_DELAY_MED)
                      .arg(HR_LT_MED)
                      .arg(HR_GT_MED)
                      .arg(Settings::heartRateDelaySMin() - 1)
                      .arg(BT_LT_MED)
                      .arg(BT_GT_MED)
                      .arg(BT_DELAY_MED)
                      .arg(BP_SYS_LT_MED)
                      .arg(BP_SYS_GT_MED)
                      .arg(BP_DELAY_MED);

    QTest::addRow("hr_delay_less_than_min") << newMosquittoMessage(payload)
                                            << DatakeeperQueryParser::CommandUnknown;

    payload = QString("{\"mac\":\"aabbccddeeff\",\"command\":\"set_alarms\","
                      "\"id\":\"another_id\","
                      "\"internment_id\":5,"
                      "\"alarms\":{"
                      "\"last_update\":\"%1\","
                      "\"spo2_lt\":%2,"
                      "\"spo2_delay_s\":%3,"
                      "\"hr_lt\":%4,"
                      "\"hr_gt\":%5,"
                      "\"hr_delay_s\":%6,"
                      "\"bt_lt\":%7,"
                      "\"bt_gt\":%8,"
                      "\"bt_delay_s\":%9,"
                      "\"bp_sys_lt\":%10,"
                      "\"bp_sys_gt\":%11,"
                      "\"bp_delay_s\":%12}}")
                      .arg(LAST_ALARM_UPDATE)
                      .arg(SPO2_MED)
                      .arg(SPO2_DELAY_MED)
                      .arg(HR_LT_MED)
                      .arg(HR_GT_MED)
                      .arg(Settings::heartRateDelaySMin())
                      .arg(BT_LT_MED)
                      .arg(BT_GT_MED)
                      .arg(BT_DELAY_MED)
                      .arg(BP_SYS_LT_MED)
                      .arg(BP_SYS_GT_MED)
                      .arg(BP_DELAY_MED);

    QTest::addRow("hr_delay_min") << newMosquittoMessage(payload)
                                  << DatakeeperQueryParser::CommandSetAlarmsThresholds;

    payload = QString("{\"mac\":\"aabbccddeeff\",\"command\":\"set_alarms\","
                      "\"id\":\"another_id\","
                      "\"internment_id\":5,"
                      "\"alarms\":{"
                      "\"last_update\":\"%1\","
                      "\"spo2_lt\":%2,"
                      "\"spo2_delay_s\":%3,"
                      "\"hr_lt\":%4,"
                      "\"hr_gt\":%5,"
                      "\"hr_delay_s\":%6,"
                      "\"bt_lt\":%7,"
                      "\"bt_gt\":%8,"
                      "\"bt_delay_s\":%9,"
                      "\"bp_sys_lt\":%10,"
                      "\"bp_sys_gt\":%11,"
                      "\"bp_delay_s\":%12}}")
                      .arg(LAST_ALARM_UPDATE)
                      .arg(SPO2_MED)
                      .arg(SPO2_DELAY_MED)
                      .arg(HR_LT_MED)
                      .arg(HR_GT_MED)
                      .arg(Settings::heartRateDelaySMax())
                      .arg(BT_LT_MED)
                      .arg(BT_GT_MED)
                      .arg(BT_DELAY_MED)
                      .arg(BP_SYS_LT_MED)
                      .arg(BP_SYS_GT_MED)
                      .arg(BP_DELAY_MED);

    QTest::addRow("hr_delay_max") << newMosquittoMessage(payload)
                                  << DatakeeperQueryParser::CommandSetAlarmsThresholds;

    payload = QString("{\"mac\":\"aabbccddeeff\",\"command\":\"set_alarms\","
                      "\"id\":\"another_id\","
                      "\"internment_id\":5,"
                      "\"alarms\":{"
                      "\"last_update\":\"%1\","
                      "\"spo2_lt\":%2,"
                      "\"spo2_delay_s\":%3,"
                      "\"hr_lt\":%4,"
                      "\"hr_gt\":%5,"
                      "\"hr_delay_s\":%6,"
                      "\"bt_lt\":%7,"
                      "\"bt_gt\":%8,"
                      "\"bt_delay_s\":%9,"
                      "\"bp_sys_lt\":%10,"
                      "\"bp_sys_gt\":%11,"
                      "\"bp_delay_s\":%12}}")
                      .arg(LAST_ALARM_UPDATE)
                      .arg(SPO2_MED)
                      .arg(SPO2_DELAY_MED)
                      .arg(HR_LT_MED)
                      .arg(HR_GT_MED)
                      .arg(Settings::heartRateDelaySMax() + 1)
                      .arg(BT_LT_MED)
                      .arg(BT_GT_MED)
                      .arg(BT_DELAY_MED)
                      .arg(BP_SYS_LT_MED)
                      .arg(BP_SYS_GT_MED)
                      .arg(BP_DELAY_MED);

    QTest::addRow("hr_delay_max+1") << newMosquittoMessage(payload)
                                    << DatakeeperQueryParser::CommandUnknown;

    // Blood pressure equal or less.
    payload = QString("{\"mac\":\"aabbccddeeff\",\"command\":\"set_alarms\","
                      "\"id\":\"another_id\","
                      "\"internment_id\":5,"
                      "\"alarms\":{"
                      "\"last_update\":\"%1\","
                      "\"spo2_lt\":%2,"
                      "\"spo2_delay_s\":%3,"
                      "\"hr_lt\":%4,"
                      "\"hr_gt\":%5,"
                      "\"hr_delay_s\":%6,"
                      "\"bt_lt\":%7,"
                      "\"bt_gt\":%8,"
                      "\"bt_delay_s\":%9,"
                      "\"bp_sys_lt\":%10,"
                      "\"bp_sys_gt\":%11,"
                      "\"bp_delay_s\":%12}}")
                      .arg(LAST_ALARM_UPDATE)
                      .arg(SPO2_MED)
                      .arg(SPO2_DELAY_MED)
                      .arg(HR_LT_MED)
                      .arg(HR_GT_MED)
                      .arg(HR_DELAY_MED)
                      .arg(BT_LT_MED)
                      .arg(BT_GT_MED)
                      .arg(BT_DELAY_MED)
                      .arg(Settings::instance().bloodPressureSysEqOrLessBot() - 1)
                      .arg(BP_SYS_GT_MED)
                      .arg(BP_DELAY_MED);

    QTest::addRow("bp_lt_less_than_min") << newMosquittoMessage(payload)
                                         << DatakeeperQueryParser::CommandUnknown;

    payload = QString("{\"mac\":\"aabbccddeeff\",\"command\":\"set_alarms\","
                      "\"id\":\"another_id\","
                      "\"internment_id\":5,"
                      "\"alarms\":{"
                      "\"last_update\":\"%1\","
                      "\"spo2_lt\":%2,"
                      "\"spo2_delay_s\":%3,"
                      "\"hr_lt\":%4,"
                      "\"hr_gt\":%5,"
                      "\"hr_delay_s\":%6,"
                      "\"bt_lt\":%7,"
                      "\"bt_gt\":%8,"
                      "\"bt_delay_s\":%9,"
                      "\"bp_sys_lt\":%10,"
                      "\"bp_sys_gt\":%11,"
                      "\"bp_delay_s\":%12}}")
                      .arg(LAST_ALARM_UPDATE)
                      .arg(SPO2_MED)
                      .arg(SPO2_DELAY_MED)
                      .arg(HR_LT_MED)
                      .arg(HR_GT_MED)
                      .arg(HR_DELAY_MED)
                      .arg(BT_LT_MED)
                      .arg(BT_GT_MED)
                      .arg(BT_DELAY_MED)
                      .arg(Settings::instance().bloodPressureSysEqOrLessBot())
                      .arg(BP_SYS_GT_MED)
                      .arg(BP_DELAY_MED);

    QTest::addRow("bp_lt_min") << newMosquittoMessage(payload)
                               << DatakeeperQueryParser::CommandSetAlarmsThresholds;

    payload = QString("{\"mac\":\"aabbccddeeff\",\"command\":\"set_alarms\","
                      "\"id\":\"another_id\","
                      "\"internment_id\":5,"
                      "\"alarms\":{"
                      "\"last_update\":\"%1\","
                      "\"spo2_lt\":%2,"
                      "\"spo2_delay_s\":%3,"
                      "\"hr_lt\":%4,"
                      "\"hr_gt\":%5,"
                      "\"hr_delay_s\":%6,"
                      "\"bt_lt\":%7,"
                      "\"bt_gt\":%8,"
                      "\"bt_delay_s\":%9,"
                      "\"bp_sys_lt\":%10,"
                      "\"bp_sys_gt\":%11,"
                      "\"bp_delay_s\":%12}}")
                      .arg(LAST_ALARM_UPDATE)
                      .arg(SPO2_MED)
                      .arg(SPO2_DELAY_MED)
                      .arg(HR_LT_MED)
                      .arg(HR_GT_MED)
                      .arg(HR_DELAY_MED)
                      .arg(BT_LT_MED)
                      .arg(BT_GT_MED)
                      .arg(BT_DELAY_MED)
                      .arg(Settings::instance().bloodPressureSysEqOrLessTop())
                      .arg(BP_SYS_GT_MED)
                      .arg(BP_DELAY_MED);

    QTest::addRow("bp_lt_max") << newMosquittoMessage(payload)
                               << DatakeeperQueryParser::CommandSetAlarmsThresholds;

    payload = QString("{\"mac\":\"aabbccddeeff\",\"command\":\"set_alarms\","
                      "\"id\":\"another_id\","
                      "\"internment_id\":5,"
                      "\"alarms\":{"
                      "\"last_update\":\"%1\","
                      "\"spo2_lt\":%2,"
                      "\"spo2_delay_s\":%3,"
                      "\"hr_lt\":%4,"
                      "\"hr_gt\":%5,"
                      "\"hr_delay_s\":%6,"
                      "\"bt_lt\":%7,"
                      "\"bt_gt\":%8,"
                      "\"bt_delay_s\":%9,"
                      "\"bp_sys_lt\":%10,"
                      "\"bp_sys_gt\":%11,"
                      "\"bp_delay_s\":%12}}")
                      .arg(LAST_ALARM_UPDATE)
                      .arg(SPO2_MED)
                      .arg(SPO2_DELAY_MED)
                      .arg(HR_LT_MED)
                      .arg(HR_GT_MED)
                      .arg(HR_DELAY_MED)
                      .arg(BT_LT_MED)
                      .arg(BT_GT_MED)
                      .arg(BT_DELAY_MED)
                      .arg(Settings::instance().bloodPressureSysEqOrLessTop() + 1)
                      .arg(BP_SYS_GT_MED)
                      .arg(BP_DELAY_MED);

    QTest::addRow("bp_lt_max+1") << newMosquittoMessage(payload)
                                 << DatakeeperQueryParser::CommandUnknown;

    // Blood pressure equal or greater.
    payload = QString("{\"mac\":\"aabbccddeeff\",\"command\":\"set_alarms\","
                      "\"id\":\"another_id\","
                      "\"internment_id\":5,"
                      "\"alarms\":{"
                      "\"last_update\":\"%1\","
                      "\"spo2_lt\":%2,"
                      "\"spo2_delay_s\":%3,"
                      "\"hr_lt\":%4,"
                      "\"hr_gt\":%5,"
                      "\"hr_delay_s\":%6,"
                      "\"bt_lt\":%7,"
                      "\"bt_gt\":%8,"
                      "\"bt_delay_s\":%9,"
                      "\"bp_sys_lt\":%10,"
                      "\"bp_sys_gt\":%11,"
                      "\"bp_delay_s\":%12}}")
                      .arg(LAST_ALARM_UPDATE)
                      .arg(SPO2_MED)
                      .arg(SPO2_DELAY_MED)
                      .arg(HR_LT_MED)
                      .arg(HR_GT_MED)
                      .arg(HR_DELAY_MED)
                      .arg(BT_LT_MED)
                      .arg(BT_GT_MED)
                      .arg(BT_DELAY_MED)
                      .arg(BP_SYS_LT_MED)
                      .arg(Settings::instance().bloodPressureSysEqOrMoreBot() - 1)
                      .arg(BP_DELAY_MED);

    QTest::addRow("bp_gt_less_than_min") << newMosquittoMessage(payload)
                                         << DatakeeperQueryParser::CommandUnknown;

    payload = QString("{\"mac\":\"aabbccddeeff\",\"command\":\"set_alarms\","
                      "\"id\":\"another_id\","
                      "\"internment_id\":5,"
                      "\"alarms\":{"
                      "\"last_update\":\"%1\","
                      "\"spo2_lt\":%2,"
                      "\"spo2_delay_s\":%3,"
                      "\"hr_lt\":%4,"
                      "\"hr_gt\":%5,"
                      "\"hr_delay_s\":%6,"
                      "\"bt_lt\":%7,"
                      "\"bt_gt\":%8,"
                      "\"bt_delay_s\":%9,"
                      "\"bp_sys_lt\":%10,"
                      "\"bp_sys_gt\":%11,"
                      "\"bp_delay_s\":%12}}")
                      .arg(LAST_ALARM_UPDATE)
                      .arg(SPO2_MED)
                      .arg(SPO2_DELAY_MED)
                      .arg(HR_LT_MED)
                      .arg(HR_GT_MED)
                      .arg(HR_DELAY_MED)
                      .arg(BT_LT_MED)
                      .arg(BT_GT_MED)
                      .arg(BT_DELAY_MED)
                      .arg(BP_SYS_LT_MED)
                      .arg(Settings::instance().bloodPressureSysEqOrMoreBot())
                      .arg(BP_DELAY_MED);

    QTest::addRow("bp_gt_min") << newMosquittoMessage(payload)
                               << DatakeeperQueryParser::CommandSetAlarmsThresholds;

    payload = QString("{\"mac\":\"aabbccddeeff\",\"command\":\"set_alarms\","
                      "\"id\":\"another_id\","
                      "\"internment_id\":5,"
                      "\"alarms\":{"
                      "\"last_update\":\"%1\","
                      "\"spo2_lt\":%2,"
                      "\"spo2_delay_s\":%3,"
                      "\"hr_lt\":%4,"
                      "\"hr_gt\":%5,"
                      "\"hr_delay_s\":%6,"
                      "\"bt_lt\":%7,"
                      "\"bt_gt\":%8,"
                      "\"bt_delay_s\":%9,"
                      "\"bp_sys_lt\":%10,"
                      "\"bp_sys_gt\":%11,"
                      "\"bp_delay_s\":%12}}")
                      .arg(LAST_ALARM_UPDATE)
                      .arg(SPO2_MED)
                      .arg(SPO2_DELAY_MED)
                      .arg(HR_LT_MED)
                      .arg(HR_GT_MED)
                      .arg(HR_DELAY_MED)
                      .arg(BT_LT_MED)
                      .arg(BT_GT_MED)
                      .arg(BT_DELAY_MED)
                      .arg(BP_SYS_LT_MED)
                      .arg(Settings::instance().bloodPressureSysEqOrMoreTop())
                      .arg(BP_DELAY_MED);

    QTest::addRow("bp_gt_max") << newMosquittoMessage(payload)
                               << DatakeeperQueryParser::CommandSetAlarmsThresholds;

    payload = QString("{\"mac\":\"aabbccddeeff\",\"command\":\"set_alarms\","
                      "\"id\":\"another_id\","
                      "\"internment_id\":5,"
                      "\"alarms\":{"
                      "\"last_update\":\"%1\","
                      "\"spo2_lt\":%2,"
                      "\"spo2_delay_s\":%3,"
                      "\"hr_lt\":%4,"
                      "\"hr_gt\":%5,"
                      "\"hr_delay_s\":%6,"
                      "\"bt_lt\":%7,"
                      "\"bt_gt\":%8,"
                      "\"bt_delay_s\":%9,"
                      "\"bp_sys_lt\":%10,"
                      "\"bp_sys_gt\":%11,"
                      "\"bp_delay_s\":%12}}")
                      .arg(LAST_ALARM_UPDATE)
                      .arg(SPO2_MED)
                      .arg(SPO2_DELAY_MED)
                      .arg(HR_LT_MED)
                      .arg(HR_GT_MED)
                      .arg(HR_DELAY_MED)
                      .arg(BT_LT_MED)
                      .arg(BT_GT_MED)
                      .arg(BT_DELAY_MED)
                      .arg(BP_SYS_LT_MED)
                      .arg(Settings::instance().bloodPressureSysEqOrMoreTop() + 1)
                      .arg(BP_DELAY_MED);

    QTest::addRow("bp_gt_max+1") << newMosquittoMessage(payload)
                                 << DatakeeperQueryParser::CommandUnknown;

    /*
     * BP LT < BP GT is currently enforced by the ranges that do not overlap,
     * so we can't test this condition.
     */

    // Blood pressure delay.
    payload = QString("{\"mac\":\"aabbccddeeff\",\"command\":\"set_alarms\","
                      "\"id\":\"another_id\","
                      "\"internment_id\":5,"
                      "\"alarms\":{"
                      "\"last_update\":\"%1\","
                      "\"spo2_lt\":%2,"
                      "\"spo2_delay_s\":%3,"
                      "\"hr_lt\":%4,"
                      "\"hr_gt\":%5,"
                      "\"hr_delay_s\":%6,"
                      "\"bt_lt\":%7,"
                      "\"bt_gt\":%8,"
                      "\"bt_delay_s\":%9,"
                      "\"bp_sys_lt\":%10,"
                      "\"bp_sys_gt\":%11,"
                      "\"bp_delay_s\":%12}}")
                      .arg(LAST_ALARM_UPDATE)
                      .arg(SPO2_MED)
                      .arg(SPO2_DELAY_MED)
                      .arg(HR_LT_MED)
                      .arg(HR_GT_MED)
                      .arg(HR_DELAY_MED)
                      .arg(BT_LT_MED)
                      .arg(BT_GT_MED)
                      .arg(BT_DELAY_MED)
                      .arg(BP_SYS_LT_MED)
                      .arg(BP_SYS_GT_MED)
                      .arg(Settings::bloodPressureDelaySMin() - 1);

    QTest::addRow("bp_delay_less_than_min") << newMosquittoMessage(payload)
                                            << DatakeeperQueryParser::CommandUnknown;

    payload = QString("{\"mac\":\"aabbccddeeff\",\"command\":\"set_alarms\","
                      "\"id\":\"another_id\","
                      "\"internment_id\":5,"
                      "\"alarms\":{"
                      "\"last_update\":\"%1\","
                      "\"spo2_lt\":%2,"
                      "\"spo2_delay_s\":%3,"
                      "\"hr_lt\":%4,"
                      "\"hr_gt\":%5,"
                      "\"hr_delay_s\":%6,"
                      "\"bt_lt\":%7,"
                      "\"bt_gt\":%8,"
                      "\"bt_delay_s\":%9,"
                      "\"bp_sys_lt\":%10,"
                      "\"bp_sys_gt\":%11,"
                      "\"bp_delay_s\":%12}}")
                      .arg(LAST_ALARM_UPDATE)
                      .arg(SPO2_MED)
                      .arg(SPO2_DELAY_MED)
                      .arg(HR_LT_MED)
                      .arg(HR_GT_MED)
                      .arg(HR_DELAY_MED)
                      .arg(BT_LT_MED)
                      .arg(BT_GT_MED)
                      .arg(BT_DELAY_MED)
                      .arg(BP_SYS_LT_MED)
                      .arg(BP_SYS_GT_MED)
                      .arg(Settings::bloodPressureDelaySMin());

    QTest::addRow("bp_delay_min") << newMosquittoMessage(payload)
                                  << DatakeeperQueryParser::CommandSetAlarmsThresholds;

    payload = QString("{\"mac\":\"aabbccddeeff\",\"command\":\"set_alarms\","
                      "\"id\":\"another_id\","
                      "\"internment_id\":5,"
                      "\"alarms\":{"
                      "\"last_update\":\"%1\","
                      "\"spo2_lt\":%2,"
                      "\"spo2_delay_s\":%3,"
                      "\"hr_lt\":%4,"
                      "\"hr_gt\":%5,"
                      "\"hr_delay_s\":%6,"
                      "\"bt_lt\":%7,"
                      "\"bt_gt\":%8,"
                      "\"bt_delay_s\":%9,"
                      "\"bp_sys_lt\":%10,"
                      "\"bp_sys_gt\":%11,"
                      "\"bp_delay_s\":%12}}")
                      .arg(LAST_ALARM_UPDATE)
                      .arg(SPO2_MED)
                      .arg(SPO2_DELAY_MED)
                      .arg(HR_LT_MED)
                      .arg(HR_GT_MED)
                      .arg(HR_DELAY_MED)
                      .arg(BT_LT_MED)
                      .arg(BT_GT_MED)
                      .arg(BT_DELAY_MED)
                      .arg(BP_SYS_LT_MED)
                      .arg(BP_SYS_GT_MED)
                      .arg(Settings::bloodPressureDelaySMax());

    QTest::addRow("bp_delay_max") << newMosquittoMessage(payload)
                                  << DatakeeperQueryParser::CommandSetAlarmsThresholds;

    payload = QString("{\"mac\":\"aabbccddeeff\",\"command\":\"set_alarms\","
                      "\"id\":\"another_id\","
                      "\"internment_id\":5,"
                      "\"alarms\":{"
                      "\"last_update\":\"%1\","
                      "\"spo2_lt\":%2,"
                      "\"spo2_delay_s\":%3,"
                      "\"hr_lt\":%4,"
                      "\"hr_gt\":%5,"
                      "\"hr_delay_s\":%6,"
                      "\"bt_lt\":%7,"
                      "\"bt_gt\":%8,"
                      "\"bt_delay_s\":%9,"
                      "\"bp_sys_lt\":%10,"
                      "\"bp_sys_gt\":%11,"
                      "\"bp_delay_s\":%12}}")
                      .arg(LAST_ALARM_UPDATE)
                      .arg(SPO2_MED)
                      .arg(SPO2_DELAY_MED)
                      .arg(HR_LT_MED)
                      .arg(HR_GT_MED)
                      .arg(HR_DELAY_MED)
                      .arg(BT_LT_MED)
                      .arg(BT_GT_MED)
                      .arg(BT_DELAY_MED)
                      .arg(BP_SYS_LT_MED)
                      .arg(BP_SYS_GT_MED)
                      .arg(Settings::bloodPressureDelaySMax() + 1);

    QTest::addRow("bp_delay_max+1") << newMosquittoMessage(payload)
                                    << DatakeeperQueryParser::CommandUnknown;

    // Body temperature equal or less.
    payload = QString("{\"mac\":\"aabbccddeeff\",\"command\":\"set_alarms\","
                      "\"id\":\"another_id\","
                      "\"internment_id\":5,"
                      "\"alarms\":{"
                      "\"last_update\":\"%1\","
                      "\"spo2_lt\":%2,"
                      "\"spo2_delay_s\":%3,"
                      "\"hr_lt\":%4,"
                      "\"hr_gt\":%5,"
                      "\"hr_delay_s\":%6,"
                      "\"bt_lt\":%7,"
                      "\"bt_gt\":%8,"
                      "\"bt_delay_s\":%9,"
                      "\"bp_sys_lt\":%10,"
                      "\"bp_sys_gt\":%11,"
                      "\"bp_delay_s\":%12}}")
                      .arg(LAST_ALARM_UPDATE)
                      .arg(SPO2_MED)
                      .arg(SPO2_DELAY_MED)
                      .arg(HR_LT_MED)
                      .arg(HR_GT_MED)
                      .arg(HR_DELAY_MED)
                      .arg(Settings::instance().bodyTempEqOrLessWireBot() - 1)
                      .arg(BT_GT_MED)
                      .arg(BT_DELAY_MED)
                      .arg(BP_SYS_LT_MED)
                      .arg(BP_SYS_GT_MED)
                      .arg(BP_DELAY_MED);

    QTest::addRow("bt_lt_less_than_min") << newMosquittoMessage(payload)
                                         << DatakeeperQueryParser::CommandUnknown;

    payload = QString("{\"mac\":\"aabbccddeeff\",\"command\":\"set_alarms\","
                      "\"id\":\"another_id\","
                      "\"internment_id\":5,"
                      "\"alarms\":{"
                      "\"last_update\":\"%1\","
                      "\"spo2_lt\":%2,"
                      "\"spo2_delay_s\":%3,"
                      "\"hr_lt\":%4,"
                      "\"hr_gt\":%5,"
                      "\"hr_delay_s\":%6,"
                      "\"bt_lt\":%7,"
                      "\"bt_gt\":%8,"
                      "\"bt_delay_s\":%9,"
                      "\"bp_sys_lt\":%10,"
                      "\"bp_sys_gt\":%11,"
                      "\"bp_delay_s\":%12}}")
                      .arg(LAST_ALARM_UPDATE)
                      .arg(SPO2_MED)
                      .arg(SPO2_DELAY_MED)
                      .arg(HR_LT_MED)
                      .arg(HR_GT_MED)
                      .arg(HR_DELAY_MED)
                      .arg(Settings::instance().bodyTempEqOrLessWireBot())
                      .arg(BT_GT_MED)
                      .arg(BT_DELAY_MED)
                      .arg(BP_SYS_LT_MED)
                      .arg(BP_SYS_GT_MED)
                      .arg(BP_DELAY_MED);

    QTest::addRow("bt_lt_min") << newMosquittoMessage(payload)
                               << DatakeeperQueryParser::CommandSetAlarmsThresholds;

    payload = QString("{\"mac\":\"aabbccddeeff\",\"command\":\"set_alarms\","
                      "\"id\":\"another_id\","
                      "\"internment_id\":5,"
                      "\"alarms\":{"
                      "\"last_update\":\"%1\","
                      "\"spo2_lt\":%2,"
                      "\"spo2_delay_s\":%3,"
                      "\"hr_lt\":%4,"
                      "\"hr_gt\":%5,"
                      "\"hr_delay_s\":%6,"
                      "\"bt_lt\":%7,"
                      "\"bt_gt\":%8,"
                      "\"bt_delay_s\":%9,"
                      "\"bp_sys_lt\":%10,"
                      "\"bp_sys_gt\":%11,"
                      "\"bp_delay_s\":%12}}")
                      .arg(LAST_ALARM_UPDATE)
                      .arg(SPO2_MED)
                      .arg(SPO2_DELAY_MED)
                      .arg(HR_LT_MED)
                      .arg(HR_GT_MED)
                      .arg(HR_DELAY_MED)
                      .arg(Settings::instance().bodyTempEqOrLessWireTop())
                      .arg(BT_GT_MED)
                      .arg(BT_DELAY_MED)
                      .arg(BP_SYS_LT_MED)
                      .arg(BP_SYS_GT_MED)
                      .arg(BP_DELAY_MED);

    QTest::addRow("bt_lt_max") << newMosquittoMessage(payload)
                               << DatakeeperQueryParser::CommandSetAlarmsThresholds;

    payload = QString("{\"mac\":\"aabbccddeeff\",\"command\":\"set_alarms\","
                      "\"id\":\"another_id\","
                      "\"internment_id\":5,"
                      "\"alarms\":{"
                      "\"last_update\":\"%1\","
                      "\"spo2_lt\":%2,"
                      "\"spo2_delay_s\":%3,"
                      "\"hr_lt\":%4,"
                      "\"hr_gt\":%5,"
                      "\"hr_delay_s\":%6,"
                      "\"bt_lt\":%7,"
                      "\"bt_gt\":%8,"
                      "\"bt_delay_s\":%9,"
                      "\"bp_sys_lt\":%10,"
                      "\"bp_sys_gt\":%11,"
                      "\"bp_delay_s\":%12}}")
                      .arg(LAST_ALARM_UPDATE)
                      .arg(SPO2_MED)
                      .arg(SPO2_DELAY_MED)
                      .arg(HR_LT_MED)
                      .arg(HR_GT_MED)
                      .arg(HR_DELAY_MED)
                      .arg(Settings::instance().bodyTempEqOrMoreWireTop() + 1)
                      .arg(BT_GT_MED)
                      .arg(BT_DELAY_MED)
                      .arg(BP_SYS_LT_MED)
                      .arg(BP_SYS_GT_MED)
                      .arg(BP_DELAY_MED);

    QTest::addRow("bt_lt_max+1") << newMosquittoMessage(payload)
                                 << DatakeeperQueryParser::CommandUnknown;

    // Body temperature equal or more.
    payload = QString("{\"mac\":\"aabbccddeeff\",\"command\":\"set_alarms\","
                      "\"id\":\"another_id\","
                      "\"internment_id\":5,"
                      "\"alarms\":{"
                      "\"last_update\":\"%1\","
                      "\"spo2_lt\":%2,"
                      "\"spo2_delay_s\":%3,"
                      "\"hr_lt\":%4,"
                      "\"hr_gt\":%5,"
                      "\"hr_delay_s\":%6,"
                      "\"bt_lt\":%7,"
                      "\"bt_gt\":%8,"
                      "\"bt_delay_s\":%9,"
                      "\"bp_sys_lt\":%10,"
                      "\"bp_sys_gt\":%11,"
                      "\"bp_delay_s\":%12}}")
                      .arg(LAST_ALARM_UPDATE)
                      .arg(SPO2_MED)
                      .arg(SPO2_DELAY_MED)
                      .arg(HR_LT_MED)
                      .arg(HR_GT_MED)
                      .arg(HR_DELAY_MED)
                      .arg(BT_LT_MED)
                      .arg(Settings::instance().bodyTempEqOrMoreWireBot() - 1)
                      .arg(BT_DELAY_MED)
                      .arg(BP_SYS_LT_MED)
                      .arg(BP_SYS_GT_MED)
                      .arg(BP_DELAY_MED);

    QTest::addRow("bt_gt_less_than_min") << newMosquittoMessage(payload)
                                         << DatakeeperQueryParser::CommandUnknown;

    payload = QString("{\"mac\":\"aabbccddeeff\",\"command\":\"set_alarms\","
                      "\"id\":\"another_id\","
                      "\"internment_id\":5,"
                      "\"alarms\":{"
                      "\"last_update\":\"%1\","
                      "\"spo2_lt\":%2,"
                      "\"spo2_delay_s\":%3,"
                      "\"hr_lt\":%4,"
                      "\"hr_gt\":%5,"
                      "\"hr_delay_s\":%6,"
                      "\"bt_lt\":%7,"
                      "\"bt_gt\":%8,"
                      "\"bt_delay_s\":%9,"
                      "\"bp_sys_lt\":%10,"
                      "\"bp_sys_gt\":%11,"
                      "\"bp_delay_s\":%12}}")
                      .arg(LAST_ALARM_UPDATE)
                      .arg(SPO2_MED)
                      .arg(SPO2_DELAY_MED)
                      .arg(HR_LT_MED)
                      .arg(HR_GT_MED)
                      .arg(HR_DELAY_MED)
                      .arg(BT_LT_MED)
                      .arg(Settings::instance().bodyTempEqOrMoreWireBot())
                      .arg(BT_DELAY_MED)
                      .arg(BP_SYS_LT_MED)
                      .arg(BP_SYS_GT_MED)
                      .arg(BP_DELAY_MED);

    QTest::addRow("bt_gt_min") << newMosquittoMessage(payload)
                               << DatakeeperQueryParser::CommandSetAlarmsThresholds;

    payload = QString("{\"mac\":\"aabbccddeeff\",\"command\":\"set_alarms\","
                      "\"id\":\"another_id\","
                      "\"internment_id\":5,"
                      "\"alarms\":{"
                      "\"last_update\":\"%1\","
                      "\"spo2_lt\":%2,"
                      "\"spo2_delay_s\":%3,"
                      "\"hr_lt\":%4,"
                      "\"hr_gt\":%5,"
                      "\"hr_delay_s\":%6,"
                      "\"bt_lt\":%7,"
                      "\"bt_gt\":%8,"
                      "\"bt_delay_s\":%9,"
                      "\"bp_sys_lt\":%10,"
                      "\"bp_sys_gt\":%11,"
                      "\"bp_delay_s\":%12}}")
                      .arg(LAST_ALARM_UPDATE)
                      .arg(SPO2_MED)
                      .arg(SPO2_DELAY_MED)
                      .arg(HR_LT_MED)
                      .arg(HR_GT_MED)
                      .arg(HR_DELAY_MED)
                      .arg(BT_LT_MED)
                      .arg(Settings::instance().bodyTempEqOrMoreWireTop())
                      .arg(BT_DELAY_MED)
                      .arg(BP_SYS_LT_MED)
                      .arg(BP_SYS_GT_MED)
                      .arg(BP_DELAY_MED);

    QTest::addRow("bt_gt_max") << newMosquittoMessage(payload)
                               << DatakeeperQueryParser::CommandSetAlarmsThresholds;

    payload = QString("{\"mac\":\"aabbccddeeff\",\"command\":\"set_alarms\","
                      "\"id\":\"another_id\","
                      "\"internment_id\":5,"
                      "\"alarms\":{"
                      "\"last_update\":\"%1\","
                      "\"spo2_lt\":%2,"
                      "\"spo2_delay_s\":%3,"
                      "\"hr_lt\":%4,"
                      "\"hr_gt\":%5,"
                      "\"hr_delay_s\":%6,"
                      "\"bt_lt\":%7,"
                      "\"bt_gt\":%8,"
                      "\"bt_delay_s\":%9,"
                      "\"bp_sys_lt\":%10,"
                      "\"bp_sys_gt\":%11,"
                      "\"bp_delay_s\":%12}}")
                      .arg(LAST_ALARM_UPDATE)
                      .arg(SPO2_MED)
                      .arg(SPO2_DELAY_MED)
                      .arg(HR_LT_MED)
                      .arg(HR_GT_MED)
                      .arg(HR_DELAY_MED)
                      .arg(BT_LT_MED)
                      .arg(Settings::instance().bodyTempEqOrMoreWireTop() + 1)
                      .arg(BT_DELAY_MED)
                      .arg(BP_SYS_LT_MED)
                      .arg(BP_SYS_GT_MED)
                      .arg(BP_DELAY_MED);

    QTest::addRow("bt_gt_max+1") << newMosquittoMessage(payload)
                                 << DatakeeperQueryParser::CommandUnknown;

    // Body temperature delay.
    payload = QString("{\"mac\":\"aabbccddeeff\",\"command\":\"set_alarms\","
                      "\"id\":\"another_id\","
                      "\"internment_id\":5,"
                      "\"alarms\":{"
                      "\"last_update\":\"%1\","
                      "\"spo2_lt\":%2,"
                      "\"spo2_delay_s\":%3,"
                      "\"hr_lt\":%4,"
                      "\"hr_gt\":%5,"
                      "\"hr_delay_s\":%6,"
                      "\"bt_lt\":%7,"
                      "\"bt_gt\":%8,"
                      "\"bt_delay_s\":%9,"
                      "\"bp_sys_lt\":%10,"
                      "\"bp_sys_gt\":%11,"
                      "\"bp_delay_s\":%12}}")
                      .arg(LAST_ALARM_UPDATE)
                      .arg(SPO2_MED)
                      .arg(SPO2_DELAY_MED)
                      .arg(HR_LT_MED)
                      .arg(HR_GT_MED)
                      .arg(HR_DELAY_MED)
                      .arg(BT_LT_MED)
                      .arg(BT_GT_MED)
                      .arg(Settings::bodyTempDelaySMin() - 1)
                      .arg(BP_SYS_LT_MED)
                      .arg(BP_SYS_GT_MED)
                      .arg(BP_DELAY_MED);

    QTest::addRow("bt_delay_less_than_min") << newMosquittoMessage(payload)
                                            << DatakeeperQueryParser::CommandUnknown;

    payload = QString("{\"mac\":\"aabbccddeeff\",\"command\":\"set_alarms\","
                      "\"id\":\"another_id\","
                      "\"internment_id\":5,"
                      "\"alarms\":{"
                      "\"last_update\":\"%1\","
                      "\"spo2_lt\":%2,"
                      "\"spo2_delay_s\":%3,"
                      "\"hr_lt\":%4,"
                      "\"hr_gt\":%5,"
                      "\"hr_delay_s\":%6,"
                      "\"bt_lt\":%7,"
                      "\"bt_gt\":%8,"
                      "\"bt_delay_s\":%9,"
                      "\"bp_sys_lt\":%10,"
                      "\"bp_sys_gt\":%11,"
                      "\"bp_delay_s\":%12}}")
                      .arg(LAST_ALARM_UPDATE)
                      .arg(SPO2_MED)
                      .arg(SPO2_DELAY_MED)
                      .arg(HR_LT_MED)
                      .arg(HR_GT_MED)
                      .arg(HR_DELAY_MED)
                      .arg(BT_LT_MED)
                      .arg(BT_GT_MED)
                      .arg(Settings::bodyTempDelaySMin())
                      .arg(BP_SYS_LT_MED)
                      .arg(BP_SYS_GT_MED)
                      .arg(BP_DELAY_MED);

    QTest::addRow("bt_delay_min") << newMosquittoMessage(payload)
                                  << DatakeeperQueryParser::CommandSetAlarmsThresholds;

    payload = QString("{\"mac\":\"aabbccddeeff\",\"command\":\"set_alarms\","
                      "\"id\":\"another_id\","
                      "\"internment_id\":5,"
                      "\"alarms\":{"
                      "\"last_update\":\"%1\","
                      "\"spo2_lt\":%2,"
                      "\"spo2_delay_s\":%3,"
                      "\"hr_lt\":%4,"
                      "\"hr_gt\":%5,"
                      "\"hr_delay_s\":%6,"
                      "\"bt_lt\":%7,"
                      "\"bt_gt\":%8,"
                      "\"bt_delay_s\":%9,"
                      "\"bp_sys_lt\":%10,"
                      "\"bp_sys_gt\":%11,"
                      "\"bp_delay_s\":%12}}")
                      .arg(LAST_ALARM_UPDATE)
                      .arg(SPO2_MED)
                      .arg(SPO2_DELAY_MED)
                      .arg(HR_LT_MED)
                      .arg(HR_GT_MED)
                      .arg(HR_DELAY_MED)
                      .arg(BT_LT_MED)
                      .arg(BT_GT_MED)
                      .arg(Settings::bodyTempDelaySMax())
                      .arg(BP_SYS_LT_MED)
                      .arg(BP_SYS_GT_MED)
                      .arg(BP_DELAY_MED);

    QTest::addRow("bt_delay_max") << newMosquittoMessage(payload)
                                  << DatakeeperQueryParser::CommandSetAlarmsThresholds;

    payload = QString("{\"mac\":\"aabbccddeeff\",\"command\":\"set_alarms\","
                      "\"id\":\"another_id\","
                      "\"internment_id\":5,"
                      "\"alarms\":{"
                      "\"last_update\":\"%1\","
                      "\"spo2_lt\":%2,"
                      "\"spo2_delay_s\":%3,"
                      "\"hr_lt\":%4,"
                      "\"hr_gt\":%5,"
                      "\"hr_delay_s\":%6,"
                      "\"bt_lt\":%7,"
                      "\"bt_gt\":%8,"
                      "\"bt_delay_s\":%9,"
                      "\"bp_sys_lt\":%10,"
                      "\"bp_sys_gt\":%11,"
                      "\"bp_delay_s\":%12}}")
                      .arg(LAST_ALARM_UPDATE)
                      .arg(SPO2_MED)
                      .arg(SPO2_DELAY_MED)
                      .arg(HR_LT_MED)
                      .arg(HR_GT_MED)
                      .arg(HR_DELAY_MED)
                      .arg(BT_LT_MED)
                      .arg(BT_GT_MED)
                      .arg(Settings::bodyTempDelaySMax() + 1)
                      .arg(BP_SYS_LT_MED)
                      .arg(BP_SYS_GT_MED)
                      .arg(BP_DELAY_MED);

    QTest::addRow("bt_gt_max+1") << newMosquittoMessage(payload)
                                 << DatakeeperQueryParser::CommandUnknown;
}

void DatakeeperQueryParserTests::checkSetAlarmsRanges()
{
    DatakeeperQueryParser::values values;
    clearValues(values);

    QFETCH(MosquittoMessage *, msg);
    QFETCH(DatakeeperQueryParser::Commands, expectedCommand);

    QVERIFY2(msg != nullptr, "Message should not be a null pointer");

    auto retval = DatakeeperQueryParser::parseMessage(&(msg->msg), values);

    QVERIFY2(retval == expectedCommand, "retval should match expectedResult");
}

void DatakeeperQueryParserTests::clearValues(DatakeeperQueryParser::values &values)
{
    values.mac.clear();
    values.command.clear();
    values.id.clear();

    values.internment_id = -1;
    values.fromTime = -1;
    values.toTime = -1;
    values.numOfSamples = -1;

    values.spo2LT = 0;
    values.hRLT = 0;
    values.hRGT = 0;
    values.bTLT = 0;
    values.bTGT = 0;
    values.bPSysLT = 0;
    values.bPSysGT = 0;
}

MosquittoMessage *DatakeeperQueryParserTests::newMosquittoMessage(const QString &payload)
{
    return new MosquittoMessage(DatakeeperQueryParser::topic(), payload, this);
}

QTEST_MAIN(DatakeeperQueryParserTests)
#include "datakeeperqueryparser_tst.moc"
