#ifndef DEVICESINFOPARSERTESTS_H
#define DEVICESINFOPARSERTESTS_H

#include <QObject>
#include <QtTest/QTest>

#include <mosquitto.h>
#include "mosquittomessage.h"
#include "../src/devicesinfoparser.h"

Q_DECLARE_METATYPE(DevicesInfoParser::values);

class DevicesInfoParserTests : public QObject
{
    Q_OBJECT

private slots:
    void checkTopic();
    void checkJson_data();
    void checkJson();

    void checkRanges_data();
    void checkRanges();

private:
    MosquittoMessage * newMosquittoMessage(const QString &payload);
};

#endif // DEVICESINFOPARSERTESTS_H
