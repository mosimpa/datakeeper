#include <QtGlobal>
#include <QString>
#include <QStringList>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QFile>
#include <QTemporaryFile>
#include <QProcess>
#include <QDebug>
#include <QDateTime>
#include <QSqlError>
#include <QSqlDriver>
#include <QSignalSpy>
#include <QScopedPointer>

#include <mosimpaqt/scaling.h>
#include <mosimpaqt/definitions.h>

#include "../src/settings.h"
#include "../src/engine.h"
#include "../src/datakeeperqueryparser.h"

#include "mosquittomessage.h"
#include "engine_tst.h"

using namespace MoSimPa;

void EngineTests::initTestCase()
{
    QString pgHost(qgetenv("PGHOST"));
    QString pgDB(qgetenv("PGDATABASE"));
    QString pgUser(qgetenv("PGUSER"));
    QString pgPwd(qgetenv("PGPASSWORD"));

    qDebug() << "Hostname: " << pgHost;
    qDebug() << "Database: " << pgDB;
    qDebug() << "User: " << pgUser;
    qDebug() << "Password: ommited";

    QVERIFY2(!pgHost.isEmpty(), "Hostname should not be empty");
    QVERIFY2(!pgDB.isEmpty(), "Database should not be empty");
    QVERIFY2(!pgUser.isEmpty(), "user should not be empty");
    QVERIFY2(!pgPwd.isEmpty(), "Password should not be empty");

    QSqlDatabase db = QSqlDatabase::addDatabase("QPSQL");
    db.setHostName(pgHost);
    db.setDatabaseName(pgDB);
    db.setUserName(pgUser);
    db.setPassword(pgPwd);

    QVERIFY2(db.open(), "The database connection needs to be opened.");
}

void EngineTests::cleanupTestCase()
{
    QSqlDatabase::database().close();
    QSqlDatabase::removeDatabase(QSqlDatabase::database().connectionName());
}

void EngineTests::check00DBCreation()
{
    // We first need to create the database schema.
    QVERIFY2(queriesFromFile(":/creation/create_tables.sql"), "create_tables.sql could not be loaded.");
    QVERIFY2(queriesFromFile(":/creation/create_stored_procedures.sql"), "create_stored_procedures.sql could not be loaded.");
    QVERIFY2(queriesFromFile(":/creation/create_views.sql"), "create_views.sql could not be loaded.");
    QVERIFY2(queriesFromFile(":/creation/create_index.sql"), "create_index.sql could not be loaded.");

    QFile origUserFile(":/creation/users/create_user_datakeeper.sql");
    QVERIFY2(origUserFile.open(QIODevice::ReadOnly | QIODevice::Text), "File should be created and opened.");

    QFile userFile(QString("%1/%2_%3_user.sql").arg(QDir::tempPath())
                                               .arg(QCoreApplication::applicationName())
                                               .arg(QCoreApplication::applicationPid()));
    QVERIFY2(userFile.open(QIODevice::WriteOnly | QIODevice::Text), "File should be created and opened.");

    QTextStream in(&origUserFile);
    QTextStream out(&userFile);
    while (!in.atEnd()) {
        QString line = in.readLine();
        if(line.contains("mosimpa-datakeeper"))
            line.replace("mosimpa-datakeeper", QSqlDatabase::database().databaseName());
        out << line;
    }
    out.flush();
    userFile.flush();
    userFile.close();

    QVERIFY2(queriesFromFile(userFile.fileName()), "create_user_datakeeper.sql could not be loaded.");
    userFile.remove();

    // Add the necessary data.
    addDevicesPatientsLocationsAndInternments(BASE_ID, BASE_MAC, NUM_ENTRIES);
}

void EngineTests::check10QueryCommandInternments()
{
    auto engine = new Engine(this);

    QString payload = QStringLiteral("{\"mac\":\"f8597159e921\",\"command\":\"internments\",\"id\":\"2\"}");

    QScopedPointer<MosquittoMessage> msg(new MosquittoMessage(DatakeeperQueryParser::topic(), payload, this));
    engine->parseMessage(&(msg->msg));

    /// \todo We do really need a MQTT server working in order to test this.

}

/// \todo This still needs verifying the returned JSON value.
void EngineTests::check20UpdateAlarms()
{
    auto engine = new Engine(this);

    /*
     * We forced entries to start with BASE_ID, so we should be able to update
     * that value.
     */

    DatakeeperQueryParser::values values;

    // The MAC of the device that requested the change.
    values.mac = "001122334455";
    values.lastAlarmUpdate = LAST_UPDATE_TS;
    values.internment_id = BASE_ID;

    /*
     * Values have been checked by the parser and will be checked by the database,
     * so we just need to set them up once.
     */
    values.spo2LT = Settings::wireSpO2EqOrLessPercBot();
    values.spo2AlarmDelayS = Settings::spO2DelaySMin();

    values.hRLT = Settings::heartRateEqOrLessWireBot();
    values.hRGT = Settings::heartRateEqOrMoreWireBot();
    values.hRAlarmDelayS = Settings::heartRateDelaySMax();

    values.bTLT = Settings::bodyTempEqOrLessWireBot();
    values.bTGT = Settings::bodyTempEqOrMoreWireTop();
    values.bTAlarmDelayS = Settings::bodyTempDelaySMax();

    values.bPSysLT = Settings::bloodPressureSysEqOrLessBot();
    values.bPSysGT = Settings::bloodPressureSysEqOrMoreBot();
    values.bPAlarmDelayS = Settings::bloodPressureDelaySMax();

    auto retval = engine->updateAlarmsThresholds(values);
    QVERIFY2(retval, "We should be able to update an existing entry.");

    // Repeting the update should fail, as BASE_ID is now not the last entry.
    retval = engine->updateAlarmsThresholds(values);
    QVERIFY2(!retval, "This should fail as BASE_ID is no longer the last update.");
}

bool EngineTests::queriesFromFile(const QString &filePath)
{
    QString thePath = filePath;
    QFile file(filePath);
    QTemporaryFile * tmpFile = QTemporaryFile::createNativeFile(file);
    if(tmpFile != nullptr)
    {
         if(!tmpFile->isReadable())
             return false;

         thePath = tmpFile->fileName();
    }

    auto db = QSqlDatabase::database();

    QStringList args;
    args << "-h" << db.hostName();
    args << "-d" << db.databaseName();
    args << "-U" << db.userName();
    args << "-w";
    args << "-f" << thePath;

    QProcess psql;
    psql.start("psql", args);
    psql.waitForFinished();

    QTextStream out(stdout);
    out << psql.readAllStandardOutput();
    out << psql.readAllStandardError();

    if(psql.exitStatus() != QProcess::NormalExit)
    {
        qDebug() << "psql process ended with error: " << psql.errorString();
        return false;
    }

    return true;
}

void EngineTests::addDevicesPatientsLocationsAndInternments(const int32_t baseID, const QString &baseMac, const int32_t number)
{
    /*
     * First let's add 3 devices, 3 patients, 3 locations and 3 internments,
     * this time specifying the IDs ourselves.
     */
    for(int i = 0; i < number; i++)
    {
        bool isQueryOK;

        // Devices.
        QString queryString;
        queryString = QString("INSERT INTO "
                              "devices(mac, date_time_added, date_time_last_seen,battmv,timems,msgid) "
                              "values('%1', "
                              "cast('%2' as timestamp), cast('%3' as timestamp), "
                              "%4, %5, %6)")
                              .arg(createMac(baseMac, i))
                              .arg(currentDateTime())
                              .arg(currentDateTime())
                              .arg(i+1*1000)
                              .arg(i+1*458)
                              .arg(i);

        QSqlQuery query;
        isQueryOK = query.exec(queryString);
        if(!isQueryOK)
            qDebug() << query.lastError();
        QVERIFY2(isQueryOK, "Query to add device should not fail");

        // Locations.
        QString locType;
        if(i%2 == 0)
            locType = QStringLiteral("local");
        else
            locType = QStringLiteral("external");

        queryString = QString("INSERT INTO locations(location_id, type, description) "
                              "values(%1, '%2', 'Test %3')")
                             .arg(baseID + i)
                             .arg(locType)
                             .arg(baseID + i);
        isQueryOK = query.exec(queryString);
        if(!isQueryOK)
            qDebug() << query.lastError();
        QVERIFY2(isQueryOK, "Query to add location should not fail");

        // Patients.
        QString gender;
        switch(i%3) {
        case 0:
            gender = QStringLiteral("m");
            break;

        case 1:
            gender = QStringLiteral("f");
            break;

        case 2:
            gender = QStringLiteral("o");
            break;
        }

        queryString = QString("INSERT INTO "
                              "patients(patient_id, date_time_added, name, surname, "
                              "age, gender, dni, comments) "
                              "values(%1, cast('%2' as timestamptz),'Name %3', 'Surname %4', %5::SMALLINT, '%6', %7, 'A comment')")
                             .arg(i+baseID)
                             .arg(currentDateTime())
                             .arg(i)
                             .arg(i)
                             .arg(3*i)
                             .arg(gender)
                             .arg(i*12456895);

        isQueryOK = query.exec(queryString);
        if(!isQueryOK)
            qDebug() << query.lastError();
        QVERIFY2(isQueryOK, "Query to add patient should not fail");

        // Internments
        queryString = QString("INSERT INTO "
                              "internments(internment_id, "
                              "date_time_begins, "
                              "patient_id, location_id, mac) "
                              "VALUES(%1,"
                              "cast('%2' as timestamptz), "
                              "%3, %4, '%5')")
                .arg(i+baseID)
                .arg(currentDateTime())
                .arg(i+baseID).arg(i+baseID).arg(createMac(baseMac, i));

        isQueryOK = query.exec(queryString);
        if(!isQueryOK)
            qDebug() << query.lastError();
        QVERIFY2(isQueryOK, "Query to add internment should not fail");

        /*
         * As we added internments manually in order to fixate their
         * internment_id we need to manually create the defaul alarm entries.
         */
        queryString = QString("INSERT INTO alarms(modify_ts, modifying_mac, internment_id) "
                              "VALUES(cast('%1' as timestamptz), '%2', %3)")
                .arg(LAST_UPDATE_TS).arg("000000000000").arg(i+baseID);

        isQueryOK = query.exec(queryString);
        if(!isQueryOK)
            qDebug() << query.lastError();
        QVERIFY2(isQueryOK, "Query to add alarm should not fail");
    }
}

QString EngineTests::currentDateTime()
{
    return QDateTime::currentDateTime().toString(Qt::ISODate);
}

QString EngineTests::createMac(const QString &base, int32_t lastOctet)
{
    lastOctet = lastOctet % 0x100;
    return QString("%1%2").arg(base).arg(lastOctet, 2, 16, QChar('0'));
}

QTEST_MAIN(EngineTests)
#include "engine_tst.moc"
