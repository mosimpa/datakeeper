#include <QtGlobal>
#include <QString>
#include <QStringList>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QFile>
#include <QTemporaryFile>
#include <QProcess>
#include <QDebug>
#include <QDateTime>
#include <QSqlError>
#include <QSqlDriver>
#include <QSignalSpy>
#include <QDateTime>

#include <mosimpaqt/scaling.h>
#include <mosimpaqt/definitions.h>

#include "../src/settings.h"
#include "../src/engine.h"

#include "db_tst.h"

using namespace MoSimPa;

void DBTests::initTestCase()
{
    QString pgHost(qgetenv("PGHOST"));
    QString pgDB(qgetenv("PGDATABASE"));
    QString pgUser(qgetenv("PGUSER"));
    QString pgPwd(qgetenv("PGPASSWORD"));

    qDebug() << "Hostname: " << pgHost;
    qDebug() << "Database: " << pgDB;
    qDebug() << "User: " << pgUser;
    qDebug() << "Password: ommited";

    QVERIFY2(!pgHost.isEmpty(), "Hostname should not be empty");
    QVERIFY2(!pgDB.isEmpty(), "Database should not be empty");
    QVERIFY2(!pgUser.isEmpty(), "user should not be empty");
    QVERIFY2(!pgPwd.isEmpty(), "Password should not be empty");

    QSqlDatabase db = QSqlDatabase::addDatabase("QPSQL");
    db.setHostName(pgHost);
    db.setDatabaseName(pgDB);
    db.setUserName(pgUser);
    db.setPassword(pgPwd);

    QVERIFY2(db.open(), "The database connection needs to be opened.");
}

void DBTests::cleanupTestCase()
{
    QSqlDatabase::database().close();
    QSqlDatabase::removeDatabase(QSqlDatabase::database().connectionName());
}

void DBTests::check00DBCreation()
{
    // We first need to create the database schema.
    QVERIFY2(queriesFromFile(":/creation/create_tables.sql"), "create_tables.sql could not be loaded.");
    QVERIFY2(queriesFromFile(":/creation/create_stored_procedures.sql"), "create_stored_procedures.sql could not be loaded.");
    QVERIFY2(queriesFromFile(":/creation/create_views.sql"), "create_views.sql could not be loaded.");
    QVERIFY2(queriesFromFile(":/creation/create_index.sql"), "create_index.sql could not be loaded.");

    QFile origUserFile(":/creation/users/create_user_datakeeper.sql");
    QVERIFY2(origUserFile.open(QIODevice::ReadOnly | QIODevice::Text), "File should be created and opened.");

    QFile userFile(QString("%1/%2_%3_user.sql").arg(QDir::tempPath())
                                               .arg(QCoreApplication::applicationName())
                                               .arg(QCoreApplication::applicationPid()));
    QVERIFY2(userFile.open(QIODevice::WriteOnly | QIODevice::Text), "File should be created and opened.");

    QTextStream in(&origUserFile);
    QTextStream out(&userFile);
    while (!in.atEnd()) {
        QString line = in.readLine();
        if(line.contains("mosimpa-datakeeper"))
            line.replace("mosimpa-datakeeper", QSqlDatabase::database().databaseName());
        out << line;
    }
    out.flush();
    userFile.flush();
    userFile.close();

    QVERIFY2(queriesFromFile(userFile.fileName()), "create_user_datakeeper.sql could not be loaded.");
    userFile.remove();
}

/**
 * @brief DBTests::check10AddDeviceSP_data
 *
 * Use int32_t instead of batt_mv_t and msg_id_t in order to check the DB's
 * constraints.
 */
void DBTests::check10AddDeviceSP_data()
{
    const QString macGood = QString("11:22:33:44:55:66");
    const int32_t battmVGood = 7000;
    const time_t timeGood = 1590591522; // Date and time (GMT): Wednesday, May 27, 2020 14:58:42
    const int32_t msgIdGood = 5;

    QTest::addColumn<QString>("mac");
    QTest::addColumn<int32_t>("battmV");
    QTest::addColumn<time_t>("time");
    QTest::addColumn<int32_t>("msgId");
    QTest::addColumn<bool>("result");

    QTest::addRow("good_everything") << macGood <<  battmVGood << timeGood << msgIdGood << true;
    QTest::addRow("bad_mac") << "notamac" <<  battmVGood << timeGood << msgIdGood << false;
    QTest::addRow("bad_battmV") << macGood <<  0 << timeGood << msgIdGood << false;
    QTest::addRow("bad_time") << macGood <<  battmVGood << static_cast<time_t>(-1) << msgIdGood << false;
    QTest::addRow("bad_msg_id") << macGood <<  battmVGood << timeGood << -1 << false;
}

void DBTests::check10AddDeviceSP()
{
    QFETCH(QString, mac);
    QFETCH(int32_t, battmV);
    QFETCH(time_t, time);
    QFETCH(int32_t, msgId);
    QFETCH(bool, result);

    QString queryString;
    queryString = QString("SELECT add_or_update_device('%1', %2, %3, %4)")
                         .arg(mac)
                         .arg(battmV)
                         .arg(time)
                         .arg(msgId);

    QSqlQuery query;
    bool retval = query.exec(queryString);
    QVERIFY2(retval == result, "retval should be equal to the expected result");

    if(!retval)
        return;

    // Get the last seen timestamp.
    queryString = QString("SELECT date_time_last_seen FROM devices WHERE mac = '%1'").arg(mac);
    retval = query.exec(queryString);
    QVERIFY2(retval == true, "The query must suceed");
    QVERIFY2(query.size() == 1, "The query result should have only one row.");

    query.next();
    auto dateTime = query.value(0).toDateTime();
    QVERIFY2(dateTime.isValid(), "The obtained date and time must be valid");

    // Wait 1 second and check that we can update the last seen timestamp.
    QTest::qSleep(1100);

    queryString = QString("SELECT update_device_last_seen_timestamp('%1')").arg(mac);

    retval = query.exec(queryString);
    QVERIFY2(retval == true, "The update last seen timestamp query must suceed");

    // Get the last seen timestamp again.
    queryString = QString("SELECT date_time_last_seen FROM devices WHERE mac = '%1'").arg(mac);
    retval = query.exec(queryString);
    QVERIFY2(retval == true, "The query must suceed");
    QVERIFY2(query.size() == 1, "The query result should have only one row.");

    query.next();
    auto newDateTime = query.value(0).toDateTime();
    QVERIFY2(newDateTime.isValid(), "The obtained date and time must be valid");

    // Compare the dates.
    QVERIFY2(dateTime.secsTo(newDateTime) > 0, "The dates must be at least one second apart.");
}

void DBTests::check20AddLocation_data()
{
    const QString typeLocal = QStringLiteral("local");
    const QString typeExternal = QStringLiteral("external");
    const QString description = QStringLiteral("áéíóú_.,;:!$%{&/()=?¿¡%$ºª+[[}]ç");

    QVERIFY2(description.size() == 32, "A full description is 32 characters long at max.");

    QTest::addColumn<QString>("type");
    QTest::addColumn<QString>("description");
    QTest::addColumn<bool>("result");

    QTest::addRow("everything_ok_local") << typeLocal << description << true;
    QTest::addRow("everything_ok_external") << typeExternal << QStringLiteral("áéíóú_.,;:?$%{&/()=?¿¡%$ºª+[[}]ç") << true;
    QTest::addRow("wrong_type") << QStringLiteral("thisisnotatype") << QStringLiteral("áéíóú_.,;:?$%{&/()=?¿¡%$?ª+[[}]ç") << false;
    QTest::addRow("too_long_comment_33_chars") << typeLocal << QStringLiteral("123456789012345678901234567890123") << false;
    QTest::addRow("duplicated_description") << typeLocal << description << false;
}

void DBTests::check20AddLocation()
{
    QFETCH(QString, type);
    QFETCH(QString, description);
    QFETCH(bool, result);

    QString queryString;
    queryString = QString("SELECT add_location('%1', '%2')")
                         .arg(type)
                         .arg(description);

    QSqlQuery query;
    bool retval = query.exec(queryString);
    QVERIFY2(retval == result, "retval should be equal to the expected result");
}

void DBTests::check30AddPatient_data()
{
    const QString nameOK = QStringLiteral("Juan Martín");
    const QString surnameOK = QStringLiteral("González Güichal");
    const int32_t ageOK = 33;
    const QString genderM = QStringLiteral("m");
    const QString genderF = QStringLiteral("f");
    const QString genderO = QStringLiteral("o");
    const int64_t dniOK = 33333333;
    const QString commentOk = QStringLiteral("áéíóú ü .;:_!\"·$%&/\n()\n\r=?¿");

    QTest::addColumn<QString>("name");
    QTest::addColumn<QString>("surname");
    QTest::addColumn<int32_t>("age");
    QTest::addColumn<QString>("gender");
    QTest::addColumn<int64_t>("dni");
    QTest::addColumn<QString>("comment");
    QTest::addColumn<bool>("result");

    QTest::addRow("everything_ok_m") << nameOK << surnameOK << ageOK << genderM << dniOK << commentOk << true;
    QTest::addRow("everything_ok_f") << nameOK << surnameOK << ageOK << genderF << dniOK << commentOk << true;
    QTest::addRow("everything_ok_o") << nameOK << surnameOK << ageOK << genderO << dniOK << commentOk << true;
    QTest::addRow("bad_name") << QStringLiteral(",") << surnameOK << ageOK << genderM << dniOK << commentOk << false;
    QTest::addRow("bad_surname") << nameOK << QStringLiteral(":") << ageOK << genderM << dniOK << commentOk << false;
    QTest::addRow("bad_age") << nameOK << surnameOK << -1 << genderM << dniOK << commentOk << false;
    QTest::addRow("bad_gender") << nameOK << surnameOK << ageOK << QStringLiteral("b") << dniOK << commentOk << false;
    QTest::addRow("bad_dni") << nameOK << surnameOK << ageOK << genderM << static_cast<int64_t>(-1) << commentOk << false;
    // Is there room for a bad comment? Yes, too long, but is it something to test?
}

void DBTests::check30AddPatient()
{
    QFETCH(QString, name);
    QFETCH(QString, surname);
    QFETCH(int32_t, age);
    QFETCH(QString, gender);
    QFETCH(int64_t, dni);
    QFETCH(QString, comment);
    QFETCH(bool, result);

    QString queryString;
    queryString = QString("SELECT add_patient('%1', '%2', %3::SMALLINT, '%4', %5, '%6')")
                         .arg(name)
                         .arg(surname)
                         .arg(age)
                         .arg(gender)
                         .arg(dni)
                         .arg(comment);

    QSqlQuery query;
    bool retval = query.exec(queryString);

    QVERIFY2(retval == result, "retval should be equal to the expected result");
}

void DBTests::check40AddInternment_data()
{
    QTest::addColumn<QString>("timestamp");
    QTest::addColumn<int32_t>("patientId");
    QTest::addColumn<int32_t>("locationId");
    QTest::addColumn<QString>("mac");
    QTest::addColumn<bool>("isQueryExecutable");
    QTest::addColumn<bool>("queryAddsInternment");
    QTest::addColumn<int32_t>("returnValue");

    addDevicesPatientsAndLocations(ADD_INT_BASE_ID, ADD_INT_BASE_MAC, ADD_INT_NUM_ENTRIES);
    QTest::addRow("add_internment_ok") << currentDateTime()
                                       << ADD_INT_BASE_ID << ADD_INT_BASE_ID
                                       << QString("%1").arg(createMac(ADD_INT_BASE_MAC, 0))
                                       << true << true << 0;

    QTest::addRow("patient_not_found") << currentDateTime()
                                       << 107 << ADD_INT_BASE_ID+1
                                       << QString("%1").arg(createMac(ADD_INT_BASE_MAC, 0))
                                       << true << false << 1;

    QTest::addRow("location_not_found") << currentDateTime()
                                       << ADD_INT_BASE_ID+1 << 107
                                       << QString("%1").arg(createMac(ADD_INT_BASE_MAC, 0))
                                       << true << false << 2;

    QTest::addRow("device_not_found") << currentDateTime()
                                      << ADD_INT_BASE_ID+1 << ADD_INT_BASE_ID+1
                                      << QString("00:11:aa:33:44:55")
                                      << true << false << 3;

    QTest::addRow("open_internment_with_same_patient_available") << currentDateTime()
                                                                 << ADD_INT_BASE_ID << ADD_INT_BASE_ID+1
                                                                 << QString("%1").arg(createMac(ADD_INT_BASE_MAC, 1)) << true << false << 4;

    QTest::addRow("open_internment_with_same_location_available") << currentDateTime()
                                                                  << ADD_INT_BASE_ID+1 << ADD_INT_BASE_ID
                                                                  << QString("%1").arg(createMac(ADD_INT_BASE_MAC, 1)) << true << false << 5;

    QTest::addRow("open_internment_with_same_device_available") << currentDateTime()
                                                                << ADD_INT_BASE_ID+1 << ADD_INT_BASE_ID+1
                                                                << QString("%1").arg(createMac(ADD_INT_BASE_MAC, 0)) << true << false << 6;

    QTest::addRow("wrong_timestamp") << "foo"
                                     << ADD_INT_BASE_ID+1 << ADD_INT_BASE_ID+1
                                     << QString("%1").arg(createMac(ADD_INT_BASE_MAC, 1)) << false << false << 6;

    QTest::addRow("wrong_mac") << currentDateTime()
                               << ADD_INT_BASE_ID+1 << ADD_INT_BASE_ID+1
                               << QString("var") << false << false << 6;
}

void DBTests::check40AddInternment()
{
    QFETCH(QString, timestamp);
    QFETCH(int32_t, patientId);
    QFETCH(int32_t, locationId);
    QFETCH(QString, mac);
    QFETCH(bool, isQueryExecutable);
    QFETCH(bool, queryAddsInternment);
    QFETCH(int32_t, returnValue);

    QString queryString;
    queryString = QString("SELECT add_internment(cast('%1' as timestamptz), %2, %3, '%4')")
                         .arg(timestamp)
                         .arg(patientId)
                         .arg(locationId)
                         .arg(mac);

    QSqlQuery query;
    bool itExecuted = query.exec(queryString);
    QVERIFY2(itExecuted == isQueryExecutable, "retval should be equal to the expected result");

    if(!isQueryExecutable)
        return;

    query.first();
    bool ok;

    int32_t retval = query.value(0).toInt(&ok);
    QVERIFY2(ok, "Conversion to int must suceed");
    QVERIFY2(retval == returnValue, "The returned value must be the expected one.");

    if(!queryAddsInternment)
    {
        QVERIFY2(retval != 0, "The returned value should not be 0");
        return;
    }
}

void DBTests::check41AlarmsDefaultThresholds()
{
    bool ok;

    addDevicesPatientsAndLocations(ALARMS_SPO2_BASE_ID, ALARMS_SPO2_BASE_MAC, ALARMS_SPO2_NUM_ENTRIES);

    // Add the internment we will work with.
    QString queryString;
    queryString = QString("INSERT INTO "
                          "internments(internment_id, "
                          "date_time_begins, patient_id, location_id, mac) "
                          "values(%1, cast('%2' as timestamptz), %3, %4, '%5')")
                          .arg(ALARMS_SPO2_BASE_ID)
                          .arg(currentDateTime())
                          .arg(ALARMS_SPO2_BASE_ID)
                          .arg(ALARMS_SPO2_BASE_ID)
                          .arg(createMac(ALARMS_SPO2_BASE_MAC, 0));

    QSqlQuery query;
    QVERIFY2(query.exec(queryString), "It should be possible to add the internment.");

    // Create a default entry.
    queryString = QString("INSERT INTO alarms(modify_ts, modifying_mac, internment_id) "
                          "VALUES(cast('2020-07-27 00:11:22-03' as timestamp), "
                          "'%1', %2)").arg(createMac(ALARMS_SPO2_BASE_MAC, 0)).arg(ALARMS_SPO2_BASE_ID);

    bool retval = query.exec(queryString);
    QVERIFY2(retval, "Query for adding default alarm values must suceeed.");

    // Retrieve the entry.
    queryString = QString("SELECT "
                          "spo2_alarm_lt, spo2_alarm_delay_s, "
                          "hr_alarm_lt, hr_alarm_gt, hr_alarm_delay_s, "
                          "bt_alarm_lt, bt_alarm_gt, bt_alarm_delay_s, "
                          "bp_sys_alarm_lt, bp_sys_alarm_gt, bp_alarm_delay_s "
                          "FROM alarms where internment_id = %1").arg(ALARMS_SPO2_BASE_ID);

    retval = query.exec(queryString);
    QVERIFY2(retval, "Query for retrieving default alarm values must suceeed.");
    QVERIFY(query.next());

    // Check the default alarms thresholds.
    for(int i = 0; i < 11; i++)
    {
        int32_t value = query.value(i).toInt(&ok);
        int32_t expected;
        QVERIFY2(ok, "Value should be an integer.");

        switch(i) {
        case 0:
            expected = 950;
            break;

        case 1:
            expected = 15;
            break;

        case 2:
            expected = 600;
            break;

        case 3:
            expected = 1000;
            break;

        case 4:
            expected = 15;
            break;

        case 5:
            expected = 370;
            break;

        case 6:
            expected = 380;
            break;

        case 7:
            expected = 15;
            break;

        case 8:
            expected = 120;
            break;

        case 9:
            expected = 150;
            break;

        case 10:
            expected = 15;
            break;

        default:
            QFAIL("Unexpected case.");
        }

        QVERIFY2(value == expected, "The value must match the expected one.");
    }
}

void DBTests::check42AlarmsSpO2Thresholds_data()
{
    QTest::addColumn<QString>("alarmColumn");
    QTest::addColumn<spo2_t>("value");
    QTest::addColumn<bool>("retval");

    // SpO2 equal or less.
    const int32_t spo2_lt_bottom = Settings::spO2EqOrLessPercBot();
    const int32_t spo2_lt_top = Settings::spO2EqOrLessPercTop();
    const auto spO2ScaledBot = Scaling::spO2PercentageToWire(spo2_lt_bottom);
    const auto spO2ScaledTop = Scaling::spO2PercentageToWire(spo2_lt_top);

    QTest::newRow("spo2_alarm_lt_bottom-1") << "spo2_alarm_lt" << static_cast<spo2_t>(spO2ScaledBot - 1) << false;
    QTest::newRow("spo2_alarm_lt_bottom") << "spo2_alarm_lt" << spO2ScaledBot << true;
    QTest::newRow("spo2_alarm_lt_half") << "spo2_alarm_lt" << static_cast<spo2_t>((spO2ScaledTop + spO2ScaledBot) / 2) << true;
    QTest::newRow("spo2_alarm_lt_top") << "spo2_alarm_lt" << spO2ScaledTop << true;
    QTest::newRow("spo2_alarm_lt_top+1") << "spo2_alarm_lt" << static_cast<spo2_t>(spO2ScaledTop + 1) << false;
}

void DBTests::check42AlarmsSpO2Thresholds()
{
    QFETCH(QString, alarmColumn);
    QFETCH(spo2_t, value);
    QFETCH(bool, retval);

    QString queryString;
    queryString = QString("UPDATE alarms SET %1 = %2 WHERE internment_id = %3")
                         .arg(alarmColumn)
                         .arg(value)
                         .arg(ALARMS_SPO2_BASE_ID);

    QSqlQuery query;
    QVERIFY2(query.exec(queryString) == retval, "The query result should match what we expect.");
}

void DBTests::check43AlarmsHRThresholds_data()
{
    QTest::addColumn<QString>("alarmColumn");
    QTest::addColumn<heart_rate_t>("value");
    QTest::addColumn<bool>("retval");

    // Heart rate equal or less.
    const int32_t hr_alarm_lt_bottom = Settings::instance().heartRateEqOrLessBPMBot();
    const int32_t hr_alarm_lt_top = Settings::instance().heartRateEqOrLessBPMTop();
    const auto hrScaledLTBot = Scaling::heartRateBPMToWire(hr_alarm_lt_bottom);
    const auto hrScaledLTTop = Scaling::heartRateBPMToWire(hr_alarm_lt_top);

    QTest::newRow("hr_alarm_lt_bottom-1") << "hr_alarm_lt" << static_cast<heart_rate_t>(hrScaledLTBot - 1) << false;
    QTest::newRow("hr_alarm_lt_bottom") << "hr_alarm_lt" << hrScaledLTBot << true;
    QTest::newRow("hr_alarm_lt_half") << "hr_alarm_lt" << static_cast<heart_rate_t>((hrScaledLTTop + hrScaledLTBot) / 2) << true;
    QTest::newRow("hr_alarm_lt_top") << "hr_alarm_lt" << hrScaledLTTop << true;
    QTest::newRow("hr_alarm_lt_top+1") << "spo2_alarm_lt" << static_cast<heart_rate_t>(hrScaledLTTop + 1) << false;

    // Heart rate equal or greater.
    const int32_t hr_alarm_gt_bottom = Settings::instance().heartRateEqOrMoreBPMBot();
    const int32_t hr_alarm_gt_top = Settings::instance().heartRateEqOrMoreBPMTop();
    const auto hrScaledGTBot = Scaling::heartRateBPMToWire(hr_alarm_gt_bottom);
    const auto hrScaledGTTop = Scaling::heartRateBPMToWire(hr_alarm_gt_top);

    QTest::newRow("hr_alarm_gt_bottom-1") << "hr_alarm_gt" << static_cast<heart_rate_t>(hrScaledGTBot - 1) << false;
    QTest::newRow("hr_alarm_gt_bottom") << "hr_alarm_gt" << hrScaledGTBot << true;
    QTest::newRow("hr_alarm_gt_half") << "hr_alarm_gt" << static_cast<heart_rate_t>((hrScaledGTTop + hrScaledGTBot) / 2) << true;
    QTest::newRow("hr_alarm_gt_top") << "hr_alarm_gt" << hrScaledGTTop << true;
    QTest::newRow("hr_alarm_gt_top+1") << "spo2_alarm_lt" << static_cast<heart_rate_t>(hrScaledGTTop + 1) << false;
}

void DBTests::check43AlarmsHRThresholds()
{
    QFETCH(QString, alarmColumn);
    QFETCH(heart_rate_t, value);
    QFETCH(bool, retval);

    QString queryString;
    queryString = QString("UPDATE alarms SET %1 = %2 WHERE internment_id = %3")
                         .arg(alarmColumn)
                         .arg(value)
                         .arg(ALARMS_SPO2_BASE_ID);

    QSqlQuery query;
    QVERIFY2(query.exec(queryString) == retval, "The query result should match what we expect.");
}

void DBTests::check44AlarmsBTThresolhds_data()
{
    QTest::addColumn<QString>("alarmColumn");
    QTest::addColumn<body_temp_t>("value");
    QTest::addColumn<bool>("retval");

    // Body temperature equal or less.
    const int32_t bt_alarm_lt_bottom = Settings::instance().bodyTempEqOrLessCelsBot();
    const int32_t bt_alarm_lt_top = Settings::instance().bodyTempEqOrLessCelsTop();
    const auto btScaledLTBot = Scaling::bodyTemperatureDegCelsiusToWire(bt_alarm_lt_bottom);
    const auto btScaledLTTop = Scaling::bodyTemperatureDegCelsiusToWire(bt_alarm_lt_top);

    QTest::newRow("bt_alarm_lt_bottom-1") << "bt_alarm_lt" << static_cast<body_temp_t>(btScaledLTBot - 1) << false;
    QTest::newRow("bt_alarm_lt_bottom") << "bt_alarm_lt" << btScaledLTBot << true;
    QTest::newRow("bt_alarm_lt_half") << "bt_alarm_lt" << static_cast<body_temp_t>((btScaledLTTop + btScaledLTBot) / 2) << true;
    QTest::newRow("bt_alarm_lt_top") << "bt_alarm_lt" << btScaledLTTop << true;
    QTest::newRow("bt_alarm_lt_top+1") << "spo2_alarm_lt" << static_cast<body_temp_t>(btScaledLTTop + 1) << false;

    // Body temperature equal or greater.
    const int32_t bt_alarm_gt_bottom = Settings::instance().bodyTempEqOrMoreCelsBot();
    const int32_t bt_alarm_gt_top = Settings::instance().bodyTempEqOrMoreCelsBot();
    const auto btScaledGTBot = Scaling::bodyTemperatureDegCelsiusToWire(bt_alarm_gt_bottom);
    const auto btScaledGTTop = Scaling::bodyTemperatureDegCelsiusToWire(bt_alarm_gt_top);

    QTest::newRow("bt_alarm_gt_bottom-1") << "bt_alarm_gt" << static_cast<body_temp_t>(btScaledGTBot - 1) << false;
    QTest::newRow("bt_alarm_gt_bottom") << "bt_alarm_gt" << btScaledGTBot << true;
    QTest::newRow("bt_alarm_gt_half") << "bt_alarm_gt" << static_cast<body_temp_t>((btScaledGTTop + btScaledGTBot) / 2) << true;
    QTest::newRow("bt_alarm_gt_top") << "bt_alarm_gt" << btScaledGTTop << true;
    QTest::newRow("bt_alarm_gt_top+1") << "spo2_alarm_lt" << static_cast<body_temp_t>(btScaledGTTop + 1) << false;
}

void DBTests::check44AlarmsBTThresolhds()
{
    QFETCH(QString, alarmColumn);
    QFETCH(body_temp_t, value);
    QFETCH(bool, retval);

    QString queryString;
    queryString = QString("UPDATE alarms SET %1 = %2 WHERE internment_id = %3")
                         .arg(alarmColumn)
                         .arg(value)
                         .arg(ALARMS_SPO2_BASE_ID);

    QSqlQuery query;
    QVERIFY2(query.exec(queryString) == retval, "The query result should match what we expect.");
}

void DBTests::check45AlarmsBPThresholds_data()
{
    QTest::addColumn<QString>("alarmColumn");
    QTest::addColumn<blood_pressure_t>("value");
    QTest::addColumn<bool>("retval");

    // Body temperature equal or less.
    const int32_t bp_sys_alarm_lt_bottom = Settings::instance().bloodPressureSysEqOrLessBot();
    const int32_t bp_sys_alarm_lt_top = Settings::instance().bloodPressureSysEqOrLessTop();
    // No scaling needed, kept like this for documenting the fact.
    const blood_pressure_t btScaledLTBot = bp_sys_alarm_lt_bottom;
    const blood_pressure_t btScaledLTTop = bp_sys_alarm_lt_top;

    QTest::newRow("bp_sys_alarm_lt_bottom-1") << "bp_sys_alarm_lt" << static_cast<blood_pressure_t>(btScaledLTBot - 1) << false;
    QTest::newRow("bp_sys_alarm_lt_bottom") << "bp_sys_alarm_lt" << btScaledLTBot << true;
    QTest::newRow("bp_sys_alarm_lt_half") << "bp_sys_alarm_lt" << static_cast<blood_pressure_t>((btScaledLTTop + btScaledLTBot) / 2) << true;
    QTest::newRow("bp_sys_alarm_lt_top") << "bp_sys_alarm_lt" << btScaledLTTop << true;
    QTest::newRow("bp_sys_alarm_lt_top+1") << "spo2_alarm_lt" << static_cast<blood_pressure_t>(btScaledLTTop + 1) << false;

    // Body temperature equal or greater.
    const int32_t bp_sys_alarm_gt_bottom = Settings::instance().bloodPressureSysEqOrMoreBot();
    const int32_t bp_sys_alarm_gt_top = Settings::instance().bloodPressureSysEqOrMoreTop();
    // No scaling needed, kept like this for documenting the fact.
    const blood_pressure_t btScaledGTBot = bp_sys_alarm_gt_bottom;
    const blood_pressure_t btScaledGTTop = bp_sys_alarm_gt_top;

    QTest::newRow("bp_sys_alarm_gt_bottom-1") << "bp_sys_alarm_gt" << static_cast<blood_pressure_t>(btScaledGTBot - 1) << false;
    QTest::newRow("bp_sys_alarm_gt_bottom") << "bp_sys_alarm_gt" << btScaledGTBot << true;
    QTest::newRow("bp_sys_alarm_gt_half") << "bp_sys_alarm_gt" << static_cast<blood_pressure_t>((btScaledGTTop + btScaledGTBot) / 2) << true;
    QTest::newRow("bp_sys_alarm_gt_top") << "bp_sys_alarm_gt" << btScaledGTTop << true;
    QTest::newRow("bp_sys_alarm_gt_top+1") << "spo2_alarm_lt" << static_cast<blood_pressure_t>(btScaledGTTop + 1) << false;
}

void DBTests::check45AlarmsBPThresholds()
{
    QFETCH(QString, alarmColumn);
    QFETCH(blood_pressure_t, value);
    QFETCH(bool, retval);

    QString queryString;
    queryString = QString("UPDATE alarms SET %1 = %2 WHERE internment_id = %3")
                         .arg(alarmColumn)
                         .arg(value)
                         .arg(ALARMS_SPO2_BASE_ID);

    QSqlQuery query;
    QVERIFY2(query.exec(queryString) == retval, "The query result should match what we expect.");
}

void DBTests::check46AlarmsDelays_data()
{
    QTest::addColumn<QString>("alarmColumn");
    QTest::addColumn<uint8_t>("value");
    QTest::addColumn<bool>("retval");

    const auto spO2DelayMin = Settings::spO2DelaySMin();
    const auto spO2DelayMax = Settings::spO2DelaySMax();
    const auto hrDelayMin = Settings::heartRateDelaySMin();
    const auto hrDelayMax = Settings::heartRateDelaySMax();
    const auto btDelayMin = Settings::bodyTempDelaySMin();
    const auto btDelayMax = Settings::bodyTempDelaySMax();
    const auto bpDelayMin = Settings::bloodPressureDelaySMin();
    const auto bpDelayMax = Settings::bloodPressureDelaySMax();

    QTest::newRow("spo2_alarm_delay_min-1") << "spo2_alarm_delay_s" << static_cast<uint8_t>(spO2DelayMin - 1) << false;
    QTest::newRow("spo2_alarm_delay_min") << "spo2_alarm_delay_s" << spO2DelayMin << true;
    QTest::newRow("spo2_alarm_delay_max") << "spo2_alarm_delay_s" << spO2DelayMax << true;
    QTest::newRow("spo2_alarm_delay_max+1") << "spo2_alarm_delay_s" << static_cast<uint8_t>(spO2DelayMax + 1) << false;

    QTest::newRow("hr_alarm_delay_min-1") << "hr_alarm_delay_s" << static_cast<uint8_t>(hrDelayMin - 1) << false;
    QTest::newRow("hr_alarm_delay_min") << "hr_alarm_delay_s" << hrDelayMin << true;
    QTest::newRow("hr_alarm_delay_max") << "hr_alarm_delay_s" << hrDelayMax << true;
    QTest::newRow("hr_alarm_delay_max+1") << "hr_alarm_delay_s" << static_cast<uint8_t>(hrDelayMax + 1) << false;

    QTest::newRow("bt_alarm_delay_min-1") << "bt_alarm_delay_s" << static_cast<uint8_t>(btDelayMin - 1) << false;
    QTest::newRow("bt_alarm_delay_min") << "bt_alarm_delay_s" << btDelayMin << true;
    QTest::newRow("bt_alarm_delay_max") << "bt_alarm_delay_s" << btDelayMax << true;
    QTest::newRow("bt_alarm_delay_max+1") << "bt_alarm_delay_s" << static_cast<uint8_t>(btDelayMax + 1) << false;

    QTest::newRow("bp_alarm_delay_min-1") << "bp_alarm_delay_s" << static_cast<uint8_t>(bpDelayMin - 1) << false;
    QTest::newRow("bp_alarm_delay_min") << "bp_alarm_delay_s" << bpDelayMin << true;
    QTest::newRow("bp_alarm_delay_max") << "bp_alarm_delay_s" << bpDelayMax << true;
    QTest::newRow("bp_alarm_delay_max+1") << "bp_alarm_delay_s" << static_cast<uint8_t>(bpDelayMax + 1) << false;
}

void DBTests::check46AlarmsDelays()
{
    QFETCH(QString, alarmColumn);
    QFETCH(uint8_t, value);
    QFETCH(bool, retval);

    QString queryString;
    queryString = QString("UPDATE alarms SET %1 = %2 WHERE internment_id = %3")
                         .arg(alarmColumn)
                         .arg(value)
                         .arg(ALARMS_SPO2_BASE_ID);

    QSqlQuery query;
    QVERIFY2(query.exec(queryString) == retval, "The query result should match what we expect.");
}

void DBTests::check47UpdateAlarmsSP()
{
    bool ok;

    /*
     * An entry was created in check41AlarmsDefaultThresholds(), let's reuse it.
     * First get modify_ts from that entry.
     */

    QString queryString;
    queryString = QString("SELECT modify_ts FROM alarms "
                          "WHERE internment_id = %1 "
                          "ORDER BY modify_ts ASC LIMIT 1").arg(ALARMS_SPO2_BASE_ID);

    QSqlQuery query;
    bool retval = query.exec(queryString);
    QVERIFY2(retval, "Could not get modify_ts");
    QVERIFY(query.size() == 1);
    QVERIFY(query.next());
    QString lastUpdate = query.value(0).toString();

    // Verify we can update the row with valid values.
    queryString = QString("SELECT update_alarms('%1', cast('%2' as timestamptz), %3,"
                          "950, 16::SMALLINT, "
                          "%4, %5, 18::SMALLINT, "
                          "%6, %7, 32::SMALLINT, "
                          "%8, %9, 60::SMALLINT)")
            .arg(createMac(ALARMS_SPO2_BASE_MAC, 0)).arg(lastUpdate).arg(ALARMS_SPO2_BASE_ID)
            .arg(Settings::heartRateEqOrLessWireBot()).arg(Settings::heartRateEqOrMoreWireBot())
            .arg(Settings::bodyTempEqOrLessWireBot()).arg(Settings::bodyTempEqOrMoreWireBot())
            .arg(Settings::bloodPressureSysEqOrLessBot()).arg(Settings::bloodPressureSysEqOrMoreBot());

    retval = query.exec(queryString);
    if(!retval)
        qDebug() << query.lastError();
    QVERIFY2(retval, "Could not update alarm.");
    QVERIFY(query.size() == 1);
    QVERIFY(query.next());
    QVERIFY(query.value(0).toInt(&ok) == 0);
    QVERIFY(ok);

    // Repeating the query should fail as the last modify_ts has changed.
    retval = query.exec(queryString);
    QVERIFY(retval);
    QVERIFY(query.next());
    QVERIFY(query.value(0).toInt(&ok) == 2);
    QVERIFY(ok);
}

void DBTests::check50InternmentsTrigger()
{
    const QString internmentsChangedStr = Engine::internmentsChangedNotificationString();
    QVERIFY2(internmentsChangedStr == QStringLiteral("internments_changed"), "Trigger string must be the expected one.");

    auto driver = QSqlDatabase::database().driver();
    driver->subscribeToNotification(internmentsChangedStr);

    QSignalSpy spy(driver, SIGNAL(notification(const QString &)));

    QString queryString;
    queryString = QString("UPDATE internments SET date_time_begins = cast('2020-07-01 00:05:12+03:00' as timestamptz) WHERE internment_id = %1")
                         .arg(ALARMS_SPO2_BASE_ID);
    QSqlQuery query;
    QVERIFY2(query.exec(queryString), "Query must suceed.");

    spy.wait();
    QVERIFY2(spy.count() == 1 , "There must be exactly one signal.");
    QList<QVariant> arguments = spy.takeFirst();

    QVERIFY(arguments.at(0).toString() == internmentsChangedStr);
}

/**
 * @brief DBTests::check60SpO2Ranges_data
 * spo2_t is replaced by int32_t in order to check boundaries.
 */
void DBTests::check60SpO2Ranges_data()
{
    const time_t VALID_TIME = 1;
    const int32_t VALID_SPO2 = (WIRE_SPO2_MAX + WIRE_SPO2_MIN) / 2;
    addDevicesPatientsAndLocations(VALUES_BASE_ID, VALUES_BASE_MAC, VALUES_NUM_ENTRIES);

    // Add the internment we will work with.
    QString queryString;
    queryString = QString("INSERT INTO "
                          "internments(internment_id, "
                          "date_time_begins, patient_id, location_id, mac) "
                          "values(%1, cast('%2' as timestamptz), %3, %4, '%5')")
                          .arg(VALUES_BASE_ID)
                          .arg(currentDateTime())
                          .arg(VALUES_BASE_ID)
                          .arg(VALUES_BASE_ID)
                          .arg(createMac(VALUES_BASE_MAC, 0));

    QSqlQuery query;
    QVERIFY2(query.exec(queryString), "It should be possible to add the internment.");

    QTest::addColumn<time_t>("time");
    QTest::addColumn<int32_t>("spo2");
    QTest::addColumn<bool>("expectedRetval");

    QTest::addRow("invalid_time") << static_cast<time_t>(-1) << VALID_SPO2 << false;
    QTest::addRow("valid_entry") << VALID_TIME << VALID_SPO2 << true;
    QTest::addRow("spo2_min-1") << VALID_TIME + 1 << WIRE_SPO2_MIN - 1 << false;
    QTest::addRow("spo2_min") << VALID_TIME + 2 << static_cast<int32_t>(WIRE_SPO2_MIN) << true;
    QTest::addRow("spo2_max") << VALID_TIME + 3 << static_cast<int32_t>(WIRE_SPO2_MAX) << true;
    QTest::addRow("spo2_max+1") << VALID_TIME + 4 << WIRE_SPO2_MAX + 1 << false;
}

void DBTests::check60SpO2Ranges()
{
    QFETCH(time_t, time);
    QFETCH(int32_t, spo2);
    QFETCH(bool, expectedRetval);

    QString queryString;
    queryString = QString("INSERT INTO spo2_values(time, spo2, internment_id)"
                          "values(%1, %2, %3)")
                         .arg(time)
                         .arg(spo2)
                         .arg(VALUES_BASE_ID);

    QSqlQuery query;
    QVERIFY2(query.exec(queryString) == expectedRetval, "The query result should match what we expect.");
}

void DBTests::check70BPRanges_data()
{
    const time_t VALID_TIME = 100;
    const int32_t VALID_SYS = (WIRE_BLOOD_PRESSURE_SYS_MAX + WIRE_BLOOD_PRESSURE_SYS_MIN) / 2;
    const int32_t VALID_DIAS = (WIRE_BLOOD_PRESSURE_DIAS_MAX + WIRE_BLOOD_PRESSURE_DIAS_MIN) / 2;

    QTest::addColumn<time_t>("time");
    QTest::addColumn<int32_t>("sys");
    QTest::addColumn<int32_t>("dias");
    QTest::addColumn<bool>("expectedRetval");

    QTest::addRow("invalid_time") << static_cast<time_t>(-1) << VALID_SYS << VALID_DIAS << false;
    QTest::addRow("valid_entry") << VALID_TIME << VALID_SYS << VALID_DIAS << true;
    QTest::addRow("bp_sys_min-1") << VALID_TIME + 1 << WIRE_BLOOD_PRESSURE_SYS_MIN - 1 << VALID_DIAS << false;
    QTest::addRow("bp_sys_min") << VALID_TIME + 2 << static_cast<int32_t>(WIRE_BLOOD_PRESSURE_SYS_MIN) << VALID_DIAS << true;
    QTest::addRow("bp_sys_max") << VALID_TIME + 3 << static_cast<int32_t>(WIRE_BLOOD_PRESSURE_SYS_MAX) << VALID_DIAS << true;
    QTest::addRow("bp_sys_max+1") << VALID_TIME + 4 << WIRE_BLOOD_PRESSURE_SYS_MAX + 1 << VALID_DIAS << false;
}

void DBTests::check70BPRanges()
{
    QFETCH(time_t, time);
    QFETCH(int32_t, sys);
    QFETCH(int32_t, dias);
    QFETCH(bool, expectedRetval);

    QString queryString;
    queryString = QString("INSERT INTO blood_pressure_values(time, systolic, diastolic, internment_id)"
                          "values(%1, %2, %3, %4)")
                         .arg(time)
                         .arg(sys)
                         .arg(dias)
                         .arg(VALUES_BASE_ID);

    QSqlQuery query;
    QVERIFY2(query.exec(queryString) == expectedRetval, "The query result should match what we expect.");
}

void DBTests::check80HRRanges_data()
{
    const time_t VALID_TIME = 200;
    const int32_t VALID_HR = (WIRE_HEART_RATE_MAX + WIRE_HEART_RATE_MIN) / 2;

    QTest::addColumn<time_t>("time");
    QTest::addColumn<int32_t>("hr");
    QTest::addColumn<bool>("expectedRetval");

    QTest::addRow("invalid_time") << static_cast<time_t>(-1) << VALID_HR << false;
    QTest::addRow("valid_entry") << VALID_TIME << VALID_HR << true;
    QTest::addRow("hr_min-1") << VALID_TIME + 1 << WIRE_HEART_RATE_MIN - 1 << false;
    QTest::addRow("hr_min") << VALID_TIME + 2 << static_cast<int32_t>(WIRE_HEART_RATE_MIN) << true;
    QTest::addRow("hr_max") << VALID_TIME + 3 << static_cast<int32_t>(WIRE_HEART_RATE_MAX) << true;
    QTest::addRow("hr_max+1") << VALID_TIME + 4 << WIRE_HEART_RATE_MAX + 1 << false;
}

void DBTests::check80HRRanges()
{
    QFETCH(time_t, time);
    QFETCH(int32_t, hr);
    QFETCH(bool, expectedRetval);

    QString queryString;
    queryString = QString("INSERT INTO heart_rate_values(time, heart_rate, internment_id)"
                          "values(%1, %2, %3)")
                         .arg(time)
                         .arg(hr)
                         .arg(VALUES_BASE_ID);

    QSqlQuery query;
    QVERIFY2(query.exec(queryString) == expectedRetval, "The query result should match what we expect.");
}

void DBTests::check90BTRanges_data()
{
    const time_t VALID_TIME = 300;
    const int32_t VALID_BT = (WIRE_BODY_TEMP_MAX + WIRE_BODY_TEMP_MIN) / 2;

    QTest::addColumn<time_t>("time");
    QTest::addColumn<int32_t>("bt");
    QTest::addColumn<bool>("expectedRetval");

    QTest::addRow("invalid_time") << static_cast<time_t>(-1) << VALID_BT << false;
    QTest::addRow("valid_entry") << VALID_TIME << VALID_BT << true;
    QTest::addRow("bt_min-1") << VALID_TIME + 1 << WIRE_BODY_TEMP_MIN - 1 << false;
    QTest::addRow("bt_min") << VALID_TIME + 2 << static_cast<int32_t>(WIRE_BODY_TEMP_MIN) << true;
    QTest::addRow("bt_max") << VALID_TIME + 3 << static_cast<int32_t>(WIRE_BODY_TEMP_MAX) << true;
    QTest::addRow("bt_max+1") << VALID_TIME + 4 << WIRE_BODY_TEMP_MAX + 1 << false;
}

void DBTests::check90BTRanges()
{
    QFETCH(time_t, time);
    QFETCH(int32_t, bt);
    QFETCH(bool, expectedRetval);

    QString queryString;
    queryString = QString("INSERT INTO body_temperature_values(time, temperature, internment_id)"
                          "values(%1, %2, %3)")
                         .arg(time)
                         .arg(bt)
                         .arg(VALUES_BASE_ID);

    QSqlQuery query;
    QVERIFY2(query.exec(queryString) == expectedRetval, "The query result should match what we expect.");
}

bool DBTests::queriesFromFile(const QString &filePath)
{
    QString thePath = filePath;
    QFile file(filePath);
    QTemporaryFile * tmpFile = QTemporaryFile::createNativeFile(file);
    if(tmpFile != nullptr)
    {
         if(!tmpFile->isReadable())
             return false;

         thePath = tmpFile->fileName();
    }

    auto db = QSqlDatabase::database();

    QStringList args;
    args << "-h" << db.hostName();
    args << "-d" << db.databaseName();
    args << "-U" << db.userName();
    args << "-w";
    args << "-f" << thePath;

    QProcess psql;
    psql.start("psql", args);
    psql.waitForFinished();

    QTextStream out(stdout);
    out << psql.readAllStandardOutput();
    out << psql.readAllStandardError();

    if(psql.exitStatus() != QProcess::NormalExit)
    {
        qDebug() << "psql process ended with error: " << psql.errorString();
        return false;
    }

    return true;
}

void DBTests::addDevicesPatientsAndLocations(const int32_t baseID, const QString &baseMac, const int32_t number)
{
    /*
     * First let's add 3 devices, 3 patients, 3 locations and 3 internments,
     * this time specifying the IDs ourselves.
     */
    for(int i = 0; i < number; i++)
    {
        // Devices.
        QString queryString;
        queryString = QString("INSERT INTO "
                              "devices(mac, date_time_added, date_time_last_seen,battmv,timems,msgid) "
                              "values('%1', "
                              "cast('%2' as timestamp), cast('%3' as timestamp), "
                              "%4, %5, %6)")
                              .arg(createMac(baseMac, i))
                              .arg(currentDateTime())
                              .arg(currentDateTime())
                              .arg(i+1*1000)
                              .arg(i+1*458)
                              .arg(i);

        QSqlQuery query;
        QVERIFY2(query.exec(queryString), "Query to add device should not fail");

        // Locations.
        QString locType;
        if(i%2 == 0)
            locType = QStringLiteral("local");
        else
            locType = QStringLiteral("external");

        queryString = QString("INSERT INTO locations(location_id, type, description) "
                              "values(%1, '%2', 'Test %3')")
                             .arg(baseID + i)
                             .arg(locType)
                             .arg(baseID + i);
        QVERIFY2(query.exec(queryString), "Query to add location should not fail");

        // Patients.
        QString gender;
        switch(i%3) {
        case 0:
            gender = QStringLiteral("m");
            break;

        case 1:
            gender = QStringLiteral("f");
            break;

        case 2:
            gender = QStringLiteral("o");
            break;
        }

        queryString = QString("INSERT INTO "
                              "patients(patient_id, date_time_added, name, surname, "
                              "age, gender, dni, comments) "
                              "values(%1, cast('%2' as timestamptz),'Name %3', 'Surname %4', %5::SMALLINT, '%6', %7, 'A comment')")
                             .arg(i+baseID)
                             .arg(currentDateTime())
                             .arg(i)
                             .arg(i)
                             .arg(3*i)
                             .arg(gender)
                             .arg(i*12456895);

        QVERIFY2(query.exec(queryString), "Query to add patient should not fail");
    }
}

QString DBTests::currentDateTime()
{
    return QDateTime::currentDateTime().toString(Qt::ISODate);
}

QString DBTests::createMac(const QString &base, int32_t lastOctet)
{
    lastOctet = lastOctet % 0x100;
    return QString("%1%2").arg(base).arg(lastOctet, 2, 16, QChar('0'));
}

QTEST_MAIN(DBTests)
#include "db_tst.moc"
