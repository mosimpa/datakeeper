#ifndef DB_TESTS_H
#define DB_TESTS_H

#include <QObject>
#include <QtTest/QTest>
#include <QFile>
#include <QString>

/// \todo Add tests for each values' mins and max as defined in mosimpaqt::definitions.
class DBTests : public QObject
{
    Q_OBJECT

private slots:
    void initTestCase();
    void cleanupTestCase();
    void check00DBCreation();
    void check10AddDeviceSP_data();
    void check10AddDeviceSP();
    void check20AddLocation_data();
    void check20AddLocation();
    void check30AddPatient_data();
    void check30AddPatient();
    void check40AddInternment_data();
    void check40AddInternment();
    void check41AlarmsDefaultThresholds();
    void check42AlarmsSpO2Thresholds_data();
    void check42AlarmsSpO2Thresholds();
    void check43AlarmsHRThresholds_data();
    void check43AlarmsHRThresholds();
    void check44AlarmsBTThresolhds_data();
    void check44AlarmsBTThresolhds();
    void check45AlarmsBPThresholds_data();
    void check45AlarmsBPThresholds();
    void check46AlarmsDelays_data();
    void check46AlarmsDelays();
    void check47UpdateAlarmsSP();
    void check50InternmentsTrigger();

    void check60SpO2Ranges_data();
    void check60SpO2Ranges();
    void check70BPRanges_data();
    void check70BPRanges();
    void check80HRRanges_data();
    void check80HRRanges();
    void check90BTRanges_data();
    void check90BTRanges();

private:
    bool queriesFromFile(const QString & filePath);
    void addDevicesPatientsAndLocations(const int32_t baseID, const QString & baseMac, const int32_t number);
    QString currentDateTime();
    QString createMac(const QString & base, int32_t lastOctet);
    const QString ADD_INT_BASE_MAC = QString("11:22:33:ff:55:");
    const int32_t ADD_INT_BASE_ID = 100;
    const int32_t ADD_INT_NUM_ENTRIES = 3;

    const QString ALARMS_SPO2_BASE_MAC = QString("22:33:44:55:66:");
    const int32_t ALARMS_SPO2_BASE_ID = 200;
    const int32_t ALARMS_SPO2_NUM_ENTRIES = 1;

    const QString VALUES_BASE_MAC = QString("55:44:33:22:11:");
    const int32_t VALUES_BASE_ID = 300;
    const int32_t VALUES_NUM_ENTRIES = 1;
};

#endif // DB_TESTS_H
