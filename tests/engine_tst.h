#ifndef ENGINE_TST_H
#define ENGINE_TST_H

#include <QObject>
#include <QtTest/QTest>
#include <QFile>
#include <QString>

/// \todo Add tests for each values' mins and max as defined in mosimpaqt::definitions.
class EngineTests : public QObject
{
    Q_OBJECT

private slots:
    void initTestCase();
    void cleanupTestCase();
    void check00DBCreation();
    void check10QueryCommandInternments();
    void check20UpdateAlarms();

private:
    bool queriesFromFile(const QString & filePath);
    void addDevicesPatientsLocationsAndInternments(const int32_t baseID, const QString & baseMac, const int32_t number);
    QString currentDateTime();
    QString createMac(const QString & base, int32_t lastOctet);
    const QString BASE_MAC = QString("11:22:33:ff:55:");
    const int32_t BASE_ID = 100;
    const int32_t NUM_ENTRIES = 3;
    const QString LAST_UPDATE_TS = "2020-07-29 15:39:31-03";
};

#endif // ENGINE_TST_H
