#include <QMetaType>
#include <QString>
#include <QTest>
#include <QVector>

#include <mosquitto.h>

#include <mosimpaqt/definitions.h>
#include <mosimpaqt/scaling.h>

#include "../src/devicesinfoparser.h"
#include "../src/settings.h"

#include "mosquittomessage.h"
#include "devicesinfoparser_tst.h"

void DevicesInfoParserTests::checkTopic()
{
    QVERIFY2(DevicesInfoParser::topic() == QStringLiteral("devices/info"), "Topic must match.");
}

void DevicesInfoParserTests::checkJson_data()
{
    DevicesInfoParser::values values;
    values.mac = QStringLiteral("001122334455");
    values.batterymV = 3000;
    values.time = 2;
    values.msgId = 5;
    QString payload;

    QTest::addColumn<MosquittoMessage*>("msg");
    QTest::addColumn<bool>("expectedRetval");
    QTest::addColumn<DevicesInfoParser::values>("expectedValues");

    QTest::addRow("invalid_json") << newMosquittoMessage("foo") << false << values;
    QTest::addRow("valid_json_invalid_data") << newMosquittoMessage("{\"foo\":\"var\"}")
                                             << false << values;

    payload = QString("{\"WiFiMAC\":\"%1\"}").arg(values.mac);
    QTest::addRow("only_mac") << newMosquittoMessage(payload) << false << values;

    payload = QString("{\"WiFiMAC\":\"%1\",\"time\":%2}").arg(values.mac).arg(values.time);
    QTest::addRow("mac_and_time") << newMosquittoMessage(payload) << false << values;

    payload = QString("{\"WiFiMAC\":\"%1\",\"time\":%2,\"msgId\":%3}")
                     .arg(values.mac).arg(values.time).arg(values.msgId);
    QTest::addRow("mac_time_and_msgId") << newMosquittoMessage(payload) << false << values;

    payload = QString("{\"WiFiMAC\":\"%1\",\"time\":%2,\"msgId\":%3,\"battmV\":%4}")
            .arg(values.mac).arg(values.time).arg(values.msgId).arg(values.batterymV);
    QTest::addRow("valid_payload") << newMosquittoMessage(payload) << true << values;

}

void DevicesInfoParserTests::checkJson()
{
    QFETCH(MosquittoMessage*, msg);
    QFETCH(bool, expectedRetval);
    QFETCH(DevicesInfoParser::values, expectedValues);
    DevicesInfoParser::values values;

    QVERIFY2(msg != nullptr, "Message should not be a null pointer");

    auto retval = DevicesInfoParser::parseMessage(&(msg->msg), values);

    QVERIFY2(retval == expectedRetval, "retval should match expectedRetval");

    if(retval == true)
    {
        QVERIFY2(values.mac == expectedValues.mac, "MAC must match.");
        QVERIFY2(values.batterymV == expectedValues.batterymV, "Battery mV must match.");
        QVERIFY2(values.time == expectedValues.time, "Time must match.");
        QVERIFY2(values.msgId == expectedValues.msgId, "Message ID must match.");
    }
}

void DevicesInfoParserTests::checkRanges_data()
{
    QTest::addColumn<MosquittoMessage*>("msg");
    QTest::addColumn<bool>("expectedRetval");
    QTest::addColumn<DevicesInfoParser::values>("expectedValues");

    DevicesInfoParser::values values;
    values.mac = QStringLiteral("foo");
    values.batterymV = 3000;
    values.time = 2;
    values.msgId = 5;

    QString payload;

    // MAC.
    payload = QString("{\"WiFiMAC\":\"%1\",\"time\":%2,\"msgId\":%3,\"battmV\":%4}")
            .arg(values.mac).arg(values.time).arg(values.msgId).arg(values.batterymV);

    QTest::addRow("wrong_mac") << newMosquittoMessage(payload) << false << values;

    values.mac = QStringLiteral("00:11:22:33:44:55");
    payload = QString("{\"WiFiMAC\":\"%1\",\"time\":%2,\"msgId\":%3,\"battmV\":%4}")
            .arg(values.mac).arg(values.time).arg(values.msgId).arg(values.batterymV);
    QTest::addRow("wrong_mac_2") << newMosquittoMessage(payload) << false << values;

    // Battery mV.
    values.mac = QStringLiteral("001122334455");
    payload = QString("{\"WiFiMAC\":\"%1\",\"time\":%2,\"msgId\":%3,\"battmV\":%4}")
               .arg(values.mac).arg(values.time).arg(values.msgId).arg(-1);
    QTest::addRow("negative_battmV") << newMosquittoMessage(payload) << false << values;

    payload = QString("{\"WiFiMAC\":\"%1\",\"time\":%2,\"msgId\":%3,\"battmV\":%4}")
               .arg(values.mac).arg(values.time).arg(values.msgId).arg(std::numeric_limits<batt_mv_t>::max() + 1);
    QTest::addRow("battmV_overflow") << newMosquittoMessage(payload) << false << values;

    // Time.
    values.time = -1;
    payload = QString("{\"WiFiMAC\":\"%1\",\"time\":%2,\"msgId\":%3,\"battmV\":%4}")
               .arg(values.mac).arg(values.time).arg(values.msgId).arg(values.batterymV);
    QTest::addRow("negative_time") << newMosquittoMessage(payload) << false << values;

    /*
     * time is time_t which is int64_t and the ESP32 has int32_t so we can skip
     * time overflow.
     */

    // Message ID.
    values.time = 2;
    payload = QString("{\"WiFiMAC\":\"%1\",\"time\":%2,\"msgId\":%3,\"battmV\":%4}")
               .arg(values.mac).arg(values.time).arg(-1).arg(values.batterymV);
    QTest::addRow("negative_msgId") << newMosquittoMessage(payload) << false << values;

    payload = QString("{\"WiFiMAC\":\"%1\",\"time\":%2,\"msgId\":%3,\"battmV\":%4}")
               .arg(values.mac).arg(values.time).arg(static_cast<int64_t>(std::numeric_limits<msg_id_t>::max()) + 1).arg(values.batterymV);
    QTest::addRow("msgId_overflow") << newMosquittoMessage(payload) << false << values;
}

void DevicesInfoParserTests::checkRanges()
{
    QFETCH(MosquittoMessage*, msg);
    QFETCH(bool, expectedRetval);
    QFETCH(DevicesInfoParser::values, expectedValues);
    DevicesInfoParser::values values;

    QVERIFY2(msg != nullptr, "Message should not be a null pointer");

    auto retval = DevicesInfoParser::parseMessage(&(msg->msg), values);

    QVERIFY2(retval == expectedRetval, "retval should match expectedRetval");

    if(retval == true)
    {
        QVERIFY2(values.mac == expectedValues.mac, "MAC must match.");
        QVERIFY2(values.batterymV == expectedValues.batterymV, "Battery mV must match.");
        QVERIFY2(values.time == expectedValues.time, "Time must match.");
        QVERIFY2(values.msgId == expectedValues.msgId, "Message ID must match.");
    }
}

MosquittoMessage *DevicesInfoParserTests::newMosquittoMessage(const QString &payload)
{
    return new MosquittoMessage(DevicesInfoParser::topic(), payload, this);
}

QTEST_MAIN(DevicesInfoParserTests)
#include "devicesinfoparser_tst.moc"
