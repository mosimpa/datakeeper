/* User */
CREATE ROLE datakeeper WITH LOGIN PASSWORD 'youshouldreallychangeme';

/* Data base */
-- Connection and temporal tables creation
GRANT CONNECT, TEMPORARY ON DATABASE "mosimpa-datakeeper" TO datakeeper;

/* Schemes */
-- Scheme access.
GRANT USAGE ON SCHEMA public to datakeeper;

/* Tables */
-- Select and insert on all the scheme's tables.
GRANT SELECT, INSERT ON ALL TABLES IN SCHEMA public TO datakeeper;

/* Sequencies */
-- Allow to use them.
GRANT USAGE, SELECT ON ALL SEQUENCES IN SCHEMA public TO datakeeper;
