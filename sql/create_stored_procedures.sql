-- Remember to add languaje plpgsql to the database before using this procedures.

-- Add a device
/**
 * \brief Adds a device to the device table.
 *
 * \returns 0 if the device is already present, 1 if it was added.
 *
 */
 create or replace function add_or_update_device(newMac macaddr, newBattmV integer, newTimems bigint, newMsgId bigint)
 returns integer
 as $$
 declare
   found_device devices%rowtype;
   currentTime constant devices.date_time_added%type := clock_timestamp();
begin
  -- Check if the MAC address is already present.
  SELECT INTO found_device * FROM devices WHERE mac=newMac;
  if not found
  then
    INSERT INTO devices(mac, date_time_added, date_time_last_seen, battmV, timems, msgId)
                values(newMac, currentTime, currentTime, newBattmV, newTimems, newMsgId);
    return 1;
  end if;

  -- Entry found, update it.
  UPDATE devices SET date_time_last_seen = currentTime, battmV = newBattmV,
                     timems = newTimems, msgId = newMsgId
                 WHERE mac = newMac;

  return 0;
end;
$$
language plpgsql;

/**
 * \brief Updates a device's timestamp.
 * \returns 1 is the device was found and updated, 0 otherwise.
 */
 create or replace function update_device_last_seen_timestamp(theMac macaddr)
 returns integer
 as $$
 declare
   found_device devices%rowtype;
   currentTime constant devices.date_time_added%type := clock_timestamp();
begin
  -- Check if the MAC address is already present.
  SELECT INTO found_device * FROM devices WHERE mac = theMac;
  if found
  then
    UPDATE devices SET date_time_last_seen = currentTime WHERE mac = theMac;
    return 1;
  end if;

  return 0;
end;
$$
language plpgsql;

-- Add a patient
/**
 * \brief Adds a patient to the patients table.
 * \returns 0
 */
create or replace function add_patient(newName varchar(64), newSurname varchar(64),
                                       newAge smallint, newGender char,
                                       newDni integer, newComments varchar(128))
returns integer
as $$
declare
  currentTime constant devices.date_time_added%type := clock_timestamp();
begin
  INSERT INTO patients(date_time_added, name, surname, age, gender, dni, comments)
         VALUES(currentTime, newName, newSurname, newAge, newGender, newDni, newComments);
  return 0;
end;
$$
language plpgsql;

-- Add a location
/**
 * \brief Adds a location to the locations table.
 * \returns 0
 */
create or replace function add_location(newType location_type, newDescription varchar(32))
returns integer
as $$
begin
  INSERT INTO locations(type, description) VALUES(newType, newDescription);
  return 0;
end;
$$
language plpgsql;

-- Add an internment
/**
 * \brief Adds an internment and the first alarms entry.
 * \returns
 * - 0 is the internment could be added.
 * - 1 if the patient could not be found.
 * - 2 if the location could not be found.
 * - 3 if the device could not be found.
 * - 4 if there is already an open internment with the current patient_id.
 * - 5 if there is already an open internment with the current location_id.
 * - 6 if there is already an open internment with the current mac.
 */
create or replace function add_internment(begins timestamptz, patient integer, location integer, macAddr macaddr)
returns integer
as $$
declare
  found_patient patients%rowtype;
  found_location locations%rowtype;
  found_device devices%rowtype;
  found_internment internments%rowtype;
  -- Limit the timestamp to just 3 decimals of milliseconds. This helps with handling date/time with Qt.
  currentTime constant alarms.modify_ts%type := current_timestamp(3);
begin
  -- Check if the patient is present.
  SELECT INTO found_patient * FROM patients WHERE patient_id = patient;
  if not found then
    raise notice 'Patient not found!';
    return 1;
  end if;

  -- Check if the location is present.
  SELECT INTO found_location * FROM locations WHERE location_id = location;
  if not found then
    raise notice 'Location not found!';
    return 2;
  end if;

  -- Check if there is a device with that MAC present.
  SELECT INTO found_device * FROM devices WHERE mac = macAddr;
  if not found then
    raise notice 'Device not found!';
    return 3;
  end if;

  -- Only if there are internments available...
  SELECT INTO found_internment * FROM internments;
  if found then

    -- Check if there are internments with the current patient on them.
    SELECT INTO found_internment * FROM internments WHERE patient_id = patient;
    if found then
      -- Check if there is any internment in which the same patient is present and date_time_ends is NULL.
      SELECT INTO found_internment * FROM internments WHERE patient_id = patient AND date_time_begins IS NOT NULL AND date_time_ends IS NOT NULL;
      if not found then
        raise notice 'Found open internment with current patient!';
        return 4;
      end if;
    end if;

    -- Check if there are internments with the current location on them.
    SELECT into found_internment * FROM internments WHERE location_id = location;
    if found then
      -- Check if there is any internment in which the same location is present and date_time_ends is NULL.
      SELECT INTO found_internment * FROM internments WHERE location_id = location AND date_time_ends IS NOT NULL;
      if not found then
        raise notice 'Found open internment with current location!';
        return 5;
      end if;
    end if;

    SELECT into found_internment * FROM internments WHERE mac = macAddr;
    if found then
      -- Check if there is any internment in which the same mac is present and date_time_ends is NULL.
      SELECT INTO found_internment * FROM internments WHERE mac = macAddr AND date_time_ends IS NOT NULL;
      if not found then
        raise notice 'Found open internment with current device!';
        return 6;
      end if;
    end if;

  end if;

  INSERT INTO internments(date_time_begins, patient_id, location_id, mac) VALUES(begins, patient, location, macAddr);
  -- Create the first alarm entry.
  INSERT INTO alarms(modify_ts, modifying_mac, internment_id) VALUES(currentTime, '000000000000', (SELECT internment_id from internments WHERE date_time_begins = begins AND patient_id = patient AND location_id = location AND mac = macAddr));
  return 0;
end;
$$
language plpgsql;

-- Update alarms thresholds from an internment
/**
 * \brief Update alarms thresholds from an internment
 * \param modMac The MAC of the modifying application (monitor).
 * \param lastAlarmId Last alarm ID as reported to the monitor.
 * \param internmentId internment_id to which the alarms are associated.
 * \returns
 * - 0 is the alarm could be updated.
 * - 1 if the internment could not be found or it is not open.
 * - 2 if the alarm was modified since last update.
 */
create or replace function update_alarms(modMac macaddr, lastUpdate timestamptz, internmentId int,
                                         spo2LT integer, spo2Delay smallint,
                                         hRLT integer, hRGT integer, hRDelay smallint,
                                         bTLT integer, bTGT integer, bTDelay smallint,
                                         bPSysLT integer, bPSysGT integer, bPDelay smallint)
returns integer
as $$
declare
  found_internment internments%rowtype;
  found_alarm alarms%rowtype;
  -- Limit the timestamp to just 3 decimals of milliseconds. This helps with handling date/time with Qt.
  currentTime constant alarms.modify_ts%type := current_timestamp(3);
begin
  -- Check if the internment is present and open.
  SELECT INTO found_internment * FROM internments WHERE internment_id = internmentId AND date_time_ends is NULL;
  if not found then
    return 1;
  end if;

  SELECT INTO found_alarm * FROM alarms WHERE modify_ts IN (SELECT modify_ts FROM alarms WHERE internment_id = internmentId ORDER BY modify_ts DESC LIMIT 1) AND modify_ts = lastUpdate;
  if not found then
    return 2;
  end if;

  INSERT INTO alarms(modify_ts, internment_id, modifying_mac, spo2_alarm_lt, spo2_alarm_delay_s,
                     hr_alarm_lt, hr_alarm_gt, hr_alarm_delay_s,
                     bt_alarm_lt, bt_alarm_gt, bt_alarm_delay_s,
                     bp_sys_alarm_lt, bp_sys_alarm_gt, bp_alarm_delay_s)
              VALUES(currentTime, internmentId, modMac,
                     spo2LT, spo2Delay,
                     hRLT, hRGT, hRDelay,
                     bTLT, bTGT, bTDelay,
                     bPSysLT, bPSysGT, bPDelay);

  return 0;
end;
$$
language plpgsql;

-- Add SpO2 data
/**
 * This procedure will first check for a device with MAC theMac.
 *
 * \returns
 * - 0 if the data was added.
 * - 1 if the device MAC was not found.
 * - 2 if the device was found but is not yet associated with an internment.
 * - 3 if there are more than one internments with the device associated to them and date_time_ends = NULL.
 */
create or replace function add_spo2(theMac macaddr, newTime bigint, newSpO2 integer)
returns integer
as $$
declare
  found_device devices%rowtype;
  found_internment internments%rowtype;
  internments_size integer;
begin
  -- Check if the device is present.
  SELECT INTO found_device * FROM devices WHERE mac = theMac;
  if not found then
    return 1;
  end if;

  -- Check for a related internment.
  internments_size := (SELECT count(*) FROM internments WHERE mac = theMac AND date_time_ends is NULL);

  if internments_size = 0 then
    return 2;
  elsif internments_size > 1 then
    raise WARNING 'More than one active internments refer to the same device! Not adding data for device %', theMac;
    return 3;
  end if;

  SELECT INTO found_internment * FROM internments WHERE mac = theMac AND date_time_ends is NULL;

  INSERT INTO spo2_values(time, spo2, internment_id) VALUES(newTime, newSpO2, found_internment.internment_id);

  return 0;
end;
$$
language plpgsql;

-- Add heart rate data
/**
 * This procedure will first check for a device with MAC theMac.
 *
 * \returns
 * - 0 if the data was added.
 * - 1 if the device MAC was not found.
 * - 2 if the device was found but is not yet associated with an internment.
 * - 3 if there are more than one internments with the device associated to them and date_time_ends = NULL.
 */
create or replace function add_heart_rate(theMac macaddr, newTime bigint, newHeartRate integer)
returns integer
as $$
declare
  found_device devices%rowtype;
  found_internment internments%rowtype;
  internments_size integer;
begin
  -- Check if the device is present.
  SELECT INTO found_device * FROM devices WHERE mac = theMac;
  if not found then
    return 1;
  end if;

  -- Check for a related internment.
  internments_size := (SELECT count(*) FROM internments WHERE mac = theMac AND date_time_ends is NULL);

  if internments_size = 0 then
    return 2;
  elsif internments_size > 1 then
    raise WARNING 'More than one active internments refer to the same device! Not adding data for device %', theMac;
    return 3;
  end if;

  SELECT INTO found_internment * FROM internments WHERE mac = theMac AND date_time_ends is NULL;

  INSERT INTO heart_rate_values(time, heart_rate, internment_id) VALUES(newTime, newHeartRate, found_internment.internment_id);

  return 0;
end;
$$
language plpgsql;

-- Add blood pressure data
/**
 * This procedure will first check for a device with MAC theMac.
 *
 * \returns
 * - 0 if the data was added.
 * - 1 if the device MAC was not found.
 * - 2 if the device was found but is not yet associated with an internment.
 * - 3 if there are more than one internments with the device associated to them and date_time_ends = NULL.
 */
create or replace function add_blood_pressure(theMac macaddr, newTime bigint, newSys smallint, newDias smallint)
returns integer
as $$
declare
  found_device devices%rowtype;
  found_internment internments%rowtype;
  internments_size integer;
begin
  -- Check if the device is present.
  SELECT INTO found_device * FROM devices WHERE mac = theMac;
  if not found then
    return 1;
  end if;

  -- Check for a related internment.
  internments_size := (SELECT count(*) FROM internments WHERE mac = theMac AND date_time_ends is NULL);

  if internments_size = 0 then
    return 2;
  elsif internments_size > 1 then
    raise WARNING 'More than one active internments refer to the same device! Not adding data for device %', theMac;
    return 3;
  end if;

  SELECT INTO found_internment * FROM internments WHERE mac = theMac AND date_time_ends is NULL;

  INSERT INTO blood_pressure_values(time, systolic, diastolic, internment_id) VALUES(newTime, newSys, newDias, found_internment.internment_id);

  return 0;
end;
$$
language plpgsql;

-- Add body temperature data
/**
 * This procedure will first check for a device with MAC theMac.
 *
 * \returns
 * - 0 if the data was added.
 * - 1 if the device MAC was not found.
 * - 2 if the device was found but is not yet associated with an internment.
 * - 3 if there are more than one internments with the device associated to them and date_time_ends = NULL.
 */
create or replace function add_body_temperature(theMac macaddr, newTime bigint, newBodyTemp integer)
returns integer
as $$
declare
  found_device devices%rowtype;
  found_internment internments%rowtype;
  internments_size integer;
begin
  -- Check if the device is present.
  SELECT INTO found_device * FROM devices WHERE mac = theMac;
  if not found then
    return 1;
  end if;

  -- Check for a related internment.
  internments_size := (SELECT count(*) FROM internments WHERE mac = theMac AND date_time_ends is NULL);

  if internments_size = 0 then
    return 2;
  elsif internments_size > 1 then
    raise WARNING 'More than one active internments refer to the same device! Not adding data for device %', theMac;
    return 3;
  end if;

  SELECT INTO found_internment * FROM internments WHERE mac = theMac AND date_time_ends is NULL;

  INSERT INTO body_temperature_values(time, temperature, internment_id) VALUES(newTime, newBodyTemp, found_internment.internment_id);

  return 0;
end;
$$
language plpgsql;

/**
 * Deletes data with seconds since epoch older than olderThan.
 * \returns 0.
 */
create or replace function delete_old_data(olderThan bigint)
returns integer
as $$
begin

  DELETE FROM spo2_values WHERE time <= olderThan;
  DELETE FROM blood_pressure_values WHERE time <= olderThan;
  DELETE FROM heart_rate_values WHERE time <= olderThan;
  DELETE FROM body_temperature_values WHERE time <= olderThan;

  return 0;
end;
$$
language plpgsql;

/**
 * \brief Stored procedure to be called whenever the internments table changes.
 * It currently does nothing, but serves to let Qt know the table has changed.
 */
create or replace function internments_changed()
returns trigger
as $$
begin
  NOTIFY internments_changed;
  raise notice 'Table internments has changed';
  return NULL;
end;
$$
language plpgsql;

CREATE TRIGGER trig_internments_changed AFTER INSERT OR UPDATE OR DELETE OR TRUNCATE ON internments EXECUTE PROCEDURE internments_changed();
