-- Devices
create table devices
(
    mac                     macaddr not null unique,
    date_time_added         timestamptz not null,
    date_time_last_seen     timestamptz not null,
    battmV                  integer,
    timems                  bigint not null,
    msgId                   bigint,
    CONSTRAINT              device_pk PRIMARY KEY(mac),
    CONSTRAINT              device_battmV CHECK(battmV > 0),
    CONSTRAINT              device_timems CHECK(timems >= 0),
    CONSTRAINT              device_msgId CHECK(msgId >= 0)
);


-- Locations
create type location_type as ENUM('local', 'external');

create table locations
(
    location_id             serial not null,
    type                    location_type not null,
    description             varchar(32) not null unique,
    CONSTRAINT              locations_pk PRIMARY KEY(location_id)
);

-- Patients
create table patients
(
    patient_id              serial not null,
    date_time_added         timestamptz not null,
    name                    varchar(64) not null,
    surname                 varchar(64) not null,
    age                     smallint,
    gender                  char,
    dni                     integer,
    comments                varchar(128),
    CONSTRAINT              patients_pk PRIMARY KEY(patient_id),
    CONSTRAINT              patients_name CHECK (name ~ '[[:alnum:]_]+'),
    CONSTRAINT              patients_surname CHECK (surname ~ '[[:alnum:]_]+'),
    CONSTRAINT              patients_age CHECK(age >= 0),
    CONSTRAINT              patients_gender CHECK(gender ~ '[mfo]{1}'),
    CONSTRAINT              patients_dni CHECK(dni >= 0)
);

-- Internments
/**
 * Alarms ar defined with gt (greater or equal than >=) and
 * lt (lesser or equal than <=).
 * Remember that values are expressed in their wire forms, see
 * https://mosimpa.gitlab.io/datos_a_transmitir/ for more info.
 */
create table internments
(
    internment_id           serial not null,
    date_time_begins        timestamptz not null,
    date_time_ends          timestamptz DEFAULT NULL,
    patient_id              integer not null,
    location_id             integer not null,
    mac                     macaddr not null,
    CONSTRAINT              internments_pk PRIMARY KEY(internment_id),
    CONSTRAINT              internments_patients_fk FOREIGN KEY(patient_id) REFERENCES patients(patient_id),
    CONSTRAINT              internments_locations_fk FOREIGN KEY(location_id) REFERENCES locations(location_id),
    CONSTRAINT              internments_devices_fk FOREIGN KEY(mac) REFERENCES devices(mac)
);

-- Alarms
/**
 * Alarms ar defined with gt (greater or equal than >=) and
 * lt (lesser or equal than <=).
 */
create table alarms
(
    modify_ts               timestamptz not null, -- Modification timestamp.
    internment_id           integer not null, -- Associated internment_id.
    modifying_mac           macaddr not null, -- MAC of the device making the modification.
    spo2_alarm_lt           integer not null DEFAULT 950, -- 0.1 %
    spo2_alarm_delay_s      smallint not null DEFAULT 15, -- 0.1 %
    hr_alarm_lt             integer not null DEFAULT 600, -- 0.1 BPM
    hr_alarm_gt             integer not null DEFAULT 1000, -- 0.1 BPM
    hr_alarm_delay_s        smallint not null DEFAULT 15, -- seconds
    bt_alarm_lt             integer not null DEFAULT 370, -- 0.1ºC
    bt_alarm_gt             integer not null DEFAULT 380, -- 0.1ºC
    bt_alarm_delay_s        smallint not null DEFAULT 15, -- seconds
    bp_sys_alarm_lt         integer not null DEFAULT 120, -- mmHg
    bp_sys_alarm_gt         integer not null DEFAULT 150, -- mmHg
    bp_alarm_delay_s        smallint not null DEFAULT 15, -- seconds
    CONSTRAINT              alarms_pk PRIMARY KEY(modify_ts, internment_id),
    CONSTRAINT              alarms_internments_fk FOREIGN KEY(internment_id) REFERENCES internments(internment_id) ON DELETE CASCADE,
    CONSTRAINT              alarms_spo2_alarm_lt CHECK(spo2_alarm_lt >= 850 AND spo2_alarm_lt <= 950),
    CONSTRAINT              alarms_spo2_alarm_delay_s CHECK(spo2_alarm_delay_s >= 15 AND spo2_alarm_delay_s <= 60),
    CONSTRAINT              alarms_hr_alarm_lt CHECK(hr_alarm_lt >= 400 AND hr_alarm_lt <= 600 AND hr_alarm_lt < hr_alarm_gt),
    CONSTRAINT              alarms_hr_alarm_gt CHECK(hr_alarm_gt >= 1000 AND hr_alarm_gt <= 1400 AND hr_alarm_lt < hr_alarm_gt),
    CONSTRAINT              alarms_hr_alarm_delay_s CHECK(hr_alarm_delay_s >= 15 AND hr_alarm_delay_s <= 60),
    CONSTRAINT              alarms_bt_alarm_lt CHECK(bt_alarm_lt >= 350 AND bt_alarm_lt <= 370), -- 0.1 ºC
    CONSTRAINT              alarms_bt_alarm_gt CHECK(bt_alarm_gt >= 380 AND bt_alarm_gt <= 400), -- 0.1 ºC
    CONSTRAINT              alarms_bt_alarm_delay_s CHECK(bt_alarm_delay_s >= 15 AND bt_alarm_delay_s <= 60),
    CONSTRAINT              alarms_bp_sys_alarm_lt CHECK(bp_sys_alarm_lt >= 100 AND bp_sys_alarm_lt <= 120 AND bp_sys_alarm_lt < bp_sys_alarm_gt),
    CONSTRAINT              alarms_bp_sys_alarm_gt CHECK(bp_sys_alarm_gt >= 150 AND bp_sys_alarm_gt <= 170 AND bp_sys_alarm_lt < bp_sys_alarm_gt),
    CONSTRAINT              alarms_bp_alarm_delay_s CHECK(bp_alarm_delay_s >= 15 AND bp_alarm_delay_s <= 60)
);

/*
 * Sensors data.
 */

create table spo2_values
(
    spo2_values_id          serial not null,
    time                    bigint not null,
    spo2                    integer not null,
    internment_id           integer not null,
    CONSTRAINT              spo2_values_pk PRIMARY KEY(spo2_values_id),
    CONSTRAINT              spo2_values_time CHECK(time >= 0),
    CONSTRAINT              spo2_values_spo2 CHECK(spo2 >=0 AND spo2 <= 1000),
    CONSTRAINT              spo2_values_internments_fk FOREIGN KEY(internment_id) REFERENCES internments(internment_id) ON DELETE CASCADE,
    CONSTRAINT              spo2_unique UNIQUE(internment_id, time)
);

create table blood_pressure_values
(
    blood_pressure_values_id serial not null,
    time                    bigint not null,
    systolic                smallint not null,
    diastolic               smallint not null,
    internment_id           integer not null,
    CONSTRAINT              blood_pressure_values_pk PRIMARY KEY(blood_pressure_values_id),
    CONSTRAINT              blood_pressure_values_time CHECK(time >= 0),
    CONSTRAINT              blood_pressure_values_sys CHECK(systolic >= 0 AND systolic <= 255),
    CONSTRAINT              blood_pressure_values_dia CHECK(diastolic >= 0 AND diastolic <= 255),
    CONSTRAINT              blood_pressure_values_internments_fk FOREIGN KEY(internment_id) REFERENCES internments(internment_id) ON DELETE CASCADE,
    CONSTRAINT              blood_pressure_values_unique UNIQUE(internment_id, time)
);

create table heart_rate_values
(
    heart_rate_values_id    serial not null,
    time                    bigint not null,
    heart_rate              integer not null,
    internment_id           integer not null,
    CONSTRAINT              heart_rate_values_pk PRIMARY KEY(heart_rate_values_id),
    CONSTRAINT              heart_rate_values_time CHECK(time >= 0),
    CONSTRAINT              heart_rate_values_heart_rate CHECK(heart_rate >= 0 AND heart_rate <= 3000),
    CONSTRAINT              heart_rate_values_internments_fk FOREIGN KEY(internment_id) REFERENCES internments(internment_id)  ON DELETE CASCADE,
    CONSTRAINT              heart_rate_values_unique UNIQUE(internment_id, time)
);

create table body_temperature_values
(
    body_temperature_values_id serial not null,
    time                    bigint not null,
    temperature             integer not null,
    internment_id           integer not null,
    CONSTRAINT              body_temperature_values_pk PRIMARY KEY(body_temperature_values_id),
    CONSTRAINT              body_temperature_values_time CHECK(time >= 0),
    CONSTRAINT              body_temperature_values_temperature CHECK(temperature >= 0 AND temperature <= 500),
    CONSTRAINT              body_temperature_values_internments_fk FOREIGN KEY(internment_id) REFERENCES internments(internment_id) ON DELETE CASCADE,
    CONSTRAINT              body_temperature_values_unique UNIQUE(internment_id, time)
);
