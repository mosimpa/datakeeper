/**
 * Deletes data with seconds since epoch older than olderThan.
 * After that it will trigger a vacuum in order to clean space for future usage.
 * \returns 0.
 */
create or replace function delete_old_data(olderThan bigint)
returns integer
as $$
begin

  DELETE FROM spo2_values WHERE time <= olderThan;
  DELETE FROM blood_pressure_values WHERE time <= olderThan;
  DELETE FROM heart_rate_values WHERE time <= olderThan;
  DELETE FROM body_temperature_values WHERE time <= olderThan;

  -- Vacuum.
  VACUUM;

  return 0;
end;
$$
language plpgsql;

/**
 * \brief Stored procedure to be called whenever the internments table changes.
 * It currently does nothing, but serves to let Qt know the table has changed.
 */
create or replace function internments_changed()
returns trigger
as $$
begin
  NOTIFY internments_changed;
  raise notice 'Table internments has changed';
  return NULL;
end;
$$
language plpgsql;

CREATE TRIGGER trig_internments_changed AFTER INSERT OR UPDATE OR DELETE OR TRUNCATE ON internments EXECUTE PROCEDURE internments_changed();
