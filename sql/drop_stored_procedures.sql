-- Eliminate stored procedures.
DROP TRIGGER trig_internments_changed ON internments;
DROP FUNCTION internments_changed();
DROP FUNCTION delete_old_data(olderThan bigint);
DROP FUNCTION add_spo2(theMac macaddr, newTime bigint, newSpO2 integer);
DROP FUNCTION add_heart_rate(theMac macaddr, newTime bigint, newHeartRate integer);
DROP FUNCTION add_blood_pressure(theMac macaddr, newTime bigint, newSys smallint, newDias smallint);
DROP FUNCTION add_body_temperature(theMac macaddr, newTime bigint, newBodyTemp integer);
DROP FUNCTION add_or_update_device(newMac macaddr, newBattmV integer, newTimems bigint, newMsgId bigint);
DROP FUNCTION add_patient(newName varchar(64), newSurname varchar(64),
                          newAge smallint, newGender char,
                          newDni integer, newComments varchar(128));
DROP FUNCTION add_location(newType location_type, newDescription varchar(32));
DROP FUNCTION update_alarms((modMac macaddr, lastAlarmId integer, internmentId int,
                             spo2LT integer, spo2Delay smallint,
                             hRLT integer, hRGT integer, hRDelay smallint,
                             bTLT integer, bTGT integer, bTDelay smallint,
                             bPSysLT integer, bPSysGT integer, bPDelay smallint);
DROP FUNCTION add_internment(begins timestamptz, patient integer, location integer, mac macaddr);
