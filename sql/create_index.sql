-- Patients table
CREATE INDEX patients_name_surname_index ON patients (name, surname);
CREATE INDEX patients_dni_index ON patients (dni);
