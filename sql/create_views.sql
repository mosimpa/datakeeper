-- Views
CREATE VIEW merged_name_surname_patients AS SELECT patient_id, date_time_added, CONCAT(surname,', ',name) AS patient, age, gender, dni, comments FROM patients;
CREATE VIEW not_interned_patients AS SELECT patient_id, date_time_added, CONCAT(surname,', ',name) AS patient, age, gender, dni, comments FROM patients WHERE patient_id NOT IN (SELECT patient_id FROM internments WHERE date_time_ends IS NULL);
CREATE VIEW available_locations AS SELECT * from locations WHERE location_id NOT IN (SELECT patient_id FROM internments WHERE date_time_ends IS NULL AND patient_id IS NOT NULL);
CREATE VIEW available_devices AS SELECT * from devices WHERE mac NOT IN (SELECT mac FROM internments WHERE date_time_ends IS NULL AND mac IS NOT NULL) ORDER BY mac ASC;
